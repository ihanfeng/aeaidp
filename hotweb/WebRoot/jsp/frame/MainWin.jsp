<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html; charset=UTF-8" errorPage="/jsp/frame/Error.jsp" %>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html>
<head>
<title>主页面</title>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<script type="text/javascript" src="js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="js/util.js"></script>
<link href="css/style.css" rel="stylesheet" type="text/css">
<style type="text/css">
.footer {
	height: 31px;
	line-height: 31px;
	font-size: 12px;
	text-align: center;
	color: #666666;
	width: 99%;
}
.mainContent{
	font-size: 14px;
	margin:8px 10px;
	line-height:25px;
}

div#wrap {
 padding-top: 87px;
}
</style>
</head>
<body>
<div id="wrap">
<table width="96%"  border="0" align="center">
  <tr>
    <td width="80" align="center"><img src="images/index/mainpic.jpg"></td>
    <td>
<div class="mainContent">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Miscdp应用开发平台，Misc Develope Platform，包括三部分：一站式的Java Web框架--Hotweb、基于Eclipse插件的GUI开发工具--DevStudio、以及运行服务器HotServer。Miscdp开发平台除了可以快速开发定制的JavaWeb应用，还可以为门户平台扩展开发Portlet组件、为流程平台扩展开发业务表单及功能。</div>
<div class="mainContent">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hotweb除了是一站式的JavaWeb框架，还包括典型的功能模型框架以及常见的预置功能，典型功能模型包括：单表操作类、主从表操作、树及关联管理、树形分组管理、综合查询、扩展Portlet、树及列表选择等；预置功能模块包括：登录认证、功能菜单管理、群组角色管理、系统用户管理、系统授权管理，系统日志管理、系统编码管理等。</div>
<div class="mainContent">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Miscdp开发平台是功能引擎与代码生成器这两种极端开发模式中间结合的产物，主要用于开发两类JavaWeb项目，一是普通Web项目，一类集成Web项目。集成Web项目跟门户平台结合，基于门户平台内置的统一身份认证，复用门户平台的组织、用户、角色数据，除了包括普通Web项目所有功能以外，还可以用于快速开发Portlet。</div>
</td>
  </tr>
</table>
</div>
</body>
</html>
<script language="javascript">
if (parent.topright.document && parent.topright.document.getElementById('currentPath'))
parent.topright.document.getElementById('currentPath').innerHTML='系统主页面';
</script>
