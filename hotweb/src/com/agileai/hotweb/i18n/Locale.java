package com.agileai.hotweb.i18n;

import java.io.Serializable;

public class Locale implements Serializable{
	private static final long serialVersionUID = 7947862199456661237L;
	
	private String language = null;
	private String country = null;
	
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
}