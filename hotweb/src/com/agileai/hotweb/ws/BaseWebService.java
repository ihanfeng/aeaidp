package com.agileai.hotweb.ws;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import org.apache.log4j.Logger;

import com.agileai.common.AppConfig;
import com.agileai.hotweb.common.BeanFactory;
import com.agileai.hotweb.common.ClassLoaderFactory;
import com.agileai.hotweb.common.HttpRequestHelper;
import com.agileai.hotweb.common.ModuleManager;
import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.util.StringUtil;

public class BaseWebService {
	@Resource
	private WebServiceContext wsContext;
	private ServiceModel serviceModel = null;
	
	protected Logger log = Logger.getLogger(this.getClass());
	
	public MessageContext getMessageContext(){
		MessageContext mc = wsContext.getMessageContext();
		return mc;
	}

	public HttpServletRequest getHttpServletRequest(){
		MessageContext mc = this.getMessageContext();
		HttpServletRequest httpRequest = (HttpServletRequest)(mc.get(MessageContext.SERVLET_REQUEST));
		return httpRequest;
	}

	public HttpServletResponse getHttpServletResponse(){
		MessageContext mc = this.getMessageContext();
		HttpServletResponse httpResponse = (HttpServletResponse)(mc.get(MessageContext.SERVLET_RESPONSE));
		return httpResponse;
	}	
	
	protected DataSource getDataSource(){
		return BeanFactory.instance().getDataSource();
	}
	
	protected DataSource getDataSource(String dataSourceId){
		return BeanFactory.instance().getDataSource(dataSourceId);
	}
	
	public AppConfig getAppConfig(){
		AppConfig result = (AppConfig)BeanFactory.instance().getAppConfig();
		return result;
	}		
	
	protected Object lookupService(String serviceId){
		Object service = null;
		ClassLoader appClassLoader = Thread.currentThread().getContextClassLoader();
		ModuleManager moduleManager = ModuleManager.getOnly(appClassLoader);
		String moduleName = moduleManager.getServiceModule(serviceId);
		if (StringUtil.isNotNullNotEmpty(moduleName)){
			ClassLoaderFactory classLoaderFactory = ClassLoaderFactory.instance(appClassLoader);
			ClassLoader moduleClassLoader = classLoaderFactory.createModuleClassLoader(moduleName);
			service = BeanFactory.instance(moduleClassLoader).getBean(serviceId);
		}else{
			service = BeanFactory.instance().getBean(serviceId);
		}
		return service;
	}
	
	protected <ServiceType> ServiceType lookupService(Class<ServiceType> serviceClass){
		String serviceId = BaseHandler.buildServiceId(serviceClass);
		Object service = this.lookupService(serviceId);
		return serviceClass.cast(service);
	}

	public ServiceModel getServiceModel() {
		return serviceModel;
	}
	
	public void setServiceModel(ServiceModel serviceModel) {
		this.serviceModel = serviceModel;
	}
	
	public String getRemoteHost(HttpServletRequest request){
		return HttpRequestHelper.getRemoteHost(request);
	}	
}