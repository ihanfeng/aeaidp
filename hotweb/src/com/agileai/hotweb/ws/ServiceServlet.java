package com.agileai.hotweb.ws;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.spi.Provider;

import org.apache.cxf.Bus;
import org.apache.cxf.BusFactory;
import org.apache.cxf.binding.BindingFactoryManager;
import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSBindingFactory;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxrs.model.wadl.WadlGenerator;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.transport.servlet.CXFNonSpringServlet;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.agileai.hotweb.common.ClassLoaderFactory;
import com.agileai.hotweb.common.MemSecrityKeys;
import com.agileai.hotweb.common.ServiceManager;
import com.agileai.util.IOUtil;
import com.agileai.util.StringUtil;

public class ServiceServlet extends CXFNonSpringServlet{
	private static final long serialVersionUID = -8311880832156804644L;
	private ServletConfig servletConfig = null;
	
	public void loadBus(ServletConfig servletConfig) {
		this.servletConfig = servletConfig;
		
		super.loadBus(servletConfig);
        Bus bus = this.getBus();
        BusFactory.setDefaultBus(bus);
        
        registryServices(false);
	}
	
	private void registryServices(boolean resetThreadDefaultBus){
		if (resetThreadDefaultBus){
			BusFactory.setThreadDefaultBus(getBus());
        }
		
        List<ServiceModel> soapModels = this.buildSoapServiceModelList();
        for (int i=0;i < soapModels.size();i++){
        	ServiceModel serviceModel = soapModels.get(i);
            this.registrySoapService(serviceModel);
        }
        
        List<ServiceModel> restModels = this.buildRestServiceModelList();
        for (int i=0;i < restModels.size();i++){
        	ServiceModel serviceModel = restModels.get(i);
            this.registryRestService(serviceModel);
        }
	}
	
	private void destroyServices(){
        List<ServiceModel> soapModels = this.buildSoapServiceModelList();
        for (int i=0;i < soapModels.size();i++){
        	ServiceModel serviceModel = soapModels.get(i);
            this.destroySoapService(serviceModel);
        }
        
        List<ServiceModel> restModels = this.buildRestServiceModelList();
        for (int i=0;i < restModels.size();i++){
        	ServiceModel serviceModel = restModels.get(i);
        	this.destroyRestService(serviceModel);
        }
	}
	
	private List<ServiceModel> buildSoapServiceModelList(){
		List<ServiceModel> models = new ArrayList<ServiceModel>();
		String mainPkg = this.getServletContext().getInitParameter("mainPkg");
		if (StringUtil.isNotNullNotEmpty(mainPkg)){
			String contextPath = servletConfig.getServletContext().getContextPath();
			String appName = contextPath.substring(1);
			ClassLoader appClassLoader = Thread.currentThread().getContextClassLoader();
			ServiceManager serviceManager = ServiceManager.getOnly(appClassLoader);
			
			List<ServiceModel> soapServiceModels = serviceManager.getSoapServiceModels(appName, mainPkg);
			
			for (int i=0;i < soapServiceModels.size();i++){
				ServiceModel serviceModel = soapServiceModels.get(i);
				if (serviceModel != null){
					models.add(serviceModel);
				}
			}
		}
		return models;
	}
	
	private List<ServiceModel> buildRestServiceModelList(){
		List<ServiceModel> models = new ArrayList<ServiceModel>();
		String mainPkg = this.getServletContext().getInitParameter("mainPkg");
		if (StringUtil.isNotNullNotEmpty(mainPkg)){
			String contextPath = servletConfig.getServletContext().getContextPath();
			String appName = contextPath.substring(1);
			
			ClassLoader appClassLoader = Thread.currentThread().getContextClassLoader();
			ServiceManager serviceManager = ServiceManager.getOnly(appClassLoader);
			List<ServiceModel> restServiceModels = serviceManager.getRestServiceModels(appName, mainPkg);
			
			for (int i=0;i < restServiceModels.size();i++){
				ServiceModel serviceModel = restServiceModels.get(i);
				if (serviceModel != null){
					models.add(serviceModel);
				}
			}
		}
		return models;
	}
	
	private void setupCharset(HttpServletRequest request, HttpServletResponse response) throws ServletException{
		try {
			String charset = null;
			String charEncoding = request.getCharacterEncoding();
			if (StringUtil.isNullOrEmpty(charEncoding)) {
				charset = "UTF-8";
			}else{
				charset = charEncoding;
			}
			response.setHeader("content-type","text/xml;charset="+charset);
			response.setCharacterEncoding(charset);
		    request.setCharacterEncoding(charset);
		} catch (Exception e) {
			throw new ServletException(e);
		}
	}
	
	public void invoke(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		setupCharset(request, response);
		
		String actionType = request.getParameter("actionType");
		if (StringUtil.isNotNullNotEmpty(actionType)){
			JSONObject jsonResponse = new JSONObject();
			try {
				if ("unload".equals(actionType) && checkUserAuth(request)){
					this.destroyServices();
					ClassLoader appClassLoader = Thread.currentThread().getContextClassLoader();
					ServiceManager serviceManager = ServiceManager.getOnly(appClassLoader);
					boolean unloadResult = serviceManager.unload();
					if (unloadResult){
						jsonResponse.put("status", "OK");	
					}else{
						jsonResponse.put("status", "Error");
						jsonResponse.put("message", "unload service failure !");
					}
				}
				else if ("load".equals(actionType) && checkUserAuth(request)){
					ClassLoader appClassLoader = Thread.currentThread().getContextClassLoader();
					ServiceManager serviceManager = ServiceManager.getOnly(appClassLoader);
					boolean loadResult = serviceManager.load();
					if (loadResult){
						this.registryServices(true);
						jsonResponse.put("status", "OK");	
					}else{
						jsonResponse.put("status", "Error");
						jsonResponse.put("message", "unload service failure !");
					}
				}
				processPrint(response, jsonResponse.toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else{
			super.invoke(request, response);
		}
	}
	
	private void registryRestService(ServiceModel serviceModel){
		try {
			ClassLoader appClassLoader = Thread.currentThread().getContextClassLoader();
			ServiceManager serviceManager = ServiceManager.getOnly(appClassLoader);
			
			String serviceClass = serviceModel.getImplClass();
			String address = serviceModel.getAddress();
			
			JAXRSServerFactoryBean serverFactory = new JAXRSServerFactoryBean();
			ClassLoaderFactory classLoaderFactory = ClassLoaderFactory.instance(appClassLoader);
			
			ClassLoader classLoader = classLoaderFactory.createServiceClassLoader();
			
			BaseRestService serviceBean = (BaseRestService)classLoader.loadClass(serviceClass).newInstance();
			serviceBean.setServiceModel(serviceModel);
			
			serverFactory.setServiceBean(serviceBean);
			serverFactory.setAddress(address);
			
			if (serviceModel.getAuthConfigs().size() > 0){
				RestServerIntercetper serverIntercetper = new RestServerIntercetper();
				serverIntercetper.setServiceModel(serviceModel);
				serverFactory.getInInterceptors().add(serverIntercetper);				
			}
			
			List<Object> providers = new ArrayList<Object>();
			
			WadlGenerator wadlGenerator = new WadlGenerator();
			wadlGenerator.setLinkAnyMediaTypeToXmlSchema(true);
			wadlGenerator.setUseJaxbContextForQnames(true);
			providers.add(wadlGenerator);

			JacksonJsonProvider jacksonJsonProvider = new JacksonJsonProvider();
			ObjectMapper objectMapper = new ObjectMapper();
			jacksonJsonProvider.setMapper(objectMapper);
			providers.add(jacksonJsonProvider);
			
			serverFactory.setProviders(providers);
			
			BindingFactoryManager manager = serverFactory.getBus().getExtension(BindingFactoryManager.class);
			JAXRSBindingFactory factory = new JAXRSBindingFactory();
			factory.setBus(serverFactory.getBus());
			manager.registerBindingFactory(JAXRSBindingFactory.JAXRS_BINDING_ID, factory);
			Server server = serverFactory.create();
			
			serviceManager.getRestServiceServerMap().put(serviceModel.getId(), server);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void registrySoapService(ServiceModel serviceModel){
		try {
			ClassLoader appClassLoader = Thread.currentThread().getContextClassLoader();
			ServiceManager serviceManager = ServiceManager.getOnly(appClassLoader);
			
			String implClass = serviceModel.getImplClass();
			String address = serviceModel.getAddress();
			
			ClassLoaderFactory classLoaderFactory = ClassLoaderFactory.instance(appClassLoader);
			Object implementor;
			
			ClassLoader classLoader = classLoaderFactory.createServiceClassLoader();
			implementor = (Object)classLoader.loadClass(implClass).newInstance();

			BaseWebService baseWebService = (BaseWebService)implementor;
			baseWebService.setServiceModel(serviceModel);
			
			Provider provider = Provider.provider();
			EndpointImpl endPoint = (EndpointImpl)provider.createEndpoint(null, implementor);
			
			if (serviceModel.getAuthConfigs().size() > 0){
				SoapServerIntercetper serverIntercetper = new SoapServerIntercetper();
				serverIntercetper.setServiceModel(serviceModel);
				endPoint.getInInterceptors().add(serverIntercetper);				
			}
			
	        endPoint.publish(address);
	        
	        serviceManager.getSoapServiceFactoryMap().put(serviceModel.getId(), endPoint);
	        
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void destroySoapService(ServiceModel serviceModel){
		ClassLoader appClassLoader = Thread.currentThread().getContextClassLoader();
		ServiceManager serviceManager = ServiceManager.getOnly(appClassLoader);
		
		String serviceId = serviceModel.getId();
		EndpointImpl endPoint = serviceManager.getSoapServiceFactoryMap().get(serviceId);
		if (endPoint != null){
			endPoint.stop();
			serviceManager.getSoapServiceFactoryMap().remove(serviceId);
		}
	}
	
	private void destroyRestService(ServiceModel serviceModel){
		ClassLoader appClassLoader = Thread.currentThread().getContextClassLoader();
		ServiceManager serviceManager = ServiceManager.getOnly(appClassLoader);
		
		String serviceId = serviceModel.getId();
		Server server = serviceManager.getRestServiceServerMap().get(serviceId);
		if (server != null){
			server.stop();
			server.destroy();
			
			serviceManager.getRestServiceServerMap().remove(serviceId);
		}
	}
	
	private void processPrint(HttpServletResponse response,String text){
		try {
			PrintWriter out= response.getWriter(); 
			out.print(text); 
			out.flush(); 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	protected boolean checkUserAuth(HttpServletRequest request) throws JSONException{
		boolean result = false;
		String inputString = this.getInputString(request,"UTF-8");
		if (StringUtil.isNotNullNotEmpty(inputString)){
			JSONObject requestJson = new JSONObject(inputString);
			String appName = requestJson.getString("appName");
			String securityKey = requestJson.getString("securityKey");
			String memSecurityKey = MemSecrityKeys.getSecrityKey(appName);
			if (securityKey.equals(memSecurityKey)){
				result = true;
			}
		}
		return result;
	}
	
	public String getInputString(HttpServletRequest request,String charset){
		String inputString = null;
		try {
			InputStream inputStream =  request.getInputStream();
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			IOUtil.copyAndCloseInput(inputStream, byteArrayOutputStream);
			inputString = byteArrayOutputStream.toString(charset);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return inputString;
	}
	
	@Override
	public void destroy() {
		super.destroy();
		try {
			String appName = this.getServletContext().getContextPath();
			if (appName.startsWith("/")){
				appName = appName.substring(1);
			}
			destroyServices();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}	
}
