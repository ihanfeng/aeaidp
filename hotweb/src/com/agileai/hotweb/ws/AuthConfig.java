package com.agileai.hotweb.ws;

import java.util.LinkedHashMap;

import com.agileai.common.KeyValuePair;

public class AuthConfig {
	private String type = null;
	private LinkedHashMap<String,KeyValuePair> properties = new LinkedHashMap<String,KeyValuePair>();
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public LinkedHashMap<String, KeyValuePair> getProperties() {
		return properties;
	}
	public void setProperties(LinkedHashMap<String, KeyValuePair> properties) {
		this.properties = properties;
	}
}
