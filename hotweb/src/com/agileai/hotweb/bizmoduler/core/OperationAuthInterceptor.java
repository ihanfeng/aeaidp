package com.agileai.hotweb.bizmoduler.core;

import com.agileai.hotweb.controller.core.BaseHandler;

public interface OperationAuthInterceptor {
	public static final String ParamKey = "operationAuthInterceptor";
	public boolean authenticate(Object user,BaseHandler handler,String actionType) throws Exception;
}
