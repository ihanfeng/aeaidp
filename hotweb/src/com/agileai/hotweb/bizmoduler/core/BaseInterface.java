package com.agileai.hotweb.bizmoduler.core;

public interface BaseInterface {
	public String getTableName();
	public String getUniqueField();
}
