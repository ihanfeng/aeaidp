package com.agileai.hotweb.controller.frame;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardService;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.hotweb.domain.FormSelect;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.RedirectRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.ListUtil;
import com.agileai.util.StringUtil;

public class CodeTypeManageListHandler extends StandardListHandler{
	public CodeTypeManageListHandler(){
		super();
		this.editHandlerClazz = CodeTypeManageEditHandler.class;
		this.serviceId = "codeTypeService";
	}
	@SuppressWarnings("unchecked")
	protected void processPageAttributes(DataParam param) {
		FormSelect typeGroupFormSelect = FormSelectFactory.create("CODE_TYPE_GROUP");
		typeGroupFormSelect.setSelectedValue(getAttributeValue("TYPE_GROUP"));
		this.setAttribute("TYPE_GROUP", typeGroupFormSelect);
		this.initMappingItem("TYPE_GROUP", typeGroupFormSelect.getContent());
		this.initMappingItem("EXTEND_SQL",FormSelectFactory.create("BOOL_DEFINE").getContent());
		
		List<DataRow> records = this.getRsList();
		for (int i=0;i < records.size();i++){
			DataRow row = records.get(i);
			String extSQL = row.getString("EXTEND_SQL");
			if (StringUtil.isNullOrEmpty(extSQL)){
				extSQL = "N";
			}
			if ("N".equals(extSQL)){
				row.put("EXTEND_SQL","N");
			}else{
				row.put("EXTEND_SQL","Y");
				row.put("IS_UNITEADMIN","N");
			}
		}
	}
	protected void initParameters(DataParam param) {
		initParamItem(param,"TYPE_GROUP","sys_code_define");
	}
	public ViewRenderer doShowCodeListAction(DataParam param){
		StandardService service = (StandardService)this.lookupService(this.serviceId);
		DataRow record = service.getRecord(param);
		String typeId = record.stringValue("TYPE_ID");
		String typeGroup = record.stringValue("TYPE_GROUP");
		return new RedirectRenderer(getHandlerURL(CodeListManageListHandler.class)
				+"&TYPE_GROUP="+typeGroup+"&TYPE_ID="+typeId);
	}
	public ViewRenderer doCheckHasDataAction(DataParam param){
		String rspText = "";
		String typeId = param.get("TYPE_ID");
		StandardService service = (StandardService)this.lookupService("codeListService");
		DataParam dataParam = new DataParam();
		dataParam.put("TYPE_ID",typeId);
		List<DataRow> records = service.findRecords(dataParam);
		if (!ListUtil.isNullOrEmpty(records)){
			rspText = "true";
		}
		return new AjaxRenderer(rspText);
	}
}
