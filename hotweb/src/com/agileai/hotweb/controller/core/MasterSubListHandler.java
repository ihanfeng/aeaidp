package com.agileai.hotweb.controller.core;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.MasterSubService;
import com.agileai.hotweb.renders.DispatchRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.RedirectRenderer;
import com.agileai.hotweb.renders.ViewRenderer;

public class MasterSubListHandler extends BaseHandler{
	@SuppressWarnings("rawtypes")
	protected Class editHandlerClazz = null;
	public MasterSubListHandler(){
		super();
	}
	public ViewRenderer prepareDisplay(DataParam param){
		mergeParam(param);
		initParameters(param);
		this.setAttributes(param);
		List<DataRow> rsList = getService().findMasterRecords(param);
		this.setRsList(rsList);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
	public ViewRenderer doQueryAction(DataParam param){
		return prepareDisplay(param);
	}
	public ViewRenderer doDeleteAction(DataParam param){
		storeParam(param);
		getService().deleteClusterRecords(param);	
		return new RedirectRenderer(getHandlerURL(getClass()));
	}
	public ViewRenderer doInsertRequestAction(DataParam param){
		storeParam(param);
		return new DispatchRenderer(getHandlerURL(this.editHandlerClazz)+"&"+OperaType.KEY+"="+OperaType.CREATE);
	}
	public ViewRenderer doCopyRequestAction(DataParam param){
		storeParam(param);
		return new DispatchRenderer(getHandlerURL(this.editHandlerClazz)+"&"+OperaType.KEY+"="+OperaType.COPY);
	}
	public ViewRenderer doViewDetailAction(DataParam param){
		storeParam(param);
		return new DispatchRenderer(getHandlerURL(this.editHandlerClazz)+"&"+OperaType.KEY+"="+OperaType.DETAIL);
	}
	public ViewRenderer doUpdateRequestAction(DataParam param){
		storeParam(param);
		return new DispatchRenderer(getHandlerURL(this.editHandlerClazz)+"&"+OperaType.KEY+"="+OperaType.UPDATE);
	}
	protected MasterSubService getService() {
		return (MasterSubService)this.lookupService(this.getServiceId());
	}	
}
