package com.agileai.hotweb.common;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxws.EndpointImpl;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;

import com.agileai.common.KeyValuePair;
import com.agileai.hotweb.ws.AuthConfig;
import com.agileai.hotweb.ws.ServiceModel;
import com.agileai.util.XmlUtil;

public class ServiceManager {
	private ClassLoader appClassLoader = null;
	public static HashMap<ClassLoader,ServiceManager> ServicerCache = new HashMap<ClassLoader,ServiceManager>();
	private HashMap<String,Server> restServiceServerMap = new HashMap<String,Server>();
	private HashMap<String,EndpointImpl> soapServiceFactoryMap = new HashMap<String,EndpointImpl>();
	
	private String basePath = null;
	
	private ServiceManager(ClassLoader appClassLoader){
		this.appClassLoader = appClassLoader;
		this.basePath = System.getProperty("catalina.base")+"/";
	}
	
	public static synchronized ServiceManager getOnly(ClassLoader appClassLoader){
		ServiceManager result = null;
		if (!ServicerCache.containsKey(appClassLoader)){
			ServiceManager moduleManager = new ServiceManager(appClassLoader);
			ServicerCache.put(appClassLoader, moduleManager);
		}
		result = ServicerCache.get(appClassLoader);
		return result;
	}
	
	public List<ServiceModel> getRestServiceModels(String appName,String mainPkg){
		List<ServiceModel> serviceModels = new ArrayList<ServiceModel>();
		StringBuffer configPath = new StringBuffer();
		configPath.append(basePath).append("webapps/").append(appName).append("/WEB-INF/services/");
		configPath.append(mainPkg.replaceAll("\\.", "/")).append("/");
		configPath.append("service/").append("/ServiceConfigs.xml");
		
		this.parseServiceModels(serviceModels,configPath.toString(), "Rest");
		
		return serviceModels;
	}
	
	public List<ServiceModel> getSoapServiceModels(String appName,String mainPkg){
		List<ServiceModel> serviceModels = new ArrayList<ServiceModel>();
		StringBuffer configPath = new StringBuffer();
		configPath.append(basePath).append("webapps/").append(appName).append("/WEB-INF/services/");
		configPath.append(mainPkg.replaceAll("\\.", "/")).append("/");
		configPath.append("service").append("/ServiceConfigs.xml");
		
		this.parseServiceModels(serviceModels,configPath.toString(), "Soap");
		
		return serviceModels;
	}
	
	public HashMap<String, Server> getRestServiceServerMap() {
		return restServiceServerMap;
	}

	public HashMap<String, EndpointImpl> getSoapServiceFactoryMap() {
		return soapServiceFactoryMap;
	}

	@SuppressWarnings("unchecked")
	private void parseServiceModels(List<ServiceModel> serviceModels,String configPath,String serviceType){
		try {
			FileInputStream serviceConfigFile = new FileInputStream(configPath);
			Document document = XmlUtil.readDocument(serviceConfigFile);
			String servicePath = "//beans/service[@type='"+serviceType+"']";
			List<Node> nodes = document.selectNodes(servicePath);
			if (nodes != null){
				for (int i=0;i < nodes.size();i++){
					Element serviceElement = (Element)nodes.get(i);
					String type = serviceElement.attributeValue("type");
					String implClass = serviceElement.attributeValue("implClass");
					String id = serviceElement.attributeValue("id");
					String address = serviceElement.attributeValue("address");
					
					ServiceModel serviceModel = new ServiceModel();
					serviceModel.setId(id);
					serviceModel.setAddress(address);
					serviceModel.setImplClass(implClass);
					serviceModel.setServiceType(type);
					
					List<Element> auths = serviceElement.elements("auth");
					if (auths != null){
						for (int j=0; j < auths.size();j++){
							Element auth = auths.get(j);
							AuthConfig authConfig = new AuthConfig();
							String authType = auth.attributeValue("type");
							authConfig.setType(authType);
							List<Element> properties = auth.elements("property");
							if (properties != null){
								for (int x=0;x < properties.size();x++){
									Element property = properties.get(x);
									String name = property.attributeValue("name");
									String value = property.getTextTrim();
									
									KeyValuePair keyValuePair = new KeyValuePair();
									keyValuePair.setKey(name);
									keyValuePair.setValue(value);
									
									authConfig.getProperties().put(name, keyValuePair);
								}
							}
							
							serviceModel.getAuthConfigs().add(authConfig);
						}
					}
					
					serviceModels.add(serviceModel);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public boolean load(){
		boolean result = false;
		try {
			ClassLoaderFactory classLoaderFactory = ClassLoaderFactory.instance(appClassLoader);
			classLoaderFactory.createServiceClassLoader();
			result = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public boolean unload(){
		boolean result = false;
		ClassLoaderFactory classLoaderFactory = ClassLoaderFactory.instance(appClassLoader);
		classLoaderFactory.rejectServiceClassLoader();

		System.gc();
		result = true;
		return result;
	}
	
	public boolean reload(){
		boolean result = false;
		result = this.unload();
		if (result){
			result = this.load();			
		}
		return result;
	}	
	

}
