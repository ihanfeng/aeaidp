package com.agileai.hotweb.common;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.XPath;

import com.agileai.util.XmlUtil;

public class ModuleManager {
	private ClassLoader appClassLoader = null;
	public static HashMap<ClassLoader,ModuleManager> ModulerCache = new HashMap<ClassLoader,ModuleManager>();
	private HashMap<String,String> serviceIndexs = null;
	private HashMap<String,String> handlerIndexs = null;
	private HashMap<String,String> portletIndexs = null;
	
	private ModuleManager(ClassLoader appClassLoader){
		this.appClassLoader = appClassLoader;
	}
	
	public static synchronized ModuleManager getOnly(ClassLoader appClassLoader){
		ModuleManager result = null;
		if (!ModulerCache.containsKey(appClassLoader)){
			ModuleManager moduleManager = new ModuleManager(appClassLoader);
			ModulerCache.put(appClassLoader, moduleManager);
			moduleManager.serviceIndexs = new HashMap<String,String>();
			moduleManager.handlerIndexs = new HashMap<String,String>();
			moduleManager.portletIndexs = new LinkedHashMap<String,String>();
		}
		result = ModulerCache.get(appClassLoader);
		return result;
	}
	
	public void initIndexs(){
		try {
			if (this.appClassLoader.getResource("HandlerContext.xml") != null){
				String webInfPath = getWebInfPath("HandlerContext.xml");
				
				File moduleFolders = new File(webInfPath+File.separator+"modules");
				if (moduleFolders.exists() ){
					File[] moduleFolderArray = moduleFolders.listFiles();
					for (int i=0;i < moduleFolderArray.length;i++){
						File moduleFolder = moduleFolderArray[i];
						String moduleName = moduleFolder.getName();
						
						this.initIndexs(webInfPath, moduleName);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	private void initIndexs(String webInfPath,String moduleName){
		String handlerModulePath  = webInfPath+File.separator+"modules"+File.separator+moduleName+File.separator+"HandlerModule.xml";
		File handlerModule = new File(handlerModulePath);
		if (handlerModule.exists()){
			Document handlersIndexDom = XmlUtil.readDocument(handlerModulePath);
			String handlerXPath = "//beans/bean";
			List<Node> handlerNodes = handlersIndexDom.selectNodes(handlerXPath);
			if (handlerNodes != null){
				for (int j=0;j < handlerNodes.size();j++){
					Element element = (Element)handlerNodes.get(j);
					String id = element.attributeValue("id");
					if (this.handlerIndexs.containsKey(id)){
						System.err.println("maybe handler " + id + " is duplicate definded !");
					}
					this.handlerIndexs.put(id, moduleName);
				}
			}	
		}
		
		String serviceModulePath = webInfPath+File.separator+"modules"+File.separator+moduleName+File.separator+"ServiceModule.xml";
		File serviceModule = new File(serviceModulePath);
		if (serviceModule.exists()){
			Document servicesIndexDom = XmlUtil.readDocument(serviceModulePath);
	        Map<String, String> ns = new HashMap<String, String>();  
	        ns.put("s", "http://www.springframework.org/schema/beans");  
	        
			String serviceXPath = "//s:beans/s:bean";
			XPath xpath = servicesIndexDom.createXPath(serviceXPath);
			xpath.setNamespaceURIs(ns);
			 
			List<Node> serviceNodes = xpath.selectNodes(servicesIndexDom);
			if (serviceNodes != null){
				for (int j=0;j < serviceNodes.size();j++){
					Element element = (Element)serviceNodes.get(j);
					String id = element.attributeValue("id");
					if (!id.endsWith("Target")){
						
						if (this.serviceIndexs.containsKey(id)){
							System.err.println("maybe service " + id + " is duplicate definded !");
						}
						
						this.serviceIndexs.put(id, moduleName);
					}
				}
			}	
		}
		
		String portletModulePath = webInfPath+File.separator+"modules"+File.separator+moduleName+File.separator+"PortletModule.xml";
		File portletModule = new File(portletModulePath);
		if(portletModule.exists()){
			Document portletIndexDom = XmlUtil.readDocument(portletModulePath);
			Map<String, String> ns = new HashMap<String, String>();  
		    ns.put("s", "http://java.sun.com/xml/ns/portlet/portlet-app_2_0.xsd");  
			String serviceXPath = "//s:portlet-app/s:portlet";
			XPath xpath = portletIndexDom.createXPath(serviceXPath);
			xpath.setNamespaceURIs(ns);
			
			List<Node> portletNodes = xpath.selectNodes(portletIndexDom);
			if (portletNodes != null){
				for (int j=0;j < portletNodes.size();j++){
					Element element = (Element)portletNodes.get(j);
					String id = element.attributeValue("id");
					if (this.portletIndexs.containsKey(id)){
						System.err.println("maybe portlet " + id + " is duplicate definded !");
					}
					this.portletIndexs.put(id, moduleName);
				}
			}
		}
	}
	
	private String getWebInfPath(String fileName) throws URISyntaxException{
		URI portletURI = this.appClassLoader.getResource(fileName).toURI();
		File porltetContextFile = new File(portletURI);
		String webInfPath = porltetContextFile.getParentFile().getParentFile().getAbsolutePath();
		return webInfPath;
	}
	
	public String getServiceModule(String serviceId){
		return serviceIndexs.get(serviceId);
	}
	
	public String getHandlerModule(String handlerId){
		return handlerIndexs.get(handlerId);
	}
	
	public String getPortletModule(String portletId){
		return portletIndexs.get(portletId);
	}	

	public List<String> getPortletIdList(){
		List<String> result = new ArrayList<String>();
		try {
			Iterator<String> iter = this.portletIndexs.keySet().iterator();
			while(iter.hasNext()){
				String portletId = iter.next();
				result.add(portletId);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public boolean load(String moduleName){
		boolean result = false;
		ClassLoaderFactory classLoaderFactory = ClassLoaderFactory.instance(appClassLoader);
		classLoaderFactory.createModuleClassLoader(moduleName);
		try {
			String webInfPath = getWebInfPath("HandlerContext.xml");
			this.initIndexs(webInfPath, moduleName);
			result = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public boolean unload(String moduleName){
		boolean result = false;
		ClassLoaderFactory classLoaderFactory = ClassLoaderFactory.instance(appClassLoader);
		ClassLoader moduleClassLoader = classLoaderFactory.rejectModuleClassLoader(moduleName);
		moduleClassLoader = null;

		HashMap<String,String> cleanedServiceIndexs = new HashMap<String,String>();
		Iterator<String> serviceKeys = this.serviceIndexs.keySet().iterator();
		while (serviceKeys.hasNext()){
			String key = serviceKeys.next();
			String tempModuleName = this.serviceIndexs.get(key);
			if (!moduleName.equals(tempModuleName)){
				cleanedServiceIndexs.put(key, tempModuleName);
			}
		}
		this.serviceIndexs.clear();
		this.serviceIndexs.putAll(cleanedServiceIndexs);
		
		HashMap<String,String> cleanedHandlerIndexs = new HashMap<String,String>();
		Iterator<String> handlerKeys = this.handlerIndexs.keySet().iterator();
		while (handlerKeys.hasNext()){
			String key = handlerKeys.next();
			String tempModuleName = this.handlerIndexs.get(key);
			if (!moduleName.equals(tempModuleName)){
				cleanedHandlerIndexs.put(key, tempModuleName);
			}
		}
		this.handlerIndexs.clear();
		this.handlerIndexs.putAll(cleanedHandlerIndexs);
		
		HashMap<String,String> cleanedPortletIndexs = new HashMap<String,String>();
		Iterator<String> portletKeys = this.portletIndexs.keySet().iterator();
		while (portletKeys.hasNext()){
			String key = portletKeys.next();
			String tempModuleName = this.portletIndexs.get(key);
			if (!moduleName.equals(tempModuleName)){
				cleanedPortletIndexs.put(key, tempModuleName);
			}
		}
		this.portletIndexs.clear();
		this.portletIndexs.putAll(cleanedPortletIndexs);
		
		System.gc();
		result = true;
		return result;
	}
	
	public boolean reload(String moduleName){
		boolean result = false;
		
		result = this.unload(moduleName);
		if (result){
			result = this.load(moduleName);			
		}
		return result;
	}	
}
