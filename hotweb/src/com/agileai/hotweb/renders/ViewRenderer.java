package com.agileai.hotweb.renders;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.agileai.hotweb.controller.core.BaseHandler;

abstract public class ViewRenderer {
	public static final String PAGE_BEAN_KEY = "pageBean";
	
	protected BaseHandler handler = null;
	protected Logger log = Logger.getLogger(this.getClass());
	
	public BaseHandler getHandler() {
		return handler;
	}
	public void setHandler(BaseHandler handler) {
		this.handler = handler;
	}
	abstract public void executeRender(HttpServlet httpServlet,HttpServletRequest request,HttpServletResponse response) throws Exception;	
}
