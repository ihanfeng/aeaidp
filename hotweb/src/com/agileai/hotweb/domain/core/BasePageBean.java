package com.agileai.hotweb.domain.core;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.log4j.Logger;

import com.agileai.domain.DataMap;
import com.agileai.util.MapUtil;
import com.agileai.util.StringUtil;

public class BasePageBean implements Serializable{
	private static final long serialVersionUID = -1371246813532709105L;
	
	protected Logger log = Logger.getLogger(this.getClass());
	private String handlerId = null;
	private String handlerURL = null;
	protected String moduleName = null;
	private DataMap attributesContainer = new DataMap();	
	private HashMap<String,Object> sessionAttributes = new HashMap<String,Object>();
	
	public BasePageBean(){
	}
	
	public void setAttribute(String key,Object value){
		this.attributesContainer.put(key,value);
	}
	
	public Object getAttribute(String key){
		return attributesContainer.get(key);
	}
	
	public Object getAttribute(String key,Object defaultValue){
		Object result = attributesContainer.get(key);
		if (result == null){
			result = defaultValue;
		}
		return result;
	}	
	
	public String getStringValue(String key){
		return attributesContainer.stringValue(key);
	}
	
	public String getStringValue(String key,String defaultValue){
		String result = attributesContainer.stringValue(key);
		if (StringUtil.isNullOrEmpty(result)){
			result = defaultValue;
		}
		return result;
	}
	
	public int getIntValue(String key){
		return attributesContainer.getInt(key);
	}
	
	public boolean getBoolValue(String key){
		return attributesContainer.getBoolean(key);
	}
	
	public Object getSessionAttribute(String key){
		return sessionAttributes.get(key);
	}
	
	public Object getSessionAttribute(String key,Object defaultValue){
		Object result = sessionAttributes.get(key);
		if (result == null){
			result = defaultValue;
		}
		return result;
	}
	
	@SuppressWarnings("rawtypes")
	public void setAttributes(DataMap params) {
		if (!MapUtil.isNullOrEmpty(params)){
			Iterator iter = params.keySet().iterator();
			while(iter.hasNext()){
				String key = (String)iter.next();
				Object value = params.get(key);
				this.setAttribute(key, value);
			}
		}
	}
	
	public String getHandlerId() {
		return handlerId;
	}
	
	public void setHandlerId(String handlerId) {
		this.handlerId = handlerId;
	}
	
	public String value(Object temp){
		String result = null;
		if (temp == null || "null".equals(String.valueOf(temp))){
			result = "";
		}
		else{
			result = String.valueOf(temp);
		}
		return result;
	}
	public String label(Object temp){
		String result = null;
		if (temp == null || String.valueOf(temp).trim().equals("")){
			result = "&nbsp;";
		}
		else{
			result = String.valueOf(temp);
		}
		return result;
	}
	public boolean isValid(String value){
		return !(value == null || value.equals("null") || value.trim().equals(""));
	}
	public String getHandlerURL() {
		return handlerURL;
	}
	public void setHandlerURL(String handlerURL) {
		this.handlerURL = handlerURL;
	}
	public DataMap getAttributesContainer() {
		return attributesContainer;
	}
	public void setAttributesContainer(DataMap attributesContainer) {
		this.attributesContainer = attributesContainer;
	}
	public void setSessionAttributes(HashMap<String,Object> sessionAttributes) {
		if (sessionAttributes != null)
		this.sessionAttributes = sessionAttributes;
	}
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}	
}