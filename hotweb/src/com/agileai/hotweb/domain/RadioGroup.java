package com.agileai.hotweb.domain;

import java.util.ArrayList;
import java.util.List;

import com.agileai.domain.DataMap;
import com.agileai.util.StringUtil;

public class RadioGroup {
	private String name = null;
	private String alias = null;
	private List<FormRadio> radioGroup = new ArrayList<FormRadio>();
	private int spaceCount = 3;
	private List<RadioEvent> events = new ArrayList<RadioEvent>();
	
	public RadioGroup(String name,FormSelect formSelect){
		this.name = name;
		this.initRadioGroup(formSelect);
	}
	private void initRadioGroup(FormSelect formSelect){
		int count = formSelect.size();
        for(int i = 1 ;i<= count;i++){
            DataMap heRow = (DataMap)formSelect.get(String.valueOf(i));
            String id = String.valueOf(heRow.get(formSelect.keyColumnName));
			if (formSelect.excludeKeys.contains(id)){
				continue;
			}
            String value = String.valueOf(heRow.get(formSelect.valueColumnName));
            FormRadio radio = new FormRadio();
            radio.setLabel(value);
            radio.setValue(id);
            if (id.equals(formSelect.seletedValue))
            	radio.setChecked(true);
            else
            	radio.setChecked(false);
            radioGroup.add(radio); 
        }
	}
	public RadioGroup addEvent(String code,String eventType,String scriptHandler){
		RadioEvent radioEvent = new RadioEvent();
		radioEvent.setCode(code);
		radioEvent.setEventType(eventType);
		radioEvent.setScriptHandler(scriptHandler);
		this.events.add(radioEvent);
		return this;
	}
	public RadioGroup addSpaceCount(int spaceCount){
		this.spaceCount = spaceCount;
		return this;
	}
	public String toString(){
		String result = "";
		StringBuffer text = new StringBuffer();
		String radioName = this.name;
		if (!StringUtil.isNullOrEmpty(alias)){
			radioName = this.alias;
		}
		for (int i=0;i < radioGroup.size();i++){
			FormRadio radio = radioGroup.get(i);
			text.append("<input type=\"radio\" name=\"").append(radioName).append("\"");
			for (int j=0;j < events.size();j++){
				RadioEvent event = events.get(j);
				String code = event.getCode();
				if (StringUtil.isNullOrEmpty(code) || code.equals(radio.getValue())){
					addEventHtml(text, event);
				}
			}
			text.append(" id=\"").append(radioName).append(i+"\"");
			text.append(" value=\"").append(radio.getValue()).append("\"");
			if (radio.isChecked()){
				text.append(" checked=\"checked\"");	
			}
			text.append(" />&nbsp;").append(radio.getLabel());
			if (i < radioGroup.size()-1){
				for (int j=0;j < spaceCount;j++){
					text.append("&nbsp;");
				}
			}
		}
		result = text.toString();
		return result;
	}
	private void addEventHtml(StringBuffer text,RadioEvent event){
		String eventType = event.getEventType();
		String scriptHandler = event.getScriptHandler();
		if (eventType.toLowerCase().startsWith("on")){
			text.append(" ").append(eventType).append("=\"").append(scriptHandler).append("\"");
		}else{
			text.append(" on").append(eventType).append("=\"").append(scriptHandler).append("\"");
		}
	}
	public RadioGroup addAlias(String alias) {
		this.alias = alias;
		return this;
	}
}
class FormRadio{
	private String label = null;
	private String value = null;
	private boolean checked = false;
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public boolean isChecked() {
		return checked;
	}
	public void setChecked(boolean checked) {
		this.checked = checked;
	}
}
class RadioEvent{
	private String code = "";
	private String eventType = "";
	private String scriptHandler = "";
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	public String getScriptHandler() {
		return scriptHandler;
	}
	public void setScriptHandler(String scriptHandler) {
		this.scriptHandler = scriptHandler;
	}
}