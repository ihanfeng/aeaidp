package com.agileai.miscdp.hotweb.domain.treecontent;

import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.agileai.miscdp.DeveloperConst;
import com.agileai.miscdp.hotweb.domain.BaseFuncModel;
import com.agileai.miscdp.hotweb.domain.ListTabColumn;
import com.agileai.miscdp.hotweb.domain.PageFormField;
import com.agileai.miscdp.hotweb.domain.PageParameter;
import com.agileai.miscdp.util.MiscdpUtil;
import com.agileai.hotweb.model.EditTableType;
import com.agileai.hotweb.model.ListTableType;
import com.agileai.hotweb.model.ListTableType.Row.Column;
import com.agileai.hotweb.model.QueryBarType;
import com.agileai.hotweb.model.treecontent.TreeContentFuncModelDocument;
import com.agileai.hotweb.model.treecontent.TreeContentFuncModelDocument.TreeContentFuncModel.BaseInfo;
import com.agileai.hotweb.model.treecontent.TreeContentFuncModelDocument.TreeContentFuncModel.ContentEditView;
import com.agileai.hotweb.model.treecontent.TreeContentFuncModelDocument.TreeContentFuncModel.ContentListTabArea;
import com.agileai.hotweb.model.treecontent.TreeContentFuncModelDocument.TreeContentFuncModel.TreeEditView;
/**
 * 树及内容操作功能模型
 */
public class TreeContentFuncModel extends BaseFuncModel{
	public static enum TreeEditMode{
		Popup,Tabed;
	}
	public static class Template{
		public static String List = "List";
		public static String ListTabedIncludeBaseTabed = "ListTabedIncludeBaseTabed";
		public static String ListTabedIncludeBasePoped = "ListTabedIncludeBasePoped";
	}
	
	private String pkGenPolicy = "";
	private String listHandlerId="";
	private String selectTreeHandlerId="";
	private String listHandlerClass="";
	private String selectTreeHandlerClass="";
	private String treeEditHandlerId="";
	private String treeEditHandlerClass="";
	
	private String treeTableName = "";
	private String rootColumnId = "";
	private String columnIdField = "";
	private String columnNameField = "";
	private String columnParentIdField = "";
	private String columnSortField = "";
	private boolean isManageTree = true; 
	private String treeEditMode = "Tabed";
	
	private List<ContentTableInfo> contentTableInfoList = new ArrayList<ContentTableInfo>();
	private List<PageFormField> treeEditFormFields = new ArrayList<PageFormField>();

	private HashMap<String,List<PageParameter>> contentPageParametersMap = new HashMap<String,List<PageParameter>>();
	private HashMap<String,List<PageFormField>> contentEditFormFieldsMap = new HashMap<String,List<PageFormField>>();
	private HashMap<String,List<ListTabColumn>> contentListTableColumnsMap = new HashMap<String,List<ListTabColumn>>();
	
	public TreeContentFuncModel(){
		this.editorId = DeveloperConst.TREECONTENT_EDITOR_ID;
	}
	public TreeContentFuncModel(String funcModelDefin){
		
	}
	
	public List<PageFormField> getTreeEditFormFields() {
		return treeEditFormFields;
	}
	public String getTreeTableName() {
		return treeTableName;
	}
	public void setTreeTableName(String table) {
		this.treeTableName = table;
	}

	public void buildFuncModel(String funcDefine){
		TreeContentFuncModel funcModel = this;
		try {
			Reader reader = new StringReader(funcDefine);
			TreeContentFuncModelDocument funcModelDocument = TreeContentFuncModelDocument.Factory.parse(reader);
			
			BaseInfo baseInfo = funcModelDocument.getTreeContentFuncModel().getBaseInfo();
			funcModel.setTreeTableName(baseInfo.getTreeTableName());
			funcModel.setListHandlerId(baseInfo.getHandler().getListHandlerId());
			funcModel.setListHandlerClass(baseInfo.getHandler().getListHandlerClass());
			
			funcModel.setSelectTreeHandlerClass(baseInfo.getHandler().getSelectTreeHandlerClass());
			funcModel.setSelectTreeHandlerId(baseInfo.getHandler().getSelectTreeHandlerId());
			
			funcModel.setTreeEditHandlerClass(baseInfo.getHandler().getTreeEditHandlerClass());
			funcModel.setTreeEditHandlerId(baseInfo.getHandler().getTreeEditHandlerId());
			
			funcModel.setManageTree(baseInfo.getIsManageTree());
			funcModel.setTreeEditMode(baseInfo.getTreeEditMode().toString());
			
			
			funcModel.setServiceId(baseInfo.getService().getServiceId());
			funcModel.setImplClassName(baseInfo.getService().getImplClassName());
			funcModel.setInterfaceName(baseInfo.getService().getInterfaceName());
			funcModel.setListJspName(baseInfo.getMainListJspName());
			funcModel.setTemplate(baseInfo.getTemplate());
			
			funcModel.setColumnIdField(baseInfo.getColumnIdField());
			funcModel.setColumnNameField(baseInfo.getColumnNameField());
			funcModel.setColumnParentIdField(baseInfo.getColumnParentIdField());
			funcModel.setColumnSortField(baseInfo.getColumnSortField());
			funcModel.setRootColumnId(baseInfo.getRootColumnId());
			
			List<ContentTableInfo> contentTableInfos = funcModel.getContentTableInfoList();
			if (contentTableInfos.size() > 0){
				contentTableInfos.clear();
			}
			BaseInfo.ContentTableInfo[] tableInfos = baseInfo.getContentTableInfoArray();
			if (tableInfos != null){
				for (int i=0;i < tableInfos.length;i++){
					BaseInfo.ContentTableInfo tableInfo = tableInfos[i];
					ContentTableInfo contentTableInfo = new ContentTableInfo();
					contentTableInfo.setTabId(tableInfo.getTabId());
					contentTableInfo.setTabName(tableInfo.getTabName());
					contentTableInfo.setTableName(tableInfo.getTableName());
					contentTableInfo.setPrimaryKey(tableInfo.getPrimaryKey());
					contentTableInfo.setColField(tableInfo.getColField());
					contentTableInfo.setRelTableName(tableInfo.getRelTableName());
					contentTableInfo.setTableMode(tableInfo.getTableMode().toString());
					contentTableInfo.setQueryListSql(tableInfo.getQueryListSql());
					
					contentTableInfos.add(contentTableInfo);
				}
			}
			
			ContentListTabArea[] contentListTabAreaArray = funcModelDocument.getTreeContentFuncModel().getContentListTabAreaArray();
			if (contentListTabAreaArray != null){
				for (int i=0;i < contentListTabAreaArray.length;i++){
					ContentListTabArea contentListTabArea = contentListTabAreaArray[i];
					String tabId = contentListTabArea.getTabId();
					
					ListTableType listTableType = contentListTabArea.getListTableArea();
					if (listTableType != null){
						List<ListTabColumn> listTabColumns = funcModel.getContentListTableColumnsMap().get(tabId);
						if (listTabColumns == null){
							listTabColumns = new ArrayList<ListTabColumn>();
							funcModel.getContentListTableColumnsMap().put(tabId,listTabColumns);						
						}
						Column[] columns = listTableType.getRow().getColumnArray();
						processListTableColumns(columns, listTabColumns);
					}
					
					QueryBarType queryBarType = contentListTabArea.getParameterArea();
					if (queryBarType != null){
						List<PageParameter> pageParameters = funcModel.getContentPageParametersMap().get(tabId);
						if (pageParameters == null){
							pageParameters = new ArrayList<PageParameter>();
							funcModel.getContentPageParametersMap().put(tabId, pageParameters);	
						}
						com.agileai.hotweb.model.FormObjectDocument.FormObject[] formObjects = queryBarType.getFormObjectArray();
						processPageParamters(formObjects, pageParameters);
					}
				}
			}
			
			ContentEditView[] contentEditViews = funcModelDocument.getTreeContentFuncModel().getContentEditViewArray();
			if (contentEditViews != null){
				for (int i=0;i < contentEditViews.length;i++){
					ContentEditView contentEditView = contentEditViews[i];
					String tabId = contentEditView.getTabId();
					
					EditTableType editTableType = contentEditView.getDetailEditArea();
					com.agileai.hotweb.model.FormObjectDocument.FormObject[] editFormObjects = editTableType.getFormObjectArray();
					
					List<PageFormField> pageFormFields = funcModel.getContentEditFormFieldsMap().get(tabId);
					if (pageFormFields == null){
						pageFormFields = new ArrayList<PageFormField>();
						funcModel.getContentEditFormFieldsMap().put(tabId, pageFormFields);						
					}
					processPageFormFields(editFormObjects, pageFormFields);
				}
			}
			TreeEditView treeEditView = funcModelDocument.getTreeContentFuncModel().getTreeEditView();
			if (treeEditView != null){
				EditTableType treeEditTableType = funcModelDocument.getTreeContentFuncModel().getTreeEditView().getTreeEditArea();
				com.agileai.hotweb.model.FormObjectDocument.FormObject[] editFormObjects = treeEditTableType.getFormObjectArray();
				processPageFormFields(editFormObjects, funcModel.getTreeEditFormFields());
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public String getListHandlerId() {
		return listHandlerId;
	}
	public void setListHandlerId(String listHandlerId) {
		this.listHandlerId = listHandlerId;
	}

	public String getListHandlerClass() {
		return listHandlerClass;
	}
	public void setListHandlerClass(String listHandlerClass) {
		this.listHandlerClass = listHandlerClass;
	}

	public String getPkGenPolicy() {
		return pkGenPolicy;
	}
	public void setPkGenPolicy(String pkGenPolicy) {
		this.pkGenPolicy = pkGenPolicy;
	}
	public List<ContentTableInfo> getContentTableInfoList() {
		return contentTableInfoList;
	}
	public HashMap<String, List<PageFormField>> getContentEditFormFieldsMap() {
		return contentEditFormFieldsMap;
	}
	public HashMap<String, List<ListTabColumn>> getContentListTableColumnsMap() {
		return contentListTableColumnsMap;
	}
	public HashMap<String, List<PageParameter>> getContentPageParametersMap() {
		return contentPageParametersMap;
	}
	public String getColumnIdField() {
		return columnIdField;
	}
	public void setColumnIdField(String columnIdField) {
		this.columnIdField = columnIdField;
	}
	public String getColumnNameField() {
		return columnNameField;
	}
	public void setColumnNameField(String columnNameField) {
		this.columnNameField = columnNameField;
	}
	public String getColumnParentIdField() {
		return columnParentIdField;
	}
	public void setColumnParentIdField(String columnParentIdField) {
		this.columnParentIdField = columnParentIdField;
	}
	public String getColumnSortField() {
		return columnSortField;
	}
	public void setColumnSortField(String columnSortField) {
		this.columnSortField = columnSortField;
	}
	public boolean isManageTree() {
		return isManageTree;
	}
	public void setManageTree(boolean isManageTree) {
		this.isManageTree = isManageTree;
	}
	public String getRootColumnId() {
		return rootColumnId;
	}
	public void setRootColumnId(String rootColumnId) {
		this.rootColumnId = rootColumnId;
	}
	public String getSelectTreeHandlerClass() {
		return selectTreeHandlerClass;
	}
	public void setSelectTreeHandlerClass(String selectTreeHandlerClass) {
		this.selectTreeHandlerClass = selectTreeHandlerClass;
	}
	public String getSelectTreeHandlerId() {
		return selectTreeHandlerId;
	}
	public void setSelectTreeHandlerId(String selectTreeHandlerId) {
		this.selectTreeHandlerId = selectTreeHandlerId;
	}
	public String getTreeEditMode() {
		return treeEditMode;
	}
	public void setTreeEditMode(String treeEditMode) {
		this.treeEditMode = treeEditMode;
	}
	public String getTreeEditJspName() {
		return MiscdpUtil.getValidName(this.treeTableName)+"Edit.jsp";
	}
	public String getTreePickJspName() {
		String interfaceName = this.interfaceName.substring(this.interfaceName.lastIndexOf(".")+1,this.interfaceName.length());
		return interfaceName+"TreePick.jsp";
	}
	public String getTreeEditHandlerClass() {
		return treeEditHandlerClass;
	}
	public void setTreeEditHandlerClass(String treeEditHandlerClass) {
		this.treeEditHandlerClass = treeEditHandlerClass;
	}
	public String getTreeEditHandlerId() {
		return treeEditHandlerId;
	}
	public void setTreeEditHandlerId(String treeEditHandlerId) {
		this.treeEditHandlerId = treeEditHandlerId;
	}
	public String getSqlNamespace() {
		String sqlNameSpaceName = this.interfaceName.substring(this.interfaceName.lastIndexOf(".")+1,this.interfaceName.length());
		return sqlNameSpaceName;
	}
	
	public String getContentEditHandlerId(String contentTableName){
		return MiscdpUtil.getValidName(contentTableName)+"Edit";
	}
	
	public String getContentEditHandlerClass(String contentTableName){
		return MiscdpUtil.getValidName(contentTableName)+"EditHandler";
	}
	
	public String getContentEditJspName(String contentTableName){
		return MiscdpUtil.getValidName(contentTableName)+"Edit.jsp";
	}
}
