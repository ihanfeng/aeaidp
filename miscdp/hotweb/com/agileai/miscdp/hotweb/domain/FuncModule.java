﻿package com.agileai.miscdp.hotweb.domain;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPersistableElement;

import com.agileai.miscdp.DeveloperConst;
/**
 * 功能模块
 */
public class FuncModule implements IEditorInput{
	protected String moduleName = "";
	protected String moduleCode = "";
	protected String moduleId = "";
	protected String projectName = "";
	private boolean hasChidren = false;
	
	protected String editorId = DeveloperConst.MODULE_EDITOR_ID;
	public boolean exists() {
		return false;
	}
	public ImageDescriptor getImageDescriptor() {
		return null;
	}
	public IPersistableElement getPersistable() {
		return null;
	}
	public static String getTipText(String funcId,String funcName){
		return "模块标识："+funcId+"  "+"模块名称："+funcName;
	}
	public String getToolTipText() {
		return getTipText(moduleId, moduleName);
	}
	@SuppressWarnings({"rawtypes" })
	public Object getAdapter(Class adapter) {
		return null;
	}
	public String getName() {
		return this.moduleName;
	}
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String funcName){
		this.moduleName = funcName;
	}
	public String getModuleId() {
		return moduleId;
	}
	public void setModuleId(String funcId) {
		this.moduleId = funcId;
	}
	public String getModuleCode() {
		return moduleCode;
	}
	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}
	public String getEditorId() {
		return editorId;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public boolean isHasChidren() {
		return hasChidren;
	}
	public void setHasChidren(boolean hasChidren) {
		this.hasChidren = hasChidren;
	}
}
