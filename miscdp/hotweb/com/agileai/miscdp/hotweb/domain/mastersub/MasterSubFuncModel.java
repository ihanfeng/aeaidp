package com.agileai.miscdp.hotweb.domain.mastersub;

import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.agileai.miscdp.DeveloperConst;
import com.agileai.miscdp.hotweb.database.DBManager;
import com.agileai.miscdp.hotweb.domain.BaseFuncModel;
import com.agileai.miscdp.hotweb.domain.ListTabColumn;
import com.agileai.miscdp.hotweb.domain.PageFormField;
import com.agileai.miscdp.hotweb.domain.PageParameter;
import com.agileai.util.StringUtil;
import com.agileai.hotweb.model.FormObjectDocument.FormObject;
import com.agileai.hotweb.model.mastersub.MasterSubFuncModelDocument;
import com.agileai.hotweb.model.mastersub.MasterSubFuncModelDocument.MasterSubFuncModel.EditMainView.SubEntryEditArea;
import com.agileai.hotweb.model.mastersub.MasterSubFuncModelDocument.MasterSubFuncModel.EditMainView.SubListViewArea;
/**
 * 主从表操作功能模型
 */
public class MasterSubFuncModel extends BaseFuncModel{
	public static final String TEMPLATE_BASETOPED = "baseToped";
	public static final String TEMPLATE_BASETABED = "baseTabed";
	
	public static final String BAS_TAB_ID = "_base";
	
	protected String tableName = "";
	protected String detailJspName = "";
	protected String listTitle = "";
	protected String detailTitle = "";
	protected List<String> rsIdColumns = new ArrayList<String>();
	protected boolean exportCsv = true;
	protected boolean exportXls = true;
	protected boolean sortAble = true;
	protected boolean pagination = true;
	protected String listHandlerId="";
	protected String editHandlerId="";
	protected String listHandlerClass="";
	protected String editHandlerClass="";
	protected String tablePK = "";
	
	protected List<PageParameter> pageParameters = new ArrayList<PageParameter>();
	protected List<ListTabColumn> listTableColumns = new ArrayList<ListTabColumn>();
	protected List<PageFormField> pageFormFields = new ArrayList<PageFormField>();
	
	protected List<SubTableConfig> subTableConfigs = new ArrayList<SubTableConfig>();
	
	private HashMap<String,List<PageFormField>> subEntryEditFormFieldsMap = new HashMap<String,List<PageFormField>>();
	private HashMap<String,List<ListTabColumn>> subListTableColumnsMap = new HashMap<String,List<ListTabColumn>>();
	private HashMap<String,List<PageFormField>> subPboxFormFieldsMap = new HashMap<String,List<PageFormField>>();
	
	public MasterSubFuncModel(){
		this.editorId = DeveloperConst.MASTERSUB_EDITOR_ID;
		this.template = TEMPLATE_BASETOPED;
	}
	public List<PageParameter> addPageParameter(PageParameter pageParameter){
		this.pageParameters.add(pageParameter);
		return this.pageParameters;
	}
	public List<ListTabColumn> addListTableColumn(ListTabColumn listTabColumn){
		this.listTableColumns.add(listTabColumn);
		return this.listTableColumns;
	}
	public List<PageFormField> addPageFormField(PageFormField pagFormField){
		this.pageFormFields.add(pagFormField);
		return this.pageFormFields;
	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String table) {
		this.tableName = table;
	}
	public String getDetailJspName() {
		return detailJspName;
	}
	public void setDetailJspName(String detailJspName) {
		if (detailJspName != null){
			this.detailJspName = detailJspName;			
		}
	}
	public String getListTitle() {
		return listTitle;
	}
	public void setListTitle(String listTitle) {
		this.listTitle = listTitle;
	}
	public String getDetailTitle() {
		return detailTitle;
	}
	public void setDetailTitle(String detailTitle) {
		this.detailTitle = detailTitle;
	}
	public List<ListTabColumn> getListTableColumns() {
		return listTableColumns;
	}
	public List<PageParameter> getPageParameters() {
		return pageParameters;
	}
	public List<PageFormField> getPageFormFields() {
		return pageFormFields;
	}
	public boolean isExportCsv() {
		return exportCsv;
	}
	public void setExportCsv(boolean exportCsv) {
		this.exportCsv = exportCsv;
	}
	public boolean isExportXls() {
		return exportXls;
	}
	public void setExportXls(boolean exportXls) {
		this.exportXls = exportXls;
	}
	public boolean isSortAble() {
		return sortAble;
	}
	public void setSortAble(boolean sortAble) {
		this.sortAble = sortAble;
	}
	public boolean isPagination() {
		return pagination;
	}
	public void setPagination(boolean pagination) {
		this.pagination = pagination;
	}
	public List<String> getRsIdColumns() {
		return rsIdColumns;
	}
	public void buildFuncModel(String funcDefine){
		MasterSubFuncModel msFuncModel = this;
		try {
			Reader reader = new StringReader(funcDefine);
			MasterSubFuncModelDocument funcModelDocument = MasterSubFuncModelDocument.Factory.parse(reader);
			
			MasterSubFuncModelDocument.MasterSubFuncModel.BaseInfo baseInfo = funcModelDocument.getMasterSubFuncModel().getBaseInfo();
			msFuncModel.setTableName(baseInfo.getTableName());
			msFuncModel.setTemplate(baseInfo.getTemplate().toString());
			msFuncModel.setListHandlerId(baseInfo.getHandler().getListHandlerId());
			msFuncModel.setListHandlerClass(baseInfo.getHandler().getListHandlerClass());
			msFuncModel.setEditHandlerId(baseInfo.getHandler().getEditHandlerId());
			msFuncModel.setEditHandlerClass(baseInfo.getHandler().getEditHandlerClass());
			
			msFuncModel.setServiceId(baseInfo.getService().getServiceId());
			msFuncModel.setImplClassName(baseInfo.getService().getImplClassName());
			msFuncModel.setInterfaceName(baseInfo.getService().getInterfaceName());
			msFuncModel.setListJspName(baseInfo.getListJspName());
			msFuncModel.setDetailJspName(baseInfo.getDetailJspName());
			msFuncModel.setListTitle(baseInfo.getListTitle());
			msFuncModel.setDetailTitle(baseInfo.getDetailTitle());
            msFuncModel.setListSql(baseInfo.getQueryListSql());
            
            MasterSubFuncModelDocument.MasterSubFuncModel.BaseInfo.SubTableInfo[] subTableInfoArray = baseInfo.getSubTableInfoArray();
            HashMap<String,SubTableConfig> subTableConfigMap = new HashMap<String,SubTableConfig>();
            msFuncModel.getSubTableConfigs().clear();
            for (int i=0;i < subTableInfoArray.length;i++){
            	MasterSubFuncModelDocument.MasterSubFuncModel.BaseInfo.SubTableInfo subTableInfo = subTableInfoArray[i];
            	SubTableConfig subTableConfig = new SubTableConfig();
            	subTableConfig.setEditMode(subTableInfo.getEditMode().toString());
            	subTableConfig.setForeignKey(subTableInfo.getForeignKey());
            	subTableConfig.setSortField(subTableInfo.getSortField());
            	subTableConfig.setPrimaryKey(subTableInfo.getPrimaryKey());
            	subTableConfig.setTabTitile(subTableInfo.getTabName());
            	subTableConfig.setQueryListSql(subTableInfo.getQueryListSql());
            	subTableConfig.setTableName(subTableInfo.getTableName());
            	msFuncModel.getSubTableConfigs().add(subTableConfig);
            	
            	subTableConfigMap.put(subTableConfig.getSubTableId(), subTableConfig);
            }
            
            MasterSubFuncModelDocument.MasterSubFuncModel.ListView listView = funcModelDocument.getMasterSubFuncModel().getListView();
			String rsIdColumnTemp  = listView.getListTableArea().getRow().getRsIdColumn();
			msFuncModel.getRsIdColumns().clear();
			if (!StringUtil.isNullOrEmpty(rsIdColumnTemp)){
				String[] rsIdColumnArray = rsIdColumnTemp.split(",");
				for (int i=0;i < rsIdColumnArray.length;i++){
					msFuncModel.getRsIdColumns().add(rsIdColumnArray[i]);
				}
			}

			com.agileai.hotweb.model.FormObjectDocument.FormObject[] formObjects = listView.getParameterArea().getFormObjectArray();
			processPageParamters(formObjects, pageParameters);
			
			com.agileai.hotweb.model.ListTableType listTableType = listView.getListTableArea();
			msFuncModel.setExportCsv(listTableType.getExportCsv());
			msFuncModel.setExportXls(listTableType.getExportXls());
			msFuncModel.setSortAble(listTableType.getSortAble());
			msFuncModel.setPagination(listTableType.getPagination());
			String tableName = msFuncModel.getTableName();
			if (StringUtil.isNullOrEmpty(listTableType.getRow().getRsIdColumn())){
				DBManager dbManager = DBManager.getInstance(this.projectName);
				String[] pks = dbManager.getPrimaryKeys(tableName);
				String primaryKey = StringUtil.append(pks,",");
				listTableType.getRow().setRsIdColumn(primaryKey);
				msFuncModel.setTablePK(primaryKey);
			}

			com.agileai.hotweb.model.ListTableType.Row.Column[] columns = listTableType.getRow().getColumnArray();
			processListTableColumns(columns, msFuncModel.getListTableColumns());
			
			MasterSubFuncModelDocument.MasterSubFuncModel.EditMainView editMainView = funcModelDocument.getMasterSubFuncModel().getEditMainView();
			FormObject[] editFormObjects =editMainView.getMasterEditArea().getFormObjectArray();
			processPageFormFields(editFormObjects, msFuncModel.getPageFormFields());
			
			SubEntryEditArea[] subEntryEditAreas = editMainView.getSubEntryEditAreaArray();
			if (subEntryEditAreas != null){
				for (int i=0;i < subEntryEditAreas.length;i++){
					SubEntryEditArea subEntryEditArea = subEntryEditAreas[i];
					String subTableId = subEntryEditArea.getSubTableId();
					List<PageFormField> subEntryEditFormFields = msFuncModel.getSubEntryEditFormFields(subTableId);
					editFormObjects = subEntryEditArea.getFormObjectArray();
					processPageFormFields(editFormObjects, subEntryEditFormFields);
					msFuncModel.putSubEntryEditFormFields(subTableId, subEntryEditFormFields);
				}
			}
			SubListViewArea[] subListViewAreas = editMainView.getSubListViewAreaArray();
			if (subListViewAreas != null){
				for (int i=0;i < subListViewAreas.length;i++){
					SubListViewArea subListViewArea = subListViewAreas[i];
					String subTableId = subListViewArea.getSubTableId();
					List<ListTabColumn> subListTableColumns = msFuncModel.getSubListTableColumns(subTableId);
					columns = subListViewArea.getRow().getColumnArray();
					processListTableColumns(columns, subListTableColumns);
					msFuncModel.putSubListTableColumns(subTableId, subListTableColumns);
				}
			}
			
			MasterSubFuncModelDocument.MasterSubFuncModel.EditPboxView[] editBoxViews = funcModelDocument.getMasterSubFuncModel().getEditPboxViewArray();
			if (editBoxViews != null){
				for (int i =0;i < editBoxViews.length;i++){
					MasterSubFuncModelDocument.MasterSubFuncModel.EditPboxView editBoxView = editBoxViews[i];
					String subTableId = editBoxView.getSubTableId();
					SubTableConfig subTableConfig = subTableConfigMap.get(subTableId);
					String pboxJspName = editBoxView.getJspName();
					subTableConfig.setPboxJspName(pboxJspName);
					subTableConfig.setPboxHandlerId(editBoxView.getHandlerId());
					subTableConfig.setPboxHandlerClass(editBoxView.getHandlerClass());
					
					FormObject[] boxEditFormObjects =editBoxView.getSubPboxEditArea().getFormObjectArray();
					List<PageFormField> pageFormFields = msFuncModel.getSubPboxFormFields(subTableId);
					processPageFormFields(boxEditFormObjects,pageFormFields);				
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String getListHandlerId() {
		return listHandlerId;
	}
	public void setListHandlerId(String listHandlerId) {
		this.listHandlerId = listHandlerId;
	}
	public String getEditHandlerId() {
		return editHandlerId;
	}
	public void setEditHandlerId(String editHandlerId) {
		this.editHandlerId = editHandlerId;
	}
	public String getListHandlerClass() {
		return listHandlerClass;
	}
	public void setListHandlerClass(String listHandlerClass) {
		this.listHandlerClass = listHandlerClass;
	}
	public String getEditHandlerClass() {
		return editHandlerClass;
	}
	public void setEditHandlerClass(String editHandlerClass) {
		this.editHandlerClass = editHandlerClass;
	}
	public List<SubTableConfig> getSubTableConfigs() {
		return subTableConfigs;
	}
	public List<PageFormField> getSubEntryEditFormFields(String subTableId) {
		List<PageFormField> result = subEntryEditFormFieldsMap.get(subTableId)==null?new ArrayList<PageFormField>():subEntryEditFormFieldsMap.get(subTableId);
		subEntryEditFormFieldsMap.put(subTableId, result);
		return result;
	}
	public void putSubEntryEditFormFields(String subTableId,List<PageFormField> subEntryEditFormFields){
		subEntryEditFormFieldsMap.put(subTableId, subEntryEditFormFields);
	}
	public List<ListTabColumn> getSubListTableColumns(String subTableId) {
		List<ListTabColumn> result = subListTableColumnsMap.get(subTableId)==null?new ArrayList<ListTabColumn>():subListTableColumnsMap.get(subTableId);
		subListTableColumnsMap.put(subTableId, result);
		return result;
	}
	public void putSubListTableColumns(String subTableId,List<ListTabColumn> subListTableColumns){
		subListTableColumnsMap.put(subTableId, subListTableColumns);
	}
	public List<PageFormField> getSubPboxFormFields(String subTableId) {
		 List<PageFormField> result = subPboxFormFieldsMap.get(subTableId)==null?new ArrayList<PageFormField>():subPboxFormFieldsMap.get(subTableId);
		 subPboxFormFieldsMap.put(subTableId, result);
		 return result;
	}
	public void putSubPboxFormFields(String subTableId,List<PageFormField> subPboxFormFields){
		subPboxFormFieldsMap.put(subTableId, subPboxFormFields);
	}
	public String getTablePK() {
		return tablePK;
	}
	public void setTablePK(String tablePK) {
		this.tablePK = tablePK;
	}
	public String getSqlNamespace() {
		String sqlNameSpaceName = this.interfaceName.substring(this.interfaceName.lastIndexOf(".")+1,this.interfaceName.length());
		return sqlNameSpaceName;
	}
}
