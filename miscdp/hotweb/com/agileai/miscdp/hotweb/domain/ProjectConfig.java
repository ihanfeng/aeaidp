﻿package com.agileai.miscdp.hotweb.domain;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;

import com.agileai.miscdp.DeveloperConst;
import com.agileai.miscdp.hotweb.ui.wizards.HotwebProjectWizard;
import com.agileai.util.CryptionUtil;
import com.agileai.util.StringUtil;
import com.agileai.util.XmlUtil;
/**
 * 工程配置
 */
public class ProjectConfig {
	public static class ProjectType{
		public static final String JavaWebProject = "JavaWebProject";
		public static final String IntegrateWebProject = "IntegrateWebProject";
	}
	
	private String configFile = DeveloperConst.PROJECT_CFG_NAME;
	private String encoding = "utf-8";
	
	private String projectType = ProjectType.JavaWebProject;
	private String projectName = null;
	private String projectAlias = "";
	private String configAlias = "";
	private String mainPkg = null;
	private String driverUrl = null;
	private String driverClass = null;
	private String userId = null;
	private String userPwd = null;
	private String serverAddress = "";
	private String serverPort = "";
	private String serverUserId = "";
	private String serverUserPwd = "";
	
	private List<ServerConfig> serverConfigs = new ArrayList<ServerConfig>();
	private List<FuncList> funcLists = new ArrayList<FuncList>();
	
	private Document document = null;

	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getDriverUrl() {
		return driverUrl;
	}

	public void setDriverUrl(String driverUrl) {
		this.driverUrl = driverUrl;
	}
	public String getDriverClass() {
		return driverClass;
	}
	public void setDriverClass(String driverClass) {
		this.driverClass = driverClass;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserPwd() {
		return userPwd;
	}
	public void setUserPwd(String userPwd) {
		this.userPwd = userPwd;
	}
	public String getConfigFile() {
		return configFile;
	}
	public String getServerAddress() {
		return serverAddress;
	}
	public void setServerAddress(String serverAddress) {
		this.serverAddress = serverAddress;
	}
	public String getServerPort() {
		return serverPort;
	}
	public void setServerPort(String serverPort) {
		this.serverPort = serverPort;
	}
	public void setConfigFile(String configFile) {
		this.configFile = configFile;
	}
	public String getProjectType() {
		return projectType;
	}
	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}
	public String getMainPkg() {
		return mainPkg;
	}
	public void setMainPkg(String mainPkg) {
		this.mainPkg = mainPkg;
	}
	public String getServerUserId() {
		return serverUserId;
	}
	public void setServerUserId(String serverUserId) {
		this.serverUserId = serverUserId;
	}
	public String getServerUserPwd() {
		return serverUserPwd;
	}
	public void setServerUserPwd(String serverUserPwd) {
		this.serverUserPwd = serverUserPwd;
	}
	public String getProjectAlias() {
		if (StringUtil.isNullOrEmpty(projectAlias)){
			return this.projectName;
		}else{
			return projectAlias;			
		}
	}
	public void setProjectAlias(String projectAlias) {
		this.projectAlias = projectAlias;
	}
	
	public String getConfigAlias() {
		if (StringUtil.isNullOrEmpty(configAlias)){
			return "default";
		}else{
			return configAlias;			
		}
	}
	public void setConfigAlias(String configAlias) {
		this.configAlias = configAlias;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void initConfig(){
		String userPwdNodePath = "//HotwebModule/DBConfig/UserPwd";
		Node pwdNode = this.getDocument().selectSingleNode(userPwdNodePath);
		String encrptPassword = pwdNode.getText(); 
		String secretKey = HotwebProjectWizard.getSecurityKey();
		this.userPwd = CryptionUtil.decryption(encrptPassword, secretKey);
		
		String userNodePath = "//HotwebModule/DBConfig/UserId";
		Node userNode = this.getDocument().selectSingleNode(userNodePath);
		this.userId = userNode.getText();
		
		String driverNodePath = "//HotwebModule/DBConfig/DriverClass";
		Node driverNode = this.getDocument().selectSingleNode(driverNodePath);
		this.driverClass = driverNode.getText();
		
		String urlNodePath = "//HotwebModule/DBConfig/DriverUrl";
		Node urlNode = this.getDocument().selectSingleNode(urlNodePath);
		this.driverUrl = urlNode.getText();
		
		
		String configAliasNodePath = "//HotwebModule/ServerConfig/ConfigAlias";
		Node  configAliasNode = this.getDocument().selectSingleNode(configAliasNodePath);
		if (configAliasNode != null){
			this.configAlias = configAliasNode.getText();			
		}
		
		String projectAliasNodePath = "//HotwebModule/ServerConfig/ProjectAlias";
		Node projectAliasNode = this.getDocument().selectSingleNode(projectAliasNodePath);
		if (projectAliasNode != null){
			this.projectAlias = projectAliasNode.getText();			
		}	
		
		String serverAdressNodePath = "//HotwebModule/ServerConfig/ServerAddress";
		Node serverAdressNode = this.getDocument().selectSingleNode(serverAdressNodePath);
		if (serverAdressNode != null){
			this.serverAddress = serverAdressNode.getText();			
		}
		
		String serverPortNodePath = "//HotwebModule/ServerConfig/ServerPort";
		Node serverPortNode = this.getDocument().selectSingleNode(serverPortNodePath);
		if (serverPortNode != null){
			this.serverPort = serverPortNode.getText();			
		}
		
		String serverUserIdNodePath = "//HotwebModule/ServerConfig/ServerUserId";
		Node serverUserIdNode = this.getDocument().selectSingleNode(serverUserIdNodePath);
		if (serverUserIdNode != null){
			this.serverUserId = serverUserIdNode.getText();			
		}	
		String serverUserPwdNodePath = "//HotwebModule/ServerConfig/ServerUserPwd";
		Node serverUserPwdNode = this.getDocument().selectSingleNode(serverUserPwdNodePath);
		if (serverUserPwdNode != null){
			String encrptServerUserPwd = serverUserPwdNode.getText();	
			this.serverUserPwd = CryptionUtil.decryption(encrptServerUserPwd, secretKey);
		}	
		
		String projectTypeNodePath = "//HotwebModule/ProjectType";
		Node projectTypeNode = this.getDocument().selectSingleNode(projectTypeNodePath);
		if (projectTypeNode != null){
			this.projectType = projectTypeNode.getText();
		}
		
		String serverListNodesPath = "//HotwebModule/ServerList/ServerConfig";
		List serverListNodes = this.getDocument().selectNodes(serverListNodesPath);
		if (serverListNodes != null){
			for (int i=0;i < serverListNodes.size();i++){
				Element element = (Element)serverListNodes.get(i);
				if (element != null){
					ServerConfig tempServerConfig = new ServerConfig();
					Node tempConfigAliasNode = element.selectSingleNode(serverListNodesPath+"["+ (i+1)+"]"+"/ConfigAlias");
					if (tempConfigAliasNode != null){
						tempServerConfig.setConfigAlias(tempConfigAliasNode.getText());
					}

					Node tempProjectAliasNode = element.selectSingleNode(serverListNodesPath+"["+ (i+1)+"]"+"/ProjectAlias");
					if (tempProjectAliasNode != null){
						tempServerConfig.setProjectAlias(tempProjectAliasNode.getText());
					}
					
					Node tempServerAddressNode = element.selectSingleNode(serverListNodesPath+"["+ (i+1)+"]"+"/ServerAddress");
					if (tempServerAddressNode != null){
						tempServerConfig.setServerAddress(tempServerAddressNode.getText());
					}
					
					Node tempServerPortNode = element.selectSingleNode(serverListNodesPath+"["+ (i+1)+"]"+"/ServerPort");
					if (tempServerPortNode != null){
						tempServerConfig.setServerPort(tempServerPortNode.getText());
					}
					
					Node tempServerUserIdNode = element.selectSingleNode(serverListNodesPath+"["+ (i+1)+"]"+"/ServerUserId");
					if (tempServerUserIdNode != null){
						tempServerConfig.setServerUserId(tempServerUserIdNode.getText());
					}
					
					Node tempServerUserPwdNode = element.selectSingleNode(serverListNodesPath+"["+ (i+1)+"]"+"/ServerUserPwd");
					if (tempServerUserPwdNode != null){
						String encrptServerUserPwd = tempServerUserPwdNode.getText();	
						String decriptPass = CryptionUtil.decryption(encrptServerUserPwd, secretKey);
						tempServerConfig.setServerUserPwd(decriptPass);
					}		
					this.serverConfigs.add(tempServerConfig);
				}
			}
		}
		
		String funcListNodesPath = "//HotwebModule/FuncTree/FuncList";
		List funcListNodes = this.getDocument().selectNodes(funcListNodesPath);
		if (funcListNodes != null){
			for (int i=0;i < funcListNodes.size();i++){
				Element element = (Element)funcListNodes.get(i);
				FuncList funcList = new FuncList();
				funcList.setId(element.attributeValue("id"));
				funcList.setCode(element.attributeValue("name"));
				funcList.setName(element.attributeValue("code"));
				
				List<Element> funcNodeElements = element.elements();
				if (funcNodeElements != null){
					for (int j=0;j < funcNodeElements.size();j++){
						Element funcNodeElement = funcNodeElements.get(j);
						FuncNode funcNode = new FuncNode();
						funcNode.setId(funcNodeElement.attributeValue("id"));
						funcNode.setName(funcNodeElement.attributeValue("name"));
						funcNode.setSubPkg(funcNodeElement.attributeValue("subPkg"));
						funcNode.setType(funcNodeElement.attributeValue("type"));
						funcNode.setTypeName(funcNodeElement.attributeValue("typeName"));
						funcList.getFuncNodes().add(funcNode);
					}
				}
				this.funcLists.add(funcList);
			}
		}
		
		String mainPkgNodePath = "//HotwebModule/MainPkg";
		Node mainPkgNode = this.getDocument().selectSingleNode(mainPkgNodePath);
		this.mainPkg = mainPkgNode.getText();
	}
	
	public boolean isIntegrateWebProject(){
		return ProjectType.IntegrateWebProject.equals(this.projectType);
	}
	
    public Document getDocument(){
        Document result = null;
        try {
            if (this.document == null){
            	this.document = XmlUtil.readDocument(this.configFile);
            }
            result = document;
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }
    
    public void saveHotwebConfig(){
        Document document = XmlUtil.createDocument();
        Element rootElement = document.addElement("HotwebModule");
        Element projectType = rootElement.addElement("ProjectType");
        projectType.setText(this.projectType);
        
        Element mainPkg = rootElement.addElement("MainPkg");
        mainPkg.setText(this.mainPkg);
        
        Element dbConfig = rootElement.addElement("DBConfig");
        dbConfig.addElement("DriverUrl").setText(this.driverUrl);
        dbConfig.addElement("DriverClass").setText(this.driverClass);
        dbConfig.addElement("UserId").setText(this.userId);
        String secretKey = HotwebProjectWizard.getSecurityKey();
        String crytPassword = CryptionUtil.encryption(this.userPwd, secretKey);
        dbConfig.addElement("UserPwd").setText(crytPassword);
        
        Element serverConfig = rootElement.addElement("ServerConfig");
        serverConfig.addElement("ConfigAlias").setText(this.getConfigAlias());
        serverConfig.addElement("ProjectAlias").setText(this.getProjectAlias());
        serverConfig.addElement("ServerAddress").setText(this.serverAddress);
        serverConfig.addElement("ServerPort").setText(this.serverPort);
        if (StringUtil.isNotNullNotEmpty(this.serverUserId)){
        	serverConfig.addElement("ServerUserId").setText(this.serverUserId);
        }
        if (StringUtil.isNotNullNotEmpty(this.serverUserPwd)){
        	 String crytServerPassword = CryptionUtil.encryption(this.serverUserPwd, secretKey);
        	 serverConfig.addElement("ServerUserPwd").setText(crytServerPassword);
        }       
        Element serverList = rootElement.addElement("ServerList");
        for (int i=0;i < this.serverConfigs.size();i++){
        	ServerConfig tempServerConfig  = this.serverConfigs.get(i);
        	Element tempServerElement = serverList.addElement("ServerConfig");
        	
        	tempServerElement.addElement("ConfigAlias").setText(tempServerConfig.getConfigAlias());
        	tempServerElement.addElement("ProjectAlias").setText(tempServerConfig.getProjectAlias());
        	tempServerElement.addElement("ServerAddress").setText(tempServerConfig.getServerAddress());
        	tempServerElement.addElement("ServerPort").setText(tempServerConfig.getServerPort());
        	
        	if (StringUtil.isNotNullNotEmpty(tempServerConfig.getServerUserId())){
        		tempServerElement.addElement("ServerUserId").setText(tempServerConfig.getServerUserId());
        	}
        	if (StringUtil.isNotNullNotEmpty(tempServerConfig.getServerUserPwd())){
        		String crytServerPassword = CryptionUtil.encryption(tempServerConfig.getServerUserPwd(), secretKey);
        		tempServerElement.addElement("ServerUserPwd").setText(crytServerPassword);
        	}
        }
        
        Element funcTreeElement = rootElement.addElement("FuncTree");
        for (int i=0;i < this.funcLists.size();i++){
        	Element funcListElement = funcTreeElement.addElement("FuncList");
        	FuncList funcList = this.funcLists.get(i);
        	funcListElement.addAttribute("id", funcList.getId());
        	funcListElement.addAttribute("name", funcList.getName());
        	funcListElement.addAttribute("code", funcList.getCode());
        	
        	if (!funcList.getFuncNodes().isEmpty()){
        		for (int j=0; j < funcList.getFuncNodes().size();j++){
        			FuncNode funcNode = funcList.getFuncNodes().get(j);
        			Element funcNodeElement = funcListElement.addElement("FuncNode");

        			funcNodeElement.addAttribute("id", funcNode.getId());
        			funcNodeElement.addAttribute("name", funcNode.getName());
        			funcNodeElement.addAttribute("subPkg", funcNode.getSubPkg());
        			funcNodeElement.addAttribute("type", funcNode.getType());
        			funcNodeElement.addAttribute("typeName", funcNode.getTypeName());
        		}
        	}
        }
        
        XmlUtil.writeDocument(document, encoding, configFile);
        this.document = document;
    }
    
    public void addInitFuncTree() throws IOException{
    	Element element = this.document.getRootElement().addElement("FuncTree");
    	String nextId = String.valueOf(System.currentTimeMillis());
    	element.addElement("FuncList").addAttribute("id",nextId).addAttribute("name","样例模块").addAttribute("code", "sample");
    	XmlUtil.writeDocument(document, encoding, configFile);
    }
	public List<ServerConfig> getServerConfigs() {
		return serverConfigs;
	}
	public void setServerConfigs(List<ServerConfig> serverConfigs) {
		this.serverConfigs = serverConfigs;
	}
	public List<FuncList> getFuncLists() {
		return funcLists;
	}
	public void setFuncLists(List<FuncList> funcLists) {
		this.funcLists = funcLists;
	}
}