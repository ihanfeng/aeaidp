package com.agileai.miscdp.hotweb.domain.treemanage;

import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import com.agileai.miscdp.DeveloperConst;
import com.agileai.miscdp.hotweb.domain.BaseFuncModel;
import com.agileai.miscdp.hotweb.domain.PageFormField;
import com.agileai.hotweb.model.treemanage.TreeManageFuncModelDocument;
/**
 * 树形管理功能模型
 */
public class TreeManageFuncModel extends BaseFuncModel{
	protected String tableName = "";
	protected String pkGenPolicy = "";
	protected String jspName = "";
	protected String pageTitle = "";
	protected String template = "Default";

	protected String idField = "";
	protected String nameField = "";
	protected String parentIdField = "";
	protected String sortField = "";
	
	protected String handlerId="";
	protected String handlerClass="";
	
	protected List<PageFormField> pageFormFields = new ArrayList<PageFormField>();
	
	public TreeManageFuncModel(){
		this.editorId = DeveloperConst.TREEMANAGE_EDITOR_ID;
	}
	public TreeManageFuncModel(String funcModelDefin){
		
	}
//	public List<PageFormField> addPageFormField(PageFormField pagFormField){
//		this.pageFormFields.add(pagFormField);
//		return this.pageFormFields;
//	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String table) {
		this.tableName = table;
	}
	public String getJspName() {
		return jspName;
	}
	public void setJspName(String detailJspName) {
		if (detailJspName != null){
			this.jspName = detailJspName;			
		}
	}
	
	public String getSqlNamespace() {
		String sqlNameSpaceName = this.interfaceName.substring(this.interfaceName.lastIndexOf(".")+1,this.interfaceName.length());
		return sqlNameSpaceName;
	}
	
	public String getPageTitle() {
		return pageTitle;
	}
	public void setPageTitle(String listTitle) {
		this.pageTitle = listTitle;
	}
	public String getTemplate() {
		return template;
	}
	public void setTemplate(String template) {
		this.template = template;
	}

	public List<PageFormField> getPageFormFields() {
		return pageFormFields;
	}

	public void buildFuncModel(String funcDefine){
		TreeManageFuncModel funcModel = this;
		try {
			Reader reader = new StringReader(funcDefine);
			TreeManageFuncModelDocument funcModelDocument = TreeManageFuncModelDocument.Factory.parse(reader);
			
			TreeManageFuncModelDocument.TreeManageFuncModel.BaseInfo baseInfo = funcModelDocument.getTreeManageFuncModel().getBaseInfo();
			funcModel.setTableName(baseInfo.getTableName());
			funcModel.setPkGenPolicy(baseInfo.getPkGenPolicy());
			funcModel.setHandlerId(baseInfo.getHandler().getHandlerId());
			funcModel.setHandlerClass(baseInfo.getHandler().getHandlerClass());
			
			funcModel.setServiceId(baseInfo.getService().getServiceId());
			funcModel.setImplClassName(baseInfo.getService().getImplClassName());
			funcModel.setInterfaceName(baseInfo.getService().getInterfaceName());
			funcModel.setJspName(baseInfo.getJspName());
			funcModel.setPageTitle(baseInfo.getPageTitle());
			funcModel.setTemplate(baseInfo.getTemplate());

			funcModel.setIdField(baseInfo.getIdField());
			funcModel.setNameField(baseInfo.getNameField());
			funcModel.setParentIdField(baseInfo.getParentIdField());
			funcModel.setSortField(baseInfo.getSortField());
			
            TreeManageFuncModelDocument.TreeManageFuncModel.PageView pageView = funcModelDocument.getTreeManageFuncModel().getPageView();
			com.agileai.hotweb.model.FormObjectDocument.FormObject[] editFormObjects =pageView.getCurrentEditArea().getFormObjectArray();
			processPageFormFields(editFormObjects, pageFormFields);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public String getHandlerId() {
		return handlerId;
	}
	public void setHandlerId(String listHandlerId) {
		this.handlerId = listHandlerId;
	}

	public String getHandlerClass() {
		return handlerClass;
	}
	public void setHandlerClass(String listHandlerClass) {
		this.handlerClass = listHandlerClass;
	}
	public String getPkGenPolicy() {
		return pkGenPolicy;
	}
	public void setPkGenPolicy(String pkGenPolicy) {
		this.pkGenPolicy = pkGenPolicy;
	}
	public String getIdField() {
		return idField;
	}
	public void setIdField(String idField) {
		this.idField = idField;
	}
	public String getNameField() {
		return nameField;
	}
	public void setNameField(String nameField) {
		this.nameField = nameField;
	}
	public String getParentIdField() {
		return parentIdField;
	}
	public void setParentIdField(String parentIdField) {
		this.parentIdField = parentIdField;
	}
	public String getSortField() {
		return sortField;
	}
	public void setSortField(String sortField) {
		this.sortField = sortField;
	}
}
