﻿package com.agileai.miscdp.hotweb.domain.standard;
/***
 * 单表操作数据表模型
 */
public class StandardTableModel {
	private String namespace = null;
	private String tableName = null;
	private String schema = null;
	private String findRecordsSql = null;
	
	private String getRecordSql = null;
	private String insertRecordSql = null;
	private String updateRecordSql = null;
	private String deleteRecordSql = null;
	
	public void setGetRecordSql(String getRecordSql) {
		this.getRecordSql = getRecordSql;
	}

	public void setInsertRecordSql(String insertRecordSql) {
		this.insertRecordSql = insertRecordSql;
	}

	public void setUpdateRecordSql(String updateRecordSql) {
		this.updateRecordSql = updateRecordSql;
	}

	public void setDeleteRecordSql(String deleteRecordSql) {
		this.deleteRecordSql = deleteRecordSql;
	}

	public String getNamespace() {
		return namespace;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getSchema() {
		return schema;
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}

	public String getFindRecordsSql() {
		if (findRecordsSql == null){
			findRecordsSql = "select * from "+this.tableName;
		}
		return findRecordsSql;
	}

	public void setFindRecordsSql(String findRecordsSql) {
		if (findRecordsSql != null && !"".equals(findRecordsSql.trim())){
			this.findRecordsSql = findRecordsSql;			
		}
	}

	public String getGetRecordSql() {
		return getRecordSql;
	}

	public String getInsertRecordSql() {
		return insertRecordSql;
	}

	public String getUpdateRecordSql() {
		return updateRecordSql;
	}

	public String getDeleteRecordSql() {
		return deleteRecordSql;
	}
	
}
