package com.agileai.miscdp.hotweb;

import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleFactory;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.MessageConsole;

public class ConsoleFactory implements IConsoleFactory{
    static MessageConsole console = new MessageConsole("Invoking HotServer Infomation:",  null);  
    static boolean isShow = false;
    
    public void openConsole() {  
        showConsole();  
    }  
    public static void showConsole() {  
        if (console != null && !isShow) {     
               IConsoleManager manager = ConsolePlugin.getDefault().getConsoleManager();     
               IConsole[] existing = manager.getConsoles();     
               boolean exists = false;     
               //add the new MessageConsole instance into the control manager and show     
               for (int i = 0; i < existing.length; i++) {     
                   if (console == existing[i])     
                       exists = true;     
               }         
               if(!exists){      
                   manager.addConsoles(new IConsole[] { console });     
               }     
               manager.showConsoleView(console);    
               isShow = true;
           }    
    }  
    public static void closeConsole(){  
        IConsoleManager manager = ConsolePlugin.getDefault().getConsoleManager();  
        if (console != null){  
            manager.removeConsoles(new IConsole[]{ console });  
        }  
    }  
    public static MessageConsole getConsole(){
    	showConsole();
        return console;  
    } 
}
