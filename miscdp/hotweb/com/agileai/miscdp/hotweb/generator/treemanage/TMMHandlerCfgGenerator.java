﻿package com.agileai.miscdp.hotweb.generator.treemanage;

import java.io.File;

import org.dom4j.Document;
import org.dom4j.io.SAXReader;

import com.agileai.miscdp.NoOpEntityResolver;
import com.agileai.miscdp.hotweb.domain.treemanage.TreeManageFuncModel;
import com.agileai.miscdp.hotweb.generator.Generator;
import com.agileai.miscdp.util.MiscdpUtil;
import com.agileai.util.XmlUtil;
/**
 * Handler配置代码生成器
 */
public class TMMHandlerCfgGenerator implements Generator{
	private TreeManageFuncModel funcModel = null;
	private String configFile = null;
	private String encoding = "UTF-8";
	
	public void generate() {
		try {
	        SAXReader saxReader = new SAXReader();
	        saxReader.setEntityResolver(new NoOpEntityResolver());
	        saxReader.setIncludeExternalDTDDeclarations(false);
	        saxReader.setValidation(false);
	        Document document = saxReader.read(new File(configFile));
	        
	        String listHandlerId = funcModel.getHandlerId();
	        String listHandlerClass = funcModel.getHandlerClass();
	        String listJspPage = funcModel.getJspName();
	        MiscdpUtil.newHandlerConfigElement(document, listHandlerId, listHandlerClass, listJspPage);

	        String parentJspName = listJspPage.substring(0,listJspPage.length()-14)+"ParentSelect"+".jsp";
	        String parentSelectHandlerId = listHandlerId.substring(0,listHandlerId.length()-10)+"ParentSelect";
	        String parentSelectHandlerClass = listHandlerClass.substring(0,listHandlerClass.lastIndexOf("."))
	        		+ "." + parentSelectHandlerId + "Handler";
	        MiscdpUtil.newHandlerConfigElement(document, parentSelectHandlerId ,parentSelectHandlerClass, parentJspName);

	        XmlUtil.writeDocument(document, encoding, configFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setConfigFile(String configFile) {
		this.configFile = configFile;
	}

	public void setFuncModel(TreeManageFuncModel suFuncModel) {
		this.funcModel = suFuncModel;
	}
}
