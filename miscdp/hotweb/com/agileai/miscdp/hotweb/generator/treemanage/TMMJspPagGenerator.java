﻿package com.agileai.miscdp.hotweb.generator.treemanage;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Platform;

import com.agileai.miscdp.MiscdpPlugin;
import com.agileai.miscdp.hotweb.domain.treemanage.TreeManageFuncModel;
import com.agileai.miscdp.hotweb.generator.Generator;
import com.agileai.util.StringUtil;

import freemarker.ext.dom.NodeModel;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
/**
 * JSP代码生成器
 */
public class TMMJspPagGenerator implements Generator{
	private String jspFile = null;
	private String parentSelectJspFile = null;
	
	public void setParentSelectJspFile(String parentSelectJspFile) {
		this.parentSelectJspFile = parentSelectJspFile;
	}
	
	private TreeManageFuncModel funcModel = null;
	private File xmlFile = null;
	private String charencoding = "UTF-8";
	private String templateDir = null;
	public void generate() {
		String fileName = "template";
		try {
			templateDir = FileLocator.toFileURL(Platform.getBundle(MiscdpPlugin.getPluginId()).getResource(fileName)).getFile().toString();
			String uniqueFieldCode = funcModel.retrieveUniqueField(funcModel.getPageFormFields());
			boolean checkUnique = !StringUtil.isNullOrEmpty(uniqueFieldCode);
			String templateFile = "/treemanage/Page.jsp.ftl";
			generatePage(jspFile,templateFile,checkUnique);
			
			templateFile = "/treemanage/parentSelectPage.jsp.ftl";
			generatePage(parentSelectJspFile,templateFile,false);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void generatePage(String jspFile, String templateFile,boolean checkUnique) {
        try {
        	Configuration cfg = new Configuration();
        	cfg.setDirectoryForTemplateLoading(new File(templateDir));
            cfg.setObjectWrapper(new DefaultObjectWrapper());
            cfg.setEncoding(Locale.getDefault(), charencoding);
        	Template temp = cfg.getTemplate(templateFile);
        	temp.setEncoding(charencoding);
            Map root = new HashMap();
            NodeModel nodeModel = freemarker.ext.dom.NodeModel.parse(xmlFile);
            root.put("doc",nodeModel);
            root.put("checkUnique", checkUnique);
            File file = new File(jspFile);
            File parentFile = file.getParentFile();
            if (!parentFile.exists()){
            	parentFile.mkdirs();
            }
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file),charencoding));
//            Writer out = new OutputStreamWriter(System.out);
            temp.process(root, out);
            out.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }		
	}
	public void setJspFile(String jspFile) {
		this.jspFile = jspFile;
	}
	public void setFuncModel(TreeManageFuncModel funcModel) {
		this.funcModel = funcModel;
	}
	public File getXmlFile() {
		return xmlFile;
	}
	public void setXmlFile(File xmlFile) {
		this.xmlFile = xmlFile;
	}
		
}
