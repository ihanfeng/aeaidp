﻿package com.agileai.miscdp.hotweb.generator.treefill;

import java.io.File;

import org.dom4j.Document;
import org.dom4j.io.SAXReader;

import com.agileai.miscdp.NoOpEntityResolver;
import com.agileai.miscdp.hotweb.domain.treefill.TreeFillFuncModel;
import com.agileai.miscdp.hotweb.generator.Generator;
import com.agileai.miscdp.util.MiscdpUtil;
import com.agileai.util.XmlUtil;
/**
 * Handler配置代码生成器
 */
public class TFMHandlerCfgGenerator implements Generator{
	private TreeFillFuncModel funcModel = null;
	private String configFile = null;
	private String encoding = "UTF-8";
	public void generate() {
		try {
	        SAXReader saxReader = new SAXReader();
	        saxReader.setEntityResolver(new NoOpEntityResolver());
	        saxReader.setIncludeExternalDTDDeclarations(false);
	        saxReader.setValidation(false);
	        Document document = saxReader.read(new File(configFile));
	        
	        String listHandlerId = funcModel.getSelectTreeHandlerId();
	        String listHandlerClass = funcModel.getSelectTreeHandlerClass();
	        String listJspName = funcModel.getListJspName();
	        MiscdpUtil.newHandlerConfigElement(document,listHandlerId,listHandlerClass,listJspName);
	        
	        XmlUtil.writeDocument(document, encoding, configFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setConfigFile(String configFile) {
		this.configFile = configFile;
	}

	public void setFuncModel(TreeFillFuncModel suFuncModel) {
		this.funcModel = suFuncModel;
	}
}
