﻿package com.agileai.miscdp.hotweb.generator.pickfill;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Platform;

import com.agileai.miscdp.MiscdpPlugin;
import com.agileai.miscdp.hotweb.generator.Generator;

import freemarker.ext.dom.NodeModel;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
/**
 * JSP代码生成器
 */
public class PFMJspPagGenerator implements Generator{
	private String listJspFile = null;
	private File xmlFile = null;
	private String charencoding = "UTF-8";
	private String templateDir = null;
	public void generate() {
		String fileName = "template";
		try {
			templateDir = FileLocator.toFileURL(Platform.getBundle(MiscdpPlugin.getPluginId()).getResource(fileName)).getFile().toString();
		} catch (IOException e) {
			e.printStackTrace();
		}
		generateList();
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void generateList() {
        try {
        	Configuration cfg = new Configuration();
        	cfg.setDirectoryForTemplateLoading(new File(templateDir));
            cfg.setObjectWrapper(new DefaultObjectWrapper());
            cfg.setEncoding(Locale.getDefault(), charencoding);
            String templateFile = "/pickfill/ListPage.jsp.ftl";
        	Template temp = cfg.getTemplate(templateFile);
        	temp.setEncoding(charencoding);
            Map root = new HashMap();
            NodeModel nodeModel = freemarker.ext.dom.NodeModel.parse(xmlFile);
            root.put("doc",nodeModel);
            File file = new File(listJspFile);
            File parentFile = file.getParentFile();
            if (!parentFile.exists()){
            	parentFile.mkdirs();
            }
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file),charencoding));
//            Writer out = new OutputStreamWriter(System.out);
            temp.process(root, out);
            out.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }		
	}

	public String getListJspFile() {
		return listJspFile;
	}

	public void setListJspFile(String xmlFile) {
		this.listJspFile = xmlFile;
	}
	public File getXmlFile() {
		return xmlFile;
	}
	public void setXmlFile(File xmlFile) {
		this.xmlFile = xmlFile;
	}
}
