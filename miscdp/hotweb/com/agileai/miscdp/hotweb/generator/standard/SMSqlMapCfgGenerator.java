﻿package com.agileai.miscdp.hotweb.generator.standard;

import java.io.File;

import org.dom4j.Document;
import org.dom4j.io.SAXReader;

import com.agileai.miscdp.MiscdpPlugin;
import com.agileai.miscdp.NoOpEntityResolver;
import com.agileai.miscdp.hotweb.domain.ResFileValueProvider;
import com.agileai.miscdp.hotweb.domain.standard.StandardFuncModel;
import com.agileai.miscdp.hotweb.generator.Generator;
import com.agileai.miscdp.util.MiscdpUtil;
import com.agileai.util.XmlUtil;
/**
 * SQLMap配置代码生成器
 */
public class SMSqlMapCfgGenerator implements Generator{
	private StandardFuncModel funcModel = null;
	private String configFile = null;
	private String encoding = "UTF-8";
	public void generate() {
		try {
	        SAXReader saxReader = new SAXReader();
	        saxReader.setEntityResolver(new NoOpEntityResolver());
	        saxReader.setIncludeExternalDTDDeclarations(false);
	        saxReader.setValidation(false);
	        Document document = saxReader.read(new File(configFile));
	        String sqlMapFileName = this.funcModel.getSqlNamespace();
	        MiscdpUtil.newSqlMapConfigElement(document, sqlMapFileName);
        	
	        ResFileValueProvider resFileValueProvider = funcModel.getResFileValueProvider();
	        if (resFileValueProvider != null){
	        	String resFileTableName = resFileValueProvider.getTableName();
	        	String resFileSqlFileName = MiscdpUtil.getValidName(resFileTableName);
	        	MiscdpUtil.newSqlMapConfigElement(document, resFileSqlFileName);
	        }
        	XmlUtil.writeDocument(document, encoding, configFile);
		} catch (Exception e) {
			MiscdpPlugin.getDefault().logError(e.getLocalizedMessage(), e);
		}
	}
	
	public StandardFuncModel getFuncModel() {
		return funcModel;
	}

	public void setFuncModel(StandardFuncModel funcModel) {
		this.funcModel = funcModel;
	}

	public void setConfigFile(String configFile) {
		this.configFile = configFile;
	}
}
