package com.agileai.miscdp.hotweb.ui.celleditors;

import org.eclipse.jface.viewers.DialogCellEditor;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;

import com.agileai.miscdp.hotweb.domain.treecontent.ContentTableInfo;
import com.agileai.miscdp.hotweb.ui.celleditors.dialog.ContentTableInfoProviderDialog;
import com.agileai.miscdp.hotweb.ui.editors.BaseModelEditor;
/**
 * 组合的DialCellEditor
 */
public class ContentTableInfoProviderEditor extends DialogCellEditor{
	Table table = null;
	BaseModelEditor baseModelEditor;
	TableViewer tableViewer;
    public ContentTableInfoProviderEditor(BaseModelEditor baseModelEditor,TableViewer tableViewer,Composite parent) {
        this(parent, SWT.NONE);
        this.table = (Table)parent;
        this.baseModelEditor = baseModelEditor;
        this.tableViewer = tableViewer;
    }
    public ContentTableInfoProviderEditor(Composite parent, int style) {
        super(parent, style);
    }
	protected Object openDialogBox(Control cellEditorWindow) {
		String projectName = baseModelEditor.getFuncModel().getProjectName();
		ContentTableInfoProviderDialog dialog = new ContentTableInfoProviderDialog(this.getControl().getShell(),projectName);
		TableItem[] tableItems = table.getSelection();
		TableItem item = (TableItem)tableItems[0];
		ContentTableInfo contentTableInfo = (ContentTableInfo) item.getData(); 
		dialog.setContentTableInfo(contentTableInfo);
		dialog.open();
		if (!dialog.isCancelEdit()){
			baseModelEditor.setModified(true);
			baseModelEditor.fireDirty();
			tableViewer.refresh();
		}
		return dialog.getCurrentTableName();
	}
}
