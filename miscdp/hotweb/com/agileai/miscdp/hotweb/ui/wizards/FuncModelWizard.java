﻿package com.agileai.miscdp.hotweb.ui.wizards;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;

import com.agileai.miscdp.hotweb.domain.ProjectConfig;
import com.agileai.miscdp.util.MiscdpUtil;
import com.agileai.domain.KeyNamePair;
/**
 * 功能模型向导
 */
public class FuncModelWizard extends Wizard implements INewWizard {
	private String projectName = null;
	private FuncModelWizardPage page;
	private ISelection selection;
	private String newFuncId;
	private String funcName;
	private String funcType;
	private String funcTypeName;
	
	public FuncModelWizard(String projectName) {
		super();
		this.projectName = projectName;
		setWindowTitle(Messages.getString("FuncWizardTitle"));
		setNeedsProgressMonitor(true);
		this.page = new FuncModelWizardPage(this,selection);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public java.util.List getFuncModleList(){
		java.util.List result =  new ArrayList();
		result.addAll(MiscdpUtil.getIniReader().getList("FunctionModels"));
		ProjectConfig projectConfig = MiscdpUtil.getProjectConfig(projectName);
		if (!projectConfig.isIntegrateWebProject()){
			for (int i=0;i < result.size();i++){
				KeyNamePair keyNamePair = (KeyNamePair)result.get(i);
				String key = keyNamePair.getKey();
				if (key.equals("portlet")){
					result.remove(keyNamePair);
					break;
				}
			}			
		}
		return result;
	}
	
	public void addPages() {
		addPage(page);
	}
	public boolean performFinish() {
		funcName = page.getFuncNameText().getText();
		IStructuredSelection selection = (IStructuredSelection)page.getFuncTypeListViewer().getSelection();
		if (selection.isEmpty()){
			page.dialogChanged();
			return false;
		}
		KeyNamePair keyNamePair = (KeyNamePair)selection.getFirstElement(); 
		funcType = keyNamePair.getKey();
		funcTypeName = keyNamePair.getValue();
		IRunnableWithProgress op = new IRunnableWithProgress() {
			public void run(IProgressMonitor monitor) throws InvocationTargetException {
				try {
					doFinish(funcName,funcType,monitor);
				} catch (CoreException e) {
					throw new InvocationTargetException(e);
				} finally {
					monitor.done();
				}
			}
		};
		try {
			getContainer().run(true, false, op);
		} catch (InterruptedException e) {
			return false;
		} catch (InvocationTargetException e) {
			Throwable realException = e.getTargetException();
			MessageDialog.openError(getShell(), "Error", realException.getMessage());
			return false;
		}
		return true;
	}
	private void doFinish(String funcName,String funcType,IProgressMonitor monitor) throws CoreException {
		monitor.beginTask("Creating BaseFuncModel " + funcName, 1000);
		monitor.worked(1);
		monitor.setTaskName("create basic funcModel define...");
		insertFuncModel(funcName,funcType);
		monitor.worked(1);
	}
	private void insertFuncModel(String funcName,String funcType){
		String nextId = String.valueOf(System.currentTimeMillis());
		newFuncId = nextId;
	}
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		this.selection = selection;
	}
	public String getNewFuncId() {
		return newFuncId;
	}
	public String getFuncName() {
		return funcName;
	}
	public String getFuncType() {
		return funcType;
	}
	public String getFuncTypeName() {
		return funcTypeName;
	}
	public String getProjectName() {
		return projectName;
	}
}
