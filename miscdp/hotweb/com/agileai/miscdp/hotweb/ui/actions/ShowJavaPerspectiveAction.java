package com.agileai.miscdp.hotweb.ui.actions;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.WorkbenchException;

public class ShowJavaPerspectiveAction implements IWorkbenchWindowActionDelegate {
	private IWorkbenchWindow window;
	public ShowJavaPerspectiveAction() {
	}
	public void run(IAction action) {
		try {
			PlatformUI.getWorkbench().showPerspective("org.eclipse.jdt.ui.JavaPerspective", window);
		} catch (WorkbenchException e) {
			e.printStackTrace();
		}
	}
	public void selectionChanged(IAction action, ISelection selection) {
	}

	public void dispose() {
	}

	public void init(IWorkbenchWindow window) {
		this.window = window;
	}
}
