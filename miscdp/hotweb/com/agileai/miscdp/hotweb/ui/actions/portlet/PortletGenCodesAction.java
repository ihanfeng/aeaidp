﻿package com.agileai.miscdp.hotweb.ui.actions.portlet;

import java.io.File;
import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.progress.IProgressService;

import com.agileai.miscdp.hotweb.domain.ProjectConfig;
import com.agileai.miscdp.hotweb.domain.portlet.PortletFuncModel;
import com.agileai.miscdp.hotweb.generator.portlet.PortletCfgGenerator;
import com.agileai.miscdp.hotweb.generator.portlet.PortletJavaGenerator;
import com.agileai.miscdp.hotweb.generator.portlet.PortletJspPagGenerator;
import com.agileai.miscdp.hotweb.ui.actions.BaseGenCodesAction;
import com.agileai.miscdp.hotweb.ui.editors.portlet.PortletModelEditor;
import com.agileai.miscdp.hotweb.ui.editors.portlet.PortletModelEditorContributor;
import com.agileai.miscdp.hotweb.ui.editors.portlet.PortletModelPanel;
import com.agileai.miscdp.util.DialogUtil;
import com.agileai.miscdp.util.MiscdpUtil;
import com.agileai.util.FileUtil;
import com.agileai.util.StringUtil;
/**
 * Portlet功能模型生成代码动作 
 */
public class PortletGenCodesAction extends BaseGenCodesAction{
	public PortletGenCodesAction() {
		
	}
	
	public void setContributor(PortletModelEditorContributor contributor) {
		this.contributor = contributor;
	}
	
	public void run() {
		PortletModelEditor modelEditor = (PortletModelEditor)this.getModelEditor();
		Shell shell = modelEditor.getSite().getShell();
		
		PortletModelPanel portletModelPanel = modelEditor.getPortletModelPanel();
		String funcSubPkg = portletModelPanel.getFuncSubPkgText().getText();
		
		if (StringUtil.isNullOrEmpty(funcSubPkg)){
			DialogUtil.showMessage(shell,"消息提示","目录编码不能为空!",DialogUtil.MESSAGE_TYPE.WARN);
			return;
		}
		
    	if (modelEditor.isDirty()){
    		String confirmTitle = MiscdpUtil.getIniReader().getValue("DialogInfomation","ConfirmTitle");
    		String confirmMsg = MiscdpUtil.getIniReader().getValue("DialogInfomation","IsSaveFirst");
    		boolean saveFirst = MessageDialog.openConfirm(shell, confirmTitle, confirmMsg);
    		if (saveFirst){
    			modelEditor.doSave(new NullProgressMonitor());
    		}else{
    			return;
    		}
    	}
		funcModel = modelEditor.buildFuncModel();
		
		IProgressService progressService = PlatformUI.getWorkbench().getProgressService();
		try {
	    	String projectName = funcModel.getProjectName();
	    	IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject(projectName);
			progressService.runInUI(progressService,this,project);
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void run(IProgressMonitor monitor) throws InvocationTargetException,
			InterruptedException {
		monitor.beginTask("init model values:", 500);
		monitor.subTask("set model values....");
		monitor.worked(50);
		
		monitor.subTask("generating Jsp files....");
		monitor.worked(200);
		PortletFuncModel portletFuncModel = (PortletFuncModel)funcModel;
		
		PortletJspPagGenerator sMJspPagGenerator = new PortletJspPagGenerator();
		File xmlFile = getXmlFile();
		sMJspPagGenerator.setXmlFile(xmlFile);
		sMJspPagGenerator.setPortletFuncModel(portletFuncModel);
    	String projectName = funcModel.getProjectName();
    	IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject(projectName);
    	
		ProjectConfig projectConfig = MiscdpUtil.getProjectConfig(projectName);
		String subDir = projectConfig.getMainPkg().replaceAll("\\.", "/");
		String subPkg = funcModel.getFuncSubPkg();
		String modulePath = project.getLocation().toString()+"/src/"+subDir+"/module/"+subPkg;
		File moduleFile = new File(modulePath);
		if (!moduleFile.exists()){
			moduleFile.mkdirs();
		}
    	
    	String defaultJspName = project.getLocation().toString()+"/WebRoot/pages/";
		sMJspPagGenerator.setJspPath(defaultJspName);
		sMJspPagGenerator.generate();
		
		monitor.subTask("generating Portlet config files....");
		monitor.worked(300);
		PortletCfgGenerator sMHandlerCfgGenerator = new PortletCfgGenerator();
		String abstHandlerCfgPath = modulePath+"/PortletModule.xml";
		sMHandlerCfgGenerator.setPortltConfigFile(abstHandlerCfgPath);
		sMHandlerCfgGenerator.setFuncModel((PortletFuncModel)funcModel);
		MiscdpUtil.initDefaultXML(modulePath,"PortletModule.xml");
		sMHandlerCfgGenerator.generate();
		
		monitor.subTask("generating Portlet java files....");
		monitor.worked(400);
		
		String srcDir = project.getLocation().toString()+"/src";
		PortletJavaGenerator sMHandlerGenerator = new PortletJavaGenerator();
		String portletClassName = portletFuncModel.getPortletClass();
		File javaFile = FileUtil.createJavaFile(srcDir, portletClassName);
		sMHandlerGenerator.setJavaFile(javaFile);
		sMHandlerGenerator.setXmlFile(xmlFile);
		sMHandlerGenerator.generate();
		
		try {
			project.refreshLocal(IResource.DEPTH_INFINITE, null);
		} catch (CoreException e) {
			e.printStackTrace();
		}
	}
	
}