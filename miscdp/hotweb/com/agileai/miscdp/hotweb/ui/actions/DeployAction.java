package com.agileai.miscdp.hotweb.ui.actions;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IViewActionDelegate;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.progress.IWorkbenchSiteProgressService;

import com.agileai.miscdp.DeployManager;
import com.agileai.miscdp.hotweb.ConsoleHandler;
import com.agileai.miscdp.hotweb.domain.DeployResource;
import com.agileai.miscdp.hotweb.domain.DeployResource.DeployTypes;
import com.agileai.miscdp.hotweb.domain.DeployResource.FileTypes;
import com.agileai.miscdp.hotweb.domain.ProjectConfig;
import com.agileai.miscdp.hotweb.ui.dialogs.DeployDialog;
import com.agileai.miscdp.server.DeployableZip;
import com.agileai.miscdp.server.HotServerManage;
import com.agileai.miscdp.util.DialogUtil;
import com.agileai.miscdp.util.MiscdpUtil;

public class DeployAction implements IViewActionDelegate{
    private IWorkbenchPart targetPart;
    private DeployManager deployManager = null; 
    private IJavaProject javaProject = null;
    
	public void run(IAction action) {
		ISelection selection = targetPart.getSite().getSelectionProvider().getSelection();
		IStructuredSelection curSelection = (IStructuredSelection)selection;
		List<?> resources = curSelection.toList();
		
		if (resources != null && resources.size() > 0){
			if (resources.size() == 1 && resources.get(0) instanceof IJavaProject){
				javaProject = DeployManager.retriveJavaProject(resources.get(0));
				String appName = javaProject.getProject().getName();
				deployManager = new DeployManager(appName);
				deployManager.parseModuleNames(javaProject);
				deployApplication(javaProject);
			}
			else{
				javaProject = DeployManager.retriveJavaProject(resources.get(0));
				if (javaProject == null){
					DialogUtil.showInfoMessage("select resource is not valid !");
					return;
				}
				String appName = javaProject.getProject().getName();
				deployManager = new DeployManager(appName);
				deployManager.parseModuleNames(javaProject);
				if (deployManager.isAllModuleResources(resources)){
					deployModules(javaProject);
				}else{
					if (deployManager.isOnlyServiceResources(resources)){
						deployServices(javaProject);
					}else{
						deployAppFiles(resources, javaProject);
					}
				}
			}
		}
	}
	
	private void deployApplication(final IJavaProject javaProject){
		Shell shell = targetPart.getSite().getShell();
		final String appName = javaProject.getProject().getName();
		DeployDialog deployDialog = new DeployDialog(shell,appName);
		
		List<DeployResource> inputList = deployManager.buildApplicaitonInputList(javaProject);
		deployDialog.setInputDeployFilesList(inputList);
		deployDialog.setDeployType(DeployTypes.Application);
		deployDialog.setSelectedDeployFilesList(deployManager.getSelectedResourceList());
		deployDialog.open();
		final boolean needReload = deployDialog.isNeedReload();
		
		if (deployDialog.getReturnCode() == Dialog.OK){
			IWorkbenchSiteProgressService siteService = (IWorkbenchSiteProgressService) targetPart.getSite().getAdapter(IWorkbenchSiteProgressService.class);     
    		Job deployJob = new Job("deployApp") {
				@Override
				protected IStatus run(IProgressMonitor arg0) {
					try {
						ProjectConfig projectConfig = MiscdpUtil.getProjectConfig(appName);
						ConsoleHandler.info("prepare deploy " + appName + " to server " + projectConfig.getConfigAlias() +" ...");
						
						DeployableZip appZip = deployManager.buildDeployZip4App(javaProject,needReload);
						ConsoleHandler.info("start deploy " + appName + " ...");
						
						HotServerManage hotServerManage = MiscdpUtil.getHotServerManage(projectConfig);
						hotServerManage.deployApplication(projectConfig.getProjectAlias(), appZip);
						
						ConsoleHandler.info("deploy " + appName + " successfully !");		
					} catch (Exception e) {
						ConsoleHandler.error(e.getLocalizedMessage());
					}
					return Status.OK_STATUS;
				}
			};
    		siteService.schedule(deployJob,0, true);
		}
	}
	
	private void deployServices(final IJavaProject javaProject){
		Shell shell = targetPart.getSite().getShell();
		final String appName = javaProject.getProject().getName();
		DeployDialog deployDialog = new DeployDialog(shell,appName);
		
		List<DeployResource> inputList = deployManager.buildServicesInputList(javaProject);
		deployDialog.setInputDeployFilesList(inputList);
		deployDialog.setDeployType(DeployTypes.Services);
		deployDialog.setSelectedDeployFilesList(deployManager.getSelectedResourceList());
		deployDialog.open();
		final boolean needReload = deployDialog.isNeedReload();
		
		if (deployDialog.getReturnCode() == Dialog.OK){
			IWorkbenchSiteProgressService siteService = (IWorkbenchSiteProgressService) targetPart.getSite().getAdapter(IWorkbenchSiteProgressService.class);     
    		Job deployJob = new Job("deployService") {
				@Override
				protected IStatus run(IProgressMonitor arg0) {
					try {
						String appName = javaProject.getProject().getName();
						ProjectConfig projectConfig = MiscdpUtil.getProjectConfig(appName);
						ConsoleHandler.info("prepare deploy services to server " + projectConfig.getConfigAlias() +" ...");
						
						DeployableZip serviceZip = deployManager.buildDeployZip4Services(javaProject,needReload);
						
						ConsoleHandler.info("start deploy services ...");
						HotServerManage hotServerManage = MiscdpUtil.getHotServerManage(projectConfig);
						hotServerManage.deployServices(projectConfig.getProjectAlias(), serviceZip);	
						
						ConsoleHandler.info("deploy services successfully !");	
					} catch (Exception e) {
						ConsoleHandler.error(e.getLocalizedMessage());
					}
					return Status.OK_STATUS;
				}
    		};
    		siteService.schedule(deployJob,0, true);
		}
	}	
	
	private void deployModules(final IJavaProject javaProject){
		Shell shell = targetPart.getSite().getShell();
		final String appName = javaProject.getProject().getName();
		DeployDialog deployDialog = new DeployDialog(shell,appName);
		
		List<DeployResource> inputList = deployManager.buildModulesInputList(javaProject);
		deployDialog.setInputDeployFilesList(inputList);
		deployDialog.setDeployType(DeployTypes.Modules);
		deployDialog.setSelectedDeployFilesList(deployManager.getSelectedResourceList());
		deployDialog.open();
		final boolean needReload = deployDialog.isNeedReload();
		
		if (deployDialog.getReturnCode() == Dialog.OK){
			final List<DeployResource> selectedDeployFilesList = deployDialog.getSelectedDeployFilesList();
			
			IWorkbenchSiteProgressService siteService = (IWorkbenchSiteProgressService) targetPart.getSite().getAdapter(IWorkbenchSiteProgressService.class);     
    		Job deployJob = new Job("deployModule") {
				@Override
				protected IStatus run(IProgressMonitor arg0) {
					try {
						List<String> selectedModuleNames = new ArrayList<String>();
						for (int i=0;i < selectedDeployFilesList.size();i++){
							DeployResource deployResource = selectedDeployFilesList.get(i);
							String moduleName = deployResource.getFileName();
							selectedModuleNames.add(moduleName);
						}
						String appName = javaProject.getProject().getName();
						ProjectConfig projectConfig = MiscdpUtil.getProjectConfig(appName);
						ConsoleHandler.info("prepare deploy modules to server " + projectConfig.getConfigAlias() +" ...");
						
						DeployableZip modulesZip = deployManager.buildDeployZip4Modules(javaProject,selectedModuleNames,needReload);
						
						ConsoleHandler.info("start deploy modules ...");
						HotServerManage hotServerManage = MiscdpUtil.getHotServerManage(projectConfig);
						hotServerManage.deployModules(projectConfig.getProjectAlias(),selectedModuleNames, modulesZip);	
						
						ConsoleHandler.info("deploy modules successfully !");	
					} catch (Exception e) {
						ConsoleHandler.error(e.getLocalizedMessage());
					}
					return Status.OK_STATUS;
				}
    		};
    		siteService.schedule(deployJob,0, true);
		}
	}	
	
	private void deployAppFiles(List<?> resources,final IJavaProject javaProject){
		final String appName = javaProject.getProject().getName();
		DeployDialog deployDialog = new DeployDialog(targetPart.getSite().getShell(),appName);
		
		List<DeployResource> inputList = deployManager.buildAppFilesInputList(resources,javaProject);
		boolean containReloadResouce = false;
		for (int i=0;i < inputList.size();i++){
			DeployResource deployResource = inputList.get(i);
			if (deployResource.isSource()){
				containReloadResouce = true;
				break;
			}
		}
		deployDialog.setContainReloadResouce(containReloadResouce);
		deployDialog.setInputDeployFilesList(inputList);
		deployDialog.setDeployType(DeployTypes.AppFiles);
		deployDialog.setSelectedDeployFilesList(deployManager.getSelectedResourceList());
		deployDialog.open();
		
		final boolean needReload = deployDialog.isNeedReload();
		if (deployDialog.getReturnCode() == Dialog.OK){
			final List<DeployResource> selectedDeployFilesList = deployDialog.getSelectedDeployFilesList();
			for (int i=0;i < selectedDeployFilesList.size();i++){
				DeployResource deployResource = selectedDeployFilesList.get(i);
				if (FileTypes.Package.equals(deployResource.getFileType())){
					deployManager.getPackagePathList().add(deployResource.getCompilePath());
				}
				else if (FileTypes.Folder.equals(deployResource.getFileType())){
					deployManager.getFolderPathList().add(deployResource.getCompilePath());
				}
			}
			
			IWorkbenchSiteProgressService siteService = (IWorkbenchSiteProgressService) targetPart.getSite().getAdapter(IWorkbenchSiteProgressService.class);     
    		Job deployJob = new Job("deployAppFiles") {
				@Override
				protected IStatus run(IProgressMonitor arg0) {
					try {
						ProjectConfig projectConfig = MiscdpUtil.getProjectConfig(appName);
						ConsoleHandler.info("prepare deploy files to server " + projectConfig.getConfigAlias() + " ...");
						DeployableZip appFilesZip = deployManager.buildDeployZip4AppFiles(javaProject,selectedDeployFilesList,needReload);
						
						ConsoleHandler.info("start deploy files ...");
						List<String> folderPathList = deployManager.getFolderPathList();
						List<String> deleteDirs = deployManager.getDeleteDirs();
						List<String> packagePathList = deployManager.getPackagePathList();
						for (int i=0;i < folderPathList.size();i++){
							deleteDirs.add(folderPathList.get(i));
						}
						for (int i=0;i < packagePathList.size();i++){
							String packagePath = packagePathList.get(i);
							if (packagePath.indexOf("/module/") > -1){
								if (packagePath.endsWith("sqlmap")){
									continue;
								}else{
									String moduleName = deployManager.parseModuleName(packagePath);
									packagePath = packagePath.replaceAll("/classes/","/modules/"+moduleName);
									deleteDirs.add(packagePath);
								}
							}else{
								if (packagePath.endsWith("sqlmap")){
									continue;
								}
								deleteDirs.add(packagePathList.get(i));								
							}
						}
						HotServerManage hotServerManage = MiscdpUtil.getHotServerManage(projectConfig);
						hotServerManage.deployAppFiles(projectConfig.getProjectAlias(),deleteDirs, appFilesZip);
						
						ConsoleHandler.info("deploy files successfully !");	
					} catch (Exception e) {
						ConsoleHandler.error(e.getLocalizedMessage());
					}
					return Status.OK_STATUS;
				}
    		};
    		siteService.schedule(deployJob,0, true);
		}
	}
	
	public void selectionChanged(IAction action, ISelection selection) {
		
	}

	@Override
	public void init(IViewPart viewPart) {
		this.targetPart = viewPart;  
	}
}