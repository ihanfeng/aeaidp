﻿package com.agileai.miscdp.hotweb.ui.views;

import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

import com.agileai.miscdp.DeveloperConst;
import com.agileai.miscdp.util.ImageUtil;
/**
 * 功能树视图标题提供器
 */
public class TreeViewLabelProvider extends LabelProvider {
	public String getText(Object obj) {
		return obj.toString();
	}
	public Image getImage(Object obj) {
		TreeObject treeNode = (TreeObject)obj;
		if (TreeObject.NODE_COMPOSITE.equals(treeNode.getNodeType())){
			return ImageUtil.get("folder");
		}
		else if (TreeObject.NODE_PROJECT.equals(treeNode.getNodeType())){
			return ImageUtil.get("project");
		}
		else{
			String imageKey = "customize";
			if (DeveloperConst.STANDARD_FUNCMODEL_ID.equals(treeNode.getFuncType())){
				imageKey = "suconfignode";
			}
			if (DeveloperConst.MASTERSUB_FUNCMODEL_ID.equals(treeNode.getFuncType())){
				imageKey = "mastersub";
			}
			else if (DeveloperConst.TREECONTENT_FUNCMODEL_ID.equals(treeNode.getFuncType())){
				imageKey = "treecontentmodel";
			}			
			else if (DeveloperConst.QUERY_FUNCMODEL_ID.equals(treeNode.getFuncType())){
				imageKey = "queymodel";
			}
			else if (DeveloperConst.PICKFILL_FUNCMODEL_ID.equals(treeNode.getFuncType())){
				imageKey = "pickfillmodel";
			}
			else if (DeveloperConst.TREEFILL_FUNCMODEL_ID.equals(treeNode.getFuncType())){
				imageKey = "treefillmodel";
			}
			else if (DeveloperConst.TREEMANAGE_FUNCMODEL_ID.equals(treeNode.getFuncType())){
				imageKey = "treemanagemodel";
			}				
			else if (DeveloperConst.SIMPLE_FUNCMODEL_ID.equals(treeNode.getFuncType())){
				imageKey = "simplestmodel";
			}
			else if (DeveloperConst.PORTLET_FUNCMODEL_ID.equals(treeNode.getFuncType())){
				imageKey = "portletmodel";
			}			
			return ImageUtil.get(imageKey);
		}
	}
}
