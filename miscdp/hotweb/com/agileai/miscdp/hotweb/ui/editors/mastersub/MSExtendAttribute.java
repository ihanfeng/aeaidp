package com.agileai.miscdp.hotweb.ui.editors.mastersub;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.FormToolkit;

import com.agileai.miscdp.DeveloperConst;
import com.agileai.miscdp.hotweb.domain.mastersub.MasterSubFuncModel;
import com.agileai.miscdp.hotweb.domain.mastersub.SubTableConfig;
import com.agileai.miscdp.util.MiscdpUtil;
import com.agileai.miscdp.util.ResourceUtil;
import com.agileai.common.IniReader;
import com.agileai.domain.KeyNamePair;
import com.agileai.util.StringUtil;
/**
 * 扩展属性
 */
public class MSExtendAttribute extends Composite {
	private final FormToolkit toolkit = new FormToolkit(Display.getCurrent());
	
	private Button paginationBtn;
	private Button sortListBtn;
	private Button exportCsvBtn;
	private Button exportXslBtn;
	
	private Combo templateCombo;
	private Text detailJspNameText;
	private Text listJspNameText;
	
	private Text interfaceNameText;
	private Text serviceIdText;
	private Text implClassNameText;
	
	private MasterSubModelEditor modelEditor = null;
	private MasterSubFuncModel funcModel = null;
	
	@SuppressWarnings({"rawtypes" })
	public MSExtendAttribute(Composite parent, int style) {
		super(parent, style);
		final GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 2;
		setLayout(gridLayout);
		toolkit.adapt(this);
		toolkit.paintBordersFor(this);

		toolkit.createLabel(this, "业务标识", SWT.NONE);

		serviceIdText = toolkit.createText(this, null, SWT.NONE);
		final GridData gd_serviceIdText = new GridData(SWT.FILL, SWT.CENTER, true, false);
		serviceIdText.setLayoutData(gd_serviceIdText);
		serviceIdText.setEditable(false);
		
		final Label serviceimplLabel = toolkit.createLabel(this, "业务实现类", SWT.NONE);
		final GridData gd_serviceimplLabel = new GridData();
		serviceimplLabel.setLayoutData(gd_serviceimplLabel);

		implClassNameText = toolkit.createText(this, null, SWT.NONE);
		final GridData gd_implClassNameText = new GridData(SWT.FILL, SWT.CENTER, true, false);
		implClassNameText.setLayoutData(gd_implClassNameText);

		final Label serviceInterfaceLabel = toolkit.createLabel(this,"业务接口类", SWT.NONE);
		final GridData gd_serviceInterfaceLabel = new GridData();
		serviceInterfaceLabel.setLayoutData(gd_serviceInterfaceLabel);

		interfaceNameText = toolkit.createText(this, null, SWT.NONE);
		final GridData gd_interfaceNameText = new GridData(SWT.FILL, SWT.CENTER, true, false);
		interfaceNameText.setLayoutData(gd_interfaceNameText);
		interfaceNameText.setForeground(ResourceUtil.getColor(255, 255, 255));
		interfaceNameText.setBackground(ResourceUtil.getColor(128, 0, 255));
		interfaceNameText.addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent e) {
				String interfaceNameTemp = interfaceNameText.getText();
				int lastPoint = interfaceNameTemp.lastIndexOf(".");
				String interfaceShortNameTemp = interfaceNameTemp.substring(lastPoint+1,interfaceNameTemp.length());
				String implClassNameTemp = interfaceNameTemp.substring(0,lastPoint)
					+"."+interfaceShortNameTemp
					+"Impl";
				String serviceIdTemp = interfaceNameTemp.substring(lastPoint+1,interfaceNameTemp.length())
					+ "Service";
				
				String[] names = interfaceNameTemp.split("\\.");
				if (names.length >= DeveloperConst.PACKAGE_LENGTH){
					String pathTemp = "";
					for (int i= DeveloperConst.PACKAGE_STARTER;i < names.length-1;i++){
						pathTemp = pathTemp + names[i] + "/";
					}
					String listFileName = listJspNameText.getText();
					String editFileName = detailJspNameText.getText();
					lastPoint = listFileName.lastIndexOf("/")+1;
//					String listFileNameTemp = listFileName.substring(lastPoint,listFileName.length()); 
					listJspNameText.setText(pathTemp + interfaceShortNameTemp+"List.jsp");
					
					lastPoint = editFileName.lastIndexOf("/")+1;
					
//					String editFileNameTemp = editFileName.substring(lastPoint,editFileName.length()); 
					detailJspNameText.setText(pathTemp + interfaceShortNameTemp+"Edit.jsp");
					
					List<SubTableConfig> subTableConfigList = funcModel.getSubTableConfigs();
					
					for (int i=0;i < subTableConfigList.size();i++){
						SubTableConfig subTableConfig = subTableConfigList.get(i);
						if (SubTableConfig.LIST_DETAIL_MODE.equals(subTableConfig.getEditMode())){
							String tableName = subTableConfig.getTableName();
							String pboxNamePrefix = MiscdpUtil.getValidName(tableName);
							subTableConfig.setPboxJspName(pathTemp + pboxNamePrefix+"EditBox.jsp");							
						}
					}
				}
				serviceIdText.setText(StringUtil.lowerFirst(serviceIdTemp));
				implClassNameText.setText(implClassNameTemp);
			}
		});
		
		
		final Label filteableLabel_1 = new Label(this, SWT.NONE);
		filteableLabel_1.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false));
		toolkit.adapt(filteableLabel_1, true, true);
		filteableLabel_1.setText("列表属性");

		final Composite composite_1_1 = new Composite(this, SWT.NONE);
		composite_1_1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		final GridLayout gridLayout_1 = new GridLayout();
		gridLayout_1.numColumns = 4;
		gridLayout_1.marginWidth = 0;
		gridLayout_1.marginHeight = 2;
		gridLayout_1.horizontalSpacing = 0;
		composite_1_1.setLayout(gridLayout_1);
		composite_1_1.setBackground(ResourceUtil.getColor(255, 255, 255));
		toolkit.adapt(composite_1_1);

		exportXslBtn = new Button(composite_1_1, SWT.CHECK);
		exportXslBtn.setBackground(ResourceUtil.getColor(255, 255, 255));
		toolkit.adapt(exportXslBtn, true, true);
		exportXslBtn.setText("导出XSL");

		exportCsvBtn = new Button(composite_1_1, SWT.CHECK);
		exportCsvBtn.setBackground(ResourceUtil.getColor(255, 255, 255));
		toolkit.adapt(exportCsvBtn, true, true);
		exportCsvBtn.setText("导出CSV");

		sortListBtn = new Button(composite_1_1, SWT.CHECK);
		sortListBtn.setBackground(ResourceUtil.getColor(255, 255, 255));
		toolkit.adapt(sortListBtn, true, true);
		sortListBtn.setText("能否排序");

		paginationBtn = new Button(composite_1_1, SWT.CHECK);
		paginationBtn.setBackground(ResourceUtil.getColor(255, 255, 255));
		toolkit.adapt(paginationBtn, true, true);
		paginationBtn.setText("能否分页");

		final Label filteableLabel_1_1 = new Label(this, SWT.NONE);
		filteableLabel_1_1.setLayoutData(new GridData());
		toolkit.adapt(filteableLabel_1_1, true, true);
		filteableLabel_1_1.setText("列表JSP");

		listJspNameText = toolkit.createText(this, null, SWT.NONE);
		final GridData gd_listJspNameText = new GridData(SWT.FILL, SWT.CENTER, true, false);
		listJspNameText.setLayoutData(gd_listJspNameText);

		final Label filteableLabel_1_1_1 = new Label(this, SWT.NONE);
		filteableLabel_1_1_1.setLayoutData(new GridData());
		toolkit.adapt(filteableLabel_1_1_1, true, true);
		filteableLabel_1_1_1.setText("明细JSP");

		detailJspNameText = toolkit.createText(this, null, SWT.NONE);
		final GridData gd_detailJspNameText = new GridData(SWT.FILL, SWT.CENTER, true, false);
		detailJspNameText.setLayoutData(gd_detailJspNameText);

		final Label filteableLabel_1_1_1_1 = new Label(this, SWT.NONE);
		filteableLabel_1_1_1_1.setLayoutData(new GridData());
		toolkit.adapt(filteableLabel_1_1_1_1, true, true);
		filteableLabel_1_1_1_1.setText("功能模板");

		templateCombo = new Combo(this, SWT.NONE);

		IniReader reader = MiscdpUtil.getIniReader();
		List defValueList = reader.getList("MasterSubModelTemplates");
		for (int i=0;i < defValueList.size();i++){
			KeyNamePair keyNamePair = (KeyNamePair)defValueList.get(i);
			templateCombo.add(keyNamePair.getKey());
		}
		toolkit.adapt(templateCombo, true, true);
		final GridData gd_templateCombo = new GridData(SWT.FILL, SWT.CENTER, true, false);
		templateCombo.setLayoutData(gd_templateCombo);
	}

	public Button getPaginationBtn() {
		return paginationBtn;
	}

	public Button getSortListBtn() {
		return sortListBtn;
	}

	public Button getExportCsvBtn() {
		return exportCsvBtn;
	}

	public Button getExportXslBtn() {
		return exportXslBtn;
	}

	public Combo getTemplateCombo() {
		return templateCombo;
	}

	public Text getDetailJspNameText() {
		return detailJspNameText;
	}
	public Text getListJspNameText() {
		return listJspNameText;
	}
	public Text getInterfaceNameText() {
		return interfaceNameText;
	}
	public Text getServiceIdText() {
		return serviceIdText;
	}
	public Text getImplClassNameText() {
		return implClassNameText;
	}

	public MasterSubModelEditor getModelEditor() {
		return modelEditor;
	}

	public void setModelEditor(MasterSubModelEditor modelEditor) {
		this.modelEditor = modelEditor;
		this.funcModel = modelEditor.getFuncModel();
	}
}
