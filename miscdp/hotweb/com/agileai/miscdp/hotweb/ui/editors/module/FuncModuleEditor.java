package com.agileai.miscdp.hotweb.ui.editors.module;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import com.agileai.miscdp.hotweb.domain.FuncModule;
import com.agileai.miscdp.hotweb.ui.editors.SimpleModelEditor;
import com.agileai.miscdp.hotweb.ui.views.TreeObject;
/**
 * 功能模块编辑器
 */
public class FuncModuleEditor extends SimpleModelEditor{
	private FuncModule funcModule;

	protected void init() {
		funcModule = (FuncModule)this.getEditorInput();
	}
	public void doSave(IProgressMonitor monitor) {
		String funcName = funcModule.getModuleName();
		this.isModified = false;
		firePropertyChange(PROP_DIRTY);
		
		TreeViewer funcTreeViewer = funcModelTree.getFuncTreeViewer();
		ISelection selection = funcTreeViewer.getSelection();
		Object obj = ((IStructuredSelection) selection).getFirstElement();
		TreeObject treeObject = null;
		if (obj instanceof TreeObject) {
			treeObject = (TreeObject) obj;
			treeObject.setName(funcName);
			treeObject.setFuncSubPkg(funcModule.getModuleCode());
		}
		updateIndexFile(funcModule.getProjectName());
		this.setPartName(funcName);
		funcTreeViewer.refresh();
	}
	public void createPartControl(Composite composite) {
		new FuncModulePanel(composite,SWT.NONE,this,funcModule.isHasChidren());
	}
	public FuncModule getFuncModule() {
		return funcModule;
	}
}
