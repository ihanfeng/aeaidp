package com.agileai.miscdp.hotweb.ui.editors.project;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.forms.widgets.FormToolkit;

import com.agileai.miscdp.MiscdpPlugin;
import com.agileai.miscdp.hotweb.domain.ProjectConfig;
import com.agileai.miscdp.hotweb.domain.ServerConfig;
import com.agileai.miscdp.server.HotServerManage;
import com.agileai.miscdp.util.DialogUtil;
import com.agileai.miscdp.util.MiscdpUtil;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;

public class ServerConfigSelectDialog extends Dialog {
	private TableViewer tableViewer;
	private Table table;
	private final FormToolkit formToolkit = new FormToolkit(Display.getDefault());
	private List<ServerConfig> serverConfigs = null;

	private ProjectConfig projectConfig = null;
	private ServerConfig selectServerConfig = null;

	public ServerConfigSelectDialog(Shell parentShell,ProjectConfig projectConfig) {
		super(parentShell);
		this.serverConfigs = new ArrayList<ServerConfig>();
		this.projectConfig = projectConfig;
        if (projectConfig.getServerConfigs().isEmpty()){
        	ServerConfig config = new ServerConfig();
        	config.setConfigAlias(projectConfig.getConfigAlias());
        	config.setProjectAlias(projectConfig.getProjectAlias());
        	config.setServerAddress(projectConfig.getServerAddress());
        	config.setServerPort(projectConfig.getServerPort());
        	config.setServerUserId(projectConfig.getServerUserId());
        	config.setServerUserPwd(projectConfig.getServerUserPwd());
        	this.serverConfigs.add(config);
        }else{
        	for (int i=0;i < projectConfig.getServerConfigs().size();i++){
        		ServerConfig tempConfig = projectConfig.getServerConfigs().get(i);
        		ServerConfig config = new ServerConfig();
            	config.setConfigAlias(tempConfig.getConfigAlias());
            	config.setProjectAlias(tempConfig.getProjectAlias());
            	config.setServerAddress(tempConfig.getServerAddress());
            	config.setServerPort(tempConfig.getServerPort());
            	config.setServerUserId(tempConfig.getServerUserId());
            	config.setServerUserPwd(tempConfig.getServerUserPwd());
            	this.serverConfigs.add(config);
        	}
        }
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new GridLayout(2, false));
		
		tableViewer = new TableViewer(container, SWT.BORDER | SWT.FULL_SELECTION);
		table = tableViewer.getTable();
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDoubleClick(MouseEvent e) {
				okPressed();
			}
		});
		table.setLinesVisible(true);
		table.setHeaderVisible(true);
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		TableColumn tableColumn = new TableColumn(table, SWT.NONE);
		tableColumn.setWidth(81);
		tableColumn.setText("别名");
		
		TableColumn tableColumn_1 = new TableColumn(table, SWT.NONE);
		tableColumn_1.setWidth(89);
		tableColumn_1.setText("应用别名");
		
		TableColumn tableColumn_2 = new TableColumn(table, SWT.NONE);
		tableColumn_2.setWidth(107);
		tableColumn_2.setText("地址");
		
		TableColumn tblclmnNewColumn = new TableColumn(table, SWT.NONE);
		tblclmnNewColumn.setWidth(72);
		tblclmnNewColumn.setText("端口");
		
		TableColumn tableColumn_3 = new TableColumn(table, SWT.NONE);
		tableColumn_3.setWidth(100);
		tableColumn_3.setText("用户名");
		
		tableViewer.setLabelProvider(new ServerConfigLabelProvider());
		tableViewer.setContentProvider(new ArrayContentProvider());
		tableViewer.setInput(this.serverConfigs);
		
		Composite composite = new Composite(container, SWT.NONE);
		composite.setLayout(new GridLayout(1, false));
		composite.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, false, false, 1, 1));

		
		final Shell parentShell = getShell();
		Button selectButton = formToolkit.createButton(composite, "选择", SWT.NONE);
		selectButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				okPressed();
			}
		});
		
		Button createButton = formToolkit.createButton(composite, "新增", SWT.NONE);
		createButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				ServerConfigEditDialog serverConfigEditDialog = new ServerConfigEditDialog(parentShell,null,ServerConfigEditDialog.ActionTypes.Create);
				serverConfigEditDialog.open();
				if (serverConfigEditDialog.getReturnCode() == Dialog.OK){
					ServerConfig serverConfig = serverConfigEditDialog.getServerConfig();
					serverConfigs.add(serverConfig);
					
					tableViewer.refresh();
					
					projectConfig.getServerConfigs().clear();
					projectConfig.getServerConfigs().addAll(serverConfigs);
					projectConfig.saveHotwebConfig();
				}
			}
		});
		
		Button copyButton = formToolkit.createButton(composite, "复制", SWT.NONE);
		copyButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				int selectIndex = table.getSelectionIndex();
				if (selectIndex < 0){
					DialogUtil.showInfoMessage("请先选中一条记录");
					return;
				}
				
				ServerConfig selectedServerConfig = serverConfigs.get(selectIndex);
				ServerConfig serverConfig = new ServerConfig();
				try {
					BeanUtils.copyProperties(serverConfig, selectedServerConfig);					
				} catch (Exception e2) {
					MiscdpPlugin.getDefault().logError(e2.getLocalizedMessage(), e2);
				}
				
				ServerConfigEditDialog serverConfigEditDialog = new ServerConfigEditDialog(parentShell,serverConfig,ServerConfigEditDialog.ActionTypes.Copy);
				serverConfigEditDialog.open();
				if (serverConfigEditDialog.getReturnCode() == Dialog.OK){
					serverConfigs.add(serverConfig);
					tableViewer.refresh();
					
					projectConfig.getServerConfigs().clear();
					projectConfig.getServerConfigs().addAll(serverConfigs);
					projectConfig.saveHotwebConfig();
				}
			}
		});
		
		Button editButton = formToolkit.createButton(composite, "修改", SWT.NONE);
		editButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				int selectIndex = table.getSelectionIndex();
				if (selectIndex < 0){
					DialogUtil.showInfoMessage("请先选中一条记录");
					return;
				}
				ServerConfig selectedServerConfig = serverConfigs.get(selectIndex);
				ServerConfigEditDialog serverConfigEditDialog = new ServerConfigEditDialog(parentShell,selectedServerConfig,ServerConfigEditDialog.ActionTypes.Update);
				serverConfigEditDialog.open();
				if (serverConfigEditDialog.getReturnCode() == Dialog.OK){
					tableViewer.refresh();
					
					projectConfig.getServerConfigs().clear();
					projectConfig.getServerConfigs().addAll(serverConfigs);
					projectConfig.saveHotwebConfig();
				}
			}
		});
		
		Button deleteButton = formToolkit.createButton(composite, "删除", SWT.NONE);
		
		Label lblNewLabel = new Label(composite, SWT.NONE);
		formToolkit.adapt(lblNewLabel, true, true);
		
		Button btnNewButton = new Button(composite, SWT.NONE);
		btnNewButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				int selectIndex = table.getSelectionIndex();
				if (selectIndex < 0){
					DialogUtil.showInfoMessage("请先选中一条记录");
					return;
				}
				ServerConfig selectedServerConfig = serverConfigs.get(selectIndex);
				try{
					String appName = selectedServerConfig.getProjectAlias();
					String serverAdress = selectedServerConfig.getServerAddress();
					String serverPort = selectedServerConfig.getServerPort();
					String serverUserId = selectedServerConfig.getServerUserId();
					String serverUserPwd = selectedServerConfig.getServerUserPwd();
					HotServerManage hotServerManage = MiscdpUtil.getHotServerManage(appName,serverAdress,serverPort,serverUserId,serverUserPwd);
					hotServerManage.isExistContext(appName);
					DialogUtil.showMessage(getShell(),"OK","连接服务器成功！",DialogUtil.MESSAGE_TYPE.INFO);
				} catch (Exception ex) {
					DialogUtil.showMessage(getShell(),"消息提示","连接服务器失败！",DialogUtil.MESSAGE_TYPE.WARN);
					MiscdpPlugin.getDefault().logError(ex.getLocalizedMessage(), ex);
				}
			}
		});
		formToolkit.adapt(btnNewButton, true, true);
		btnNewButton.setText("测试");
		deleteButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				int selectIndex = table.getSelectionIndex();
				if (selectIndex < 0){
					DialogUtil.showInfoMessage("请先选中一条记录");
					return;
				}
				serverConfigs.remove(selectIndex);
				tableViewer.refresh();
				
				projectConfig.getServerConfigs().clear();
				projectConfig.getServerConfigs().addAll(serverConfigs);
				projectConfig.saveHotwebConfig();
			}
		});
		return container;
	}
	@Override
	protected void okPressed() {
		int selectIndex = table.getSelectionIndex();
		if (selectIndex < 0){
			DialogUtil.showInfoMessage("请先选中一条记录");
			return;
		}
		this.selectServerConfig = this.serverConfigs.get(selectIndex);

		projectConfig.setConfigAlias(selectServerConfig.getConfigAlias());
		projectConfig.setProjectAlias(selectServerConfig.getProjectAlias());
		projectConfig.setServerAddress(selectServerConfig.getServerAddress());
		projectConfig.setServerPort(selectServerConfig.getServerPort());
		projectConfig.setServerUserId(selectServerConfig.getServerUserId());
		projectConfig.setServerUserPwd(selectServerConfig.getServerUserPwd());
		
		projectConfig.saveHotwebConfig();
		super.okPressed();
	}
	
	public ServerConfig getSelectServerConfig() {
		return selectServerConfig;
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	@Override
	protected Point getInitialSize() {
		return new Point(555, 463);
	}
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("配置选择");
	}	
}
class ServerConfigLabelProvider extends LabelProvider implements ITableLabelProvider {
	public String getColumnText(Object element, int columnIndex) {
		if (element instanceof ServerConfig) {
			ServerConfig p = (ServerConfig) element;
			if (columnIndex == 0) {
				return p.getConfigAlias();
			} else if (columnIndex == 1) {
				return p.getProjectAlias();
			} else if (columnIndex == 2) {
				return p.getServerAddress();
			} else if (columnIndex == 3) {
				return p.getServerPort();	
			} else if (columnIndex == 4) {
				return p.getServerUserId();			
			} else if (columnIndex == 5) {
				return p.getServerUserPwd();
			}
		}
		return null;
	}
	public Image getColumnImage(Object element, int columnIndex) {
		return null;
	}
}
