package com.agileai.miscdp.hotweb.ui.editors.treecontent;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.FormToolkit;

import com.agileai.miscdp.DeveloperConst;
import com.agileai.miscdp.hotweb.domain.treecontent.TreeContentFuncModel;
import com.agileai.miscdp.util.ResourceUtil;
import com.agileai.util.StringUtil;
/**
 * 扩展属性
 */
public class TACExtendAttribute extends Composite {
	private final FormToolkit toolkit = new FormToolkit(Display.getCurrent());
	
	private Combo templateCombo;
	private Text listJspNameText;
	
	private Text interfaceNameText;
	private Text serviceIdText;
	private Text implClassNameText;
	
	public TACExtendAttribute(Composite parent, int style) {
		super(parent, style);
		final GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 2;
		setLayout(gridLayout);
		toolkit.adapt(this);
		toolkit.paintBordersFor(this);

		toolkit.createLabel(this, "业务标识", SWT.NONE);

		serviceIdText = toolkit.createText(this, null, SWT.NONE);
		serviceIdText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		serviceIdText.setEditable(false);
		
		toolkit.createLabel(this, "业务实现类", SWT.NONE);

		implClassNameText = toolkit.createText(this, null, SWT.NONE);
		implClassNameText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		toolkit.createLabel(this,"业务接口类", SWT.NONE);

		interfaceNameText = toolkit.createText(this, null, SWT.NONE);
		interfaceNameText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		interfaceNameText.setForeground(ResourceUtil.getColor(255, 255, 255));
		interfaceNameText.setBackground(ResourceUtil.getColor(128, 0, 255));
		interfaceNameText.addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent e) {
				String interfaceNameTemp = interfaceNameText.getText();
				int lastPoint = interfaceNameTemp.lastIndexOf(".");
				String interfaceShortNameTemp = interfaceNameTemp.substring(lastPoint+1,interfaceNameTemp.length());
				String implClassNameTemp = interfaceNameTemp.substring(0,lastPoint)
					+"."+interfaceShortNameTemp
					+"Impl";
				String serviceIdTemp = interfaceNameTemp.substring(lastPoint+1,interfaceNameTemp.length())
					+ "Service";
				
				String[] names = interfaceNameTemp.split("\\.");
				if (names.length >= DeveloperConst.PACKAGE_LENGTH){
					String pathTemp = "";
					for (int i= DeveloperConst.PACKAGE_STARTER;i < names.length-1;i++){
						pathTemp = pathTemp + names[i] + "/";
					}
					String listFileName = listJspNameText.getText();
					lastPoint = listFileName.lastIndexOf("/")+1;
//					String listFileNameTemp = listFileName.substring(lastPoint,listFileName.length()); 
					listJspNameText.setText(pathTemp + interfaceShortNameTemp+"List.jsp");
					
					
//					String editFileNameTemp = editFileName.substring(lastPoint,editFileName.length()); 
				}
				serviceIdText.setText(StringUtil.lowerFirst(serviceIdTemp));
				implClassNameText.setText(implClassNameTemp);
			}
		});

		final Label filteableLabel_1_1 = new Label(this, SWT.NONE);
		toolkit.adapt(filteableLabel_1_1, true, true);
		filteableLabel_1_1.setText("主页面JSP");

		listJspNameText = toolkit.createText(this, null, SWT.NONE);
		listJspNameText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		final Label filteableLabel_1_1_1_1 = new Label(this, SWT.NONE);
		toolkit.adapt(filteableLabel_1_1_1_1, true, true);
		filteableLabel_1_1_1_1.setText("功能模板");

		templateCombo = new Combo(this, SWT.NONE);

//		IniReader reader = MiscdpUtil.getIniReader();
//		List defValueList = reader.getList("TemplateDefine");
//		for (int i=0;i < defValueList.size();i++){
//			KeyNamePair keyNamePair = (KeyNamePair)defValueList.get(i);
//			templateCombo.add(keyNamePair.getKey());
//		}
		toolkit.adapt(templateCombo, true, true);
		templateCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
	}
	
	@SuppressWarnings("rawtypes")
	public void refreshTemplateCombo(TreeContentModelEditor modelEditor){
		boolean isManageTree = modelEditor.getBasicConfigPage().getIsManageTreeY().getSelection();
		List input = (List)modelEditor.getBasicConfigPage().getTableViewer().getInput();
		boolean isOneRelateTable = input.size() == 1;
		String treeEditModel = modelEditor.getBasicConfigPage().getTreeEditModeCombo().getText();
		if (templateCombo.getItemCount() > 0){
			templateCombo.setItems(new String[]{});	
			templateCombo.setText("");
		}
		if (isManageTree && TreeContentFuncModel.TreeEditMode.Tabed.toString().equals(treeEditModel)){
			templateCombo.add(TreeContentFuncModel.Template.ListTabedIncludeBaseTabed);
		}
		else if (isManageTree && TreeContentFuncModel.TreeEditMode.Popup.toString().equals(treeEditModel) && isOneRelateTable){
			templateCombo.add(TreeContentFuncModel.Template.List.toString());
		}
		else if (isManageTree && TreeContentFuncModel.TreeEditMode.Popup.toString().equals(treeEditModel) && !isOneRelateTable){
			templateCombo.add(TreeContentFuncModel.Template.ListTabedIncludeBasePoped);
		}
		else if (!isManageTree && isOneRelateTable){
			templateCombo.add(TreeContentFuncModel.Template.List.toString());
		}
		else if (!isManageTree && !isOneRelateTable){
			templateCombo.add(TreeContentFuncModel.Template.ListTabedIncludeBasePoped);
		}
		templateCombo.select(0);
	}
	
	public Combo getTemplateCombo() {
		return templateCombo;
	}

	public Text getListJspNameText() {
		return listJspNameText;
	}
	public Text getInterfaceNameText() {
		return interfaceNameText;
	}
	public Text getServiceIdText() {
		return serviceIdText;
	}
	public Text getImplClassNameText() {
		return implClassNameText;
	}
}
