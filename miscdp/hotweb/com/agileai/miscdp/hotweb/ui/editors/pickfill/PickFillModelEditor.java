﻿package com.agileai.miscdp.hotweb.ui.editors.pickfill;

import java.io.File;
import java.io.StringWriter;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.ide.IDE;

import com.agileai.miscdp.hotweb.domain.BaseFuncModel;
import com.agileai.miscdp.hotweb.domain.ListTabColumn;
import com.agileai.miscdp.hotweb.domain.PageParameter;
import com.agileai.miscdp.hotweb.domain.pickfill.PickFillFuncModel;
import com.agileai.miscdp.hotweb.ui.editors.BaseModelEditor;
import com.agileai.miscdp.hotweb.ui.views.FuncModelTreeView;
import com.agileai.miscdp.hotweb.ui.views.TreeObject;
import com.agileai.miscdp.util.DialogUtil;
import com.agileai.miscdp.util.MiscdpUtil;
import com.agileai.hotweb.model.ListTableType;
import com.agileai.hotweb.model.ListTableType.Row;
import com.agileai.hotweb.model.QueryBarType;
import com.agileai.hotweb.model.pickfill.PickFillFuncModelDocument;
import com.agileai.hotweb.model.pickfill.PickFillFuncModelDocument.PickFillFuncModel.BaseInfo;
import com.agileai.hotweb.model.pickfill.PickFillFuncModelDocument.PickFillFuncModel.BaseInfo.Service;
import com.agileai.hotweb.model.pickfill.PickFillFuncModelDocument.PickFillFuncModel.BaseInfo.SqlResource;
import com.agileai.hotweb.model.pickfill.PickFillFuncModelDocument.PickFillFuncModel.ListView;
import com.agileai.util.FileUtil;
import com.agileai.util.StringUtil;
/**
 * 选择框功能模型编辑器
 */
public class PickFillModelEditor extends BaseModelEditor{
	private PFMBasicConfigPage qMBasicConfigPage;
	private PFMListJspCfgPage qMListJspCfgPage;
	
    public void init(IEditorSite site, IEditorInput input) throws PartInitException {
        setSite(site);
        setInput(input);
        setPartName(input.getName());
		init();
		ResourcesPlugin.getWorkspace().addResourceChangeListener(this);
    }
	private void init() {
		funcModel = (PickFillFuncModel)this.getEditorInput();
	}

	protected void createPages() {
		qMBasicConfigPage = new PFMBasicConfigPage(getContainer(),this,SWT.NONE);
		qMListJspCfgPage = new PFMListJspCfgPage(getContainer(),this,SWT.NONE);
		setFuncModel((PickFillFuncModel)funcModel);
		
		String basicConfig = MiscdpUtil.getIniReader().getValue("EditorInfo","BasicInfo");
		int basicConfigIndex = addPage(qMBasicConfigPage);
		setPageText(basicConfigIndex,basicConfig);
		qMBasicConfigPage.createContent();
		
		String listJspConfig = MiscdpUtil.getIniReader().getValue("EditorInfo","ListInfo");
		int listJspCfgIndex = addPage(qMListJspCfgPage);
		setPageText(listJspCfgIndex,listJspConfig);
		qMListJspCfgPage.createContent();
		
		initValues();
		
		qMBasicConfigPage.registryModifier();
		qMListJspCfgPage.registryModifier();
	}
	public void dispose() {
		ResourcesPlugin.getWorkspace().removeResourceChangeListener(this);
		super.dispose();
	}
	public void doSave(IProgressMonitor monitor) {
		try {
			PickFillFuncModel pickFillFuncModel = buildFuncModel();
	    	String projectName = pickFillFuncModel.getProjectName();
	    	IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject(projectName);
	    	
	    	String listJspName = qMBasicConfigPage.getQMExtendAttribute().getListJspNameText().getText();
	    	if (StringUtil.isNullOrEmpty(listJspName)){
	    		String listJspCheckNullMsg = MiscdpUtil.getIniReader().getValue("PickFillModelEditor","ListJspCheckNullMsg");
	    		DialogUtil.showInfoMessage(listJspCheckNullMsg);
	    		return;
	    	}
	    	PickFillFuncModelDocument funcModelDocument = (PickFillFuncModelDocument)buildFuncModelDocument(pickFillFuncModel);
			StringWriter writer = new StringWriter();
			funcModelDocument.save(writer);
			String funcName = pickFillFuncModel.getFuncName();
			String funcSubPkg = pickFillFuncModel.getFuncSubPkg();
			
			String funcDefine = writer.toString();
			String funcId = pickFillFuncModel.getFuncId();
			String fileName = MiscdpUtil.getFuncFileName(projectName,funcId);
			FileUtil.writeFile(fileName, funcDefine);
			
			this.isModified = false;
			firePropertyChange(PROP_DIRTY);
			
			TreeViewer funcTreeViewer = funcModelTree.getFuncTreeViewer();
			ISelection selection = funcTreeViewer.getSelection();
			Object obj = ((IStructuredSelection) selection).getFirstElement();
			TreeObject treeObject = null;
			if (obj instanceof TreeObject) {
				treeObject = (TreeObject) obj;
				treeObject.setName(funcName);
				treeObject.setFuncSubPkg(funcSubPkg);
			}
			
			updateIndexFile(projectName);
			
			this.setPartName(funcName);
			funcTreeViewer.refresh();
			try {
				funcModel.buildFuncModel(funcDefine);
				project.refreshLocal(IResource.DEPTH_INFINITE, monitor);
			} catch (CoreException e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void doSaveAs() {
	}
	public void gotoMarker(IMarker marker) {
		setActivePage(0);
		IDE.gotoMarker(getEditor(0), marker);
	}
	public boolean isDirty(){
		return this.isModified;
	}
	public void setActivePage(int pageIndex) {
		super.setActivePage(pageIndex);
	}
	public boolean isSaveAsAllowed() {
		return false;
	}
	public void fireDirty(){
		firePropertyChange(PROP_DIRTY);
	}
	protected void pageChange(int newPageIndex) {
		super.pageChange(newPageIndex);
	}
	public void resourceChanged(final IResourceChangeEvent event){
	}
	public PFMBasicConfigPage getBasicConfigPage() {
		return qMBasicConfigPage;
	}
	public PFMListJspCfgPage getListJspCfgPage() {
		return qMListJspCfgPage;
	}
	public void setFuncModel(PickFillFuncModel suFuncModel) {
		this.qMBasicConfigPage.setFuncModel(suFuncModel);
		this.qMListJspCfgPage.setFuncModel(suFuncModel);
	}
	public PickFillFuncModel buildFuncModel() {
		this.qMBasicConfigPage.buildConfig();
		return this.qMListJspCfgPage.buildConfig();
	}
	public void initValues() {
		this.qMBasicConfigPage.initValues();
		this.qMListJspCfgPage.initValues();
	}
	public PickFillFuncModel getFuncModel() {
		return (PickFillFuncModel)funcModel;
	}
	public void setFuncModelTree(FuncModelTreeView funcModelTree) {
		this.funcModelTree = funcModelTree;
	}
	public void setModified(boolean isModified) {
		this.isModified = isModified;
	}
	public Object buildFuncModelDocument(BaseFuncModel baseFuncModel) {
		PickFillFuncModel pickFillFuncModel = (PickFillFuncModel)baseFuncModel;
		PickFillFuncModelDocument funcModelDocument = PickFillFuncModelDocument.Factory.newInstance();
		PickFillFuncModelDocument.PickFillFuncModel funcModel = funcModelDocument.addNewPickFillFuncModel();
		BaseInfo baseInfo = funcModel.addNewBaseInfo();
		String listJspName = pickFillFuncModel.getListJspName();
		
		baseInfo.setTemplate(pickFillFuncModel.getTemplate());
		baseInfo.setListJspName(listJspName);
		baseInfo.setListTitle(pickFillFuncModel.getListTitle());
		baseInfo.setQueryListSql(pickFillFuncModel.getListSql());
		baseInfo.setCodeField(pickFillFuncModel.getCodeField());
		baseInfo.setNameField(pickFillFuncModel.getNameField());
		baseInfo.setIsMultiSelect(pickFillFuncModel.getIsMultiSelect());
		
		Service service = baseInfo.addNewService();
		String interfaceName = pickFillFuncModel.getInterfaceName();
		service.setServiceId(pickFillFuncModel.getServiceId());
		service.setImplClassName(pickFillFuncModel.getImplClassName());
		service.setInterfaceName(interfaceName);
		
		String listHandlerId = listJspName.substring(0,listJspName.length()-4);
		listHandlerId = listHandlerId.substring(listHandlerId.lastIndexOf("/")+1,listHandlerId.length());
		String tempClassPath = interfaceName.substring(0,interfaceName.lastIndexOf(".")).replace("service","handler"); 
		String listHandlerClass = tempClassPath + "." + listHandlerId + "Handler";
		BaseInfo.Handler handler = baseInfo.addNewHandler();
		handler.setListHandlerId(listHandlerId);
		handler.setListHandlerClass(listHandlerClass);
		
		String sqlMapFile = pickFillFuncModel.getSqlMapFile();
		if (StringUtil.isNullOrEmpty(sqlMapFile)){
			DialogUtil.showInfoMessage("SqlMap文件不能为空！");
			return null;
		}
		File tempFile = new File(sqlMapFile);
		String tempFileName = tempFile.getName();
		String sqlNameSpace = tempFileName.substring(0,tempFileName.length()-4);
		SqlResource sqlResource = baseInfo.addNewSqlResource();
		sqlResource.setSqlNameSpace(sqlNameSpace);
		
		String tempSqlMapFile = sqlMapFile.replace("\\", "/");
		sqlResource.setSqlMapPath(tempSqlMapFile);
		
		ListView listView = funcModel.addNewListView();
		List<PageParameter> pageParameters = pickFillFuncModel.getPageParameters();
		
		QueryBarType queryBarType = listView.addNewParameterArea(); 
		processQueryBarType(queryBarType, pageParameters);
		
		ListTableType listTableType = listView.addNewListTableArea();
		listTableType.setExportCsv(pickFillFuncModel.isExportCsv());
		listTableType.setExportXls(pickFillFuncModel.isExportXls());
		listTableType.setPagination(pickFillFuncModel.isPagination());
		listTableType.setSortAble(pickFillFuncModel.isSortAble());
		
		List<ListTabColumn> listTabColumns = pickFillFuncModel.getListTableColumns();
		Row row = listTableType.addNewRow();
		String rsIdColumn = StringUtils.join(pickFillFuncModel.getRsIdColumns().toArray());
		row.setRsIdColumn(rsIdColumn);
		
		for (int i=0;i < listTabColumns.size();i++){
			ListTabColumn listTabColumn = listTabColumns.get(i);
			ListTableType.Row.Column column = row.addNewColumn();
			column.setCell(listTabColumn.getCell());
			column.setProperty(listTabColumn.getProperty());
			column.setFormat(listTabColumn.getFormat());
			column.setTitle(listTabColumn.getTitle());
			column.setWidth(listTabColumn.getWidth());
			column.setMappingItem(listTabColumn.getMappingItem());
		}
		return funcModelDocument;
	}
	public PFMBasicConfigPage getQMBasicConfigPage() {
		return qMBasicConfigPage;
	}
	public PFMListJspCfgPage getQMListJspCfgPage() {
		return qMListJspCfgPage;
	}
}
