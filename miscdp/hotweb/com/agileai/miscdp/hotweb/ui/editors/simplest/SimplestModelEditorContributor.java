﻿package com.agileai.miscdp.hotweb.ui.editors.simplest;

import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IWorkbenchActionConstants;

import com.agileai.miscdp.DeveloperConst;
import com.agileai.miscdp.hotweb.ui.actions.simplest.SIMGenCodesAction;
import com.agileai.miscdp.hotweb.ui.editors.BaseModelEditorContributor;
import com.agileai.miscdp.util.ImageUtil;
/**
 * SimplestModelEditorContributor
 */
public class SimplestModelEditorContributor extends BaseModelEditorContributor {
	public SimplestModelEditorContributor() {
		super();
	}
	protected void createActions() {
		genCodesAction = new SIMGenCodesAction();
		genCodesAction.setText("生成代码");
		genCodesAction.setToolTipText("生成代码及配置信息");
		ImageDescriptor genCodesImageDesc = ImageUtil.getDescriptor("gencodes");
		genCodesAction.setImageDescriptor(genCodesImageDesc);
		((SIMGenCodesAction)genCodesAction).setContributor(this);
	}
	public void contributeToMenu(IMenuManager manager) {
		String path = "MisDevMenu"; 
		IMenuManager menu = manager.findMenuUsingPath(path);
		if (menu != null){
			manager.prependToGroup(IWorkbenchActionConstants.MB_ADDITIONS, menu);
			menu.add(genCodesAction);
			genCodesAction.setEnabled(true);
			menu.insertBefore(DeveloperConst.SHOW_PERSPECTIVE_ACTION_ID,genCodesAction);
		}
	}
	public void contributeToToolBar(IToolBarManager manager) {
		manager.add(new Separator());
		manager.add(genCodesAction);
	}
}
