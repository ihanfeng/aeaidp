package com.agileai.miscdp.hotweb.ui.editors.query;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.FormToolkit;

import com.agileai.miscdp.hotweb.domain.query.QueryFuncModel;
import com.agileai.miscdp.hotweb.ui.editors.Modifier;
import com.agileai.miscdp.util.ResourceUtil;
import com.agileai.util.StringUtil;
/**
 * 基础配置页面
 */
public class QMBasicConfigPage extends Composite implements Modifier {
	private Text listSqlText;
	private Text sqlMapFileText;
	private QueryModelEditor modelEditor = null;
	private Text funcNameText;
	private Text funcTypeText;
	private final FormToolkit toolkit = new FormToolkit(Display.getCurrent());
	private QMExtendAttribute qMExtendAttribute;

	private QueryFuncModel funcModel= null;
	private Text funcSubPkgText;
	private Button detailRadioButtonY = null;
	private Button detailRadioButtonN = null;
	
	/**
	 * Create the composite
	 * @param parent
	 * @param style
	 */
	public QMBasicConfigPage(Composite parent,QueryModelEditor suModelEditor, int style) {
		super(parent, style);
		toolkit.adapt(this);
		toolkit.paintBordersFor(this);
		this.modelEditor = suModelEditor;
//		createContent();
	}
	public void createContent(){
		final GridLayout gridLayout = new GridLayout();
		gridLayout.verticalSpacing = 2;
		gridLayout.marginWidth = 2;
		gridLayout.marginHeight = 2;
		gridLayout.horizontalSpacing = 2;
		gridLayout.numColumns = 3;
		setLayout(gridLayout);

		final Label label_1 = new Label(this, SWT.NONE);
		label_1.setText("功能名称");

		funcNameText = new Text(this, SWT.BORDER);
		toolkit.adapt(funcNameText, true, true);
		funcNameText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		new Label(this, SWT.NONE);

		final Label label = new Label(this, SWT.NONE);
		label.setText("功能类型");

		funcTypeText = new Text(this, SWT.READ_ONLY | SWT.BORDER);
		toolkit.adapt(funcTypeText, true, true);
		funcTypeText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		new Label(this, SWT.NONE);
		
		Label label_3 = new Label(this, SWT.NONE);
		label_3.setText("目录编码");
		
		funcSubPkgText = new Text(this, SWT.BORDER | SWT.READ_ONLY);
		funcSubPkgText.setEditable(false);
		funcSubPkgText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		toolkit.adapt(funcSubPkgText, true, true);
		new Label(this, SWT.NONE);
		
		Label label_4 = new Label(this, SWT.NONE);
		label_4.setText("显示明细");
		
		Composite composite_1 = new Composite(this, SWT.NONE);
		toolkit.adapt(composite_1);
		toolkit.paintBordersFor(composite_1);
		GridLayout gl_composite_1 = new GridLayout(2, false);
		gl_composite_1.marginHeight = 2;
		composite_1.setLayout(gl_composite_1);
		
		detailRadioButtonY = new Button(composite_1, SWT.RADIO);
		toolkit.adapt(detailRadioButtonY, true, true);
		detailRadioButtonY.setText("是");
		
		detailRadioButtonN = new Button(composite_1, SWT.RADIO);
		toolkit.adapt(detailRadioButtonN, true, true);
		detailRadioButtonN.setText("否");
		new Label(this, SWT.NONE);

		final Label targetPackageLabel_1_1 = new Label(this, SWT.NONE);
		targetPackageLabel_1_1.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false));
		targetPackageLabel_1_1.setText("SqlMap文件");

		sqlMapFileText = toolkit.createText(this, null, SWT.READ_ONLY);
		sqlMapFileText.setEditable(true);
		sqlMapFileText.setForeground(ResourceUtil.getColor(255, 255, 255));
		sqlMapFileText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		sqlMapFileText.setBackground(ResourceUtil.getColor(128, 0, 255));
		new Label(this, SWT.NONE);
		
		final Composite formComposite = new Composite(this, SWT.NONE);
		formComposite.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		formComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
		final GridLayout gridLayout_4 = new GridLayout();
		gridLayout_4.numColumns = 3;
		gridLayout_4.marginHeight = 2;
		gridLayout_4.horizontalSpacing = 0;
		gridLayout_4.verticalSpacing = 0;
		gridLayout_4.marginWidth = 0;
		formComposite.setLayout(gridLayout_4);

		final TabFolder tabFolder = new TabFolder(formComposite, SWT.NONE);
		final GridData gd_tabFolder = new GridData(SWT.FILL, SWT.FILL, true, true, 3, 3);
		gd_tabFolder.widthHint = 494;
		gd_tabFolder.heightHint = 254;
		tabFolder.setLayoutData(gd_tabFolder);
		toolkit.adapt(tabFolder, true, true);
		
		final TabItem tabItem_1 = new TabItem(tabFolder, SWT.NONE);
		tabItem_1.setText("列表SQL");
		final Composite composite = new Composite(tabFolder, SWT.NONE);
		composite.setLayout(new GridLayout());
		toolkit.adapt(composite);
		listSqlText = new Text(composite, SWT.BORDER | SWT.V_SCROLL | SWT.MULTI);
		toolkit.adapt(listSqlText, true, true);
		tabItem_1.setControl(composite);
		
		final TabItem tabItem = new TabItem(tabFolder, SWT.NONE);
		tabItem.setText("扩展属性");
		qMExtendAttribute = new QMExtendAttribute(tabFolder,SWT.NONE);
		tabItem.setControl(qMExtendAttribute);
		
		final GridData gd_listSqlText = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd_listSqlText.heightHint = 211;
		listSqlText.setLayoutData(gd_listSqlText);

		final Label label_2 = toolkit.createLabel(composite, "注意:列表SQL的字段应为大写!", SWT.NONE);
		label_2.setForeground(ResourceUtil.getColor(255, 0, 0));
	}
	public void dispose() {
		super.dispose();
	}
	protected void checkSubclass() {
	}
	public void setFuncModel(QueryFuncModel standardFuncModel) {
		this.funcModel = standardFuncModel;
	}
	public QueryFuncModel buildConfig(){
		funcModel.setFuncName(funcNameText.getText());
		funcModel.setListSql(listSqlText.getText());
//		funcModel.setFuncType(funcTypeText.getText());
		funcModel.setFuncSubPkg(funcSubPkgText.getText());
		funcModel.setShowDetail(detailRadioButtonY.getSelection());
		funcModel.setSqlMapFile(sqlMapFileText.getText());
		funcModel.setServiceId(qMExtendAttribute.getServiceIdText().getText());
		funcModel.setImplClassName(qMExtendAttribute.getImplClassNameText().getText());
		funcModel.setInterfaceName(qMExtendAttribute.getInterfaceNameText().getText());
		funcModel.setListJspName(qMExtendAttribute.getListJspNameText().getText());
		funcModel.setDetailJspName(qMExtendAttribute.getDetailJspNameText().getText());

		funcModel.setExportCsv(qMExtendAttribute.getExportCsvBtn().getSelection());
		funcModel.setExportXls(qMExtendAttribute.getExportXslBtn().getSelection());
		funcModel.setPagination(qMExtendAttribute.getPaginationBtn().getSelection());
		funcModel.setSortAble(qMExtendAttribute.getSortListBtn().getSelection());
		funcModel.setTemplate(qMExtendAttribute.getTemplateCombo().getItem(0));
		return this.funcModel;
	}

	public void initValues() {
		funcNameText.setText(this.funcModel.getFuncName());
		funcTypeText.setText(this.funcModel.getFuncTypeName());
		funcSubPkgText.setText(this.funcModel.getFuncSubPkg());
		
		if (StringUtil.isNullOrEmpty(this.funcModel.getSqlMapFile())){
			String sqlMapFileName = "SampleQueryToChangeIt.xml";
			sqlMapFileText.setText(sqlMapFileName);
			this.funcModel.setSqlMapFile(sqlMapFileName);
		}else{
			sqlMapFileText.setText(this.funcModel.getSqlMapFile());
		}
		
		listSqlText.setText(this.funcModel.getListSql());
		
		if (this.funcModel.isShowDetail()){
			detailRadioButtonY.setSelection(true);
			detailRadioButtonN.setSelection(false);
		}
		else{
			detailRadioButtonY.setSelection(false);
			detailRadioButtonN.setSelection(true);
		}
		
		qMExtendAttribute.getServiceIdText().setText(funcModel.getServiceId());
		qMExtendAttribute.getImplClassNameText().setText(funcModel.getImplClassName());
		qMExtendAttribute.getInterfaceNameText().setText(funcModel.getInterfaceName());
		qMExtendAttribute.getListJspNameText().setText(funcModel.getListJspName());
		qMExtendAttribute.getDetailJspNameText().setText(funcModel.getDetailJspName());
		qMExtendAttribute.getTemplateCombo().select(0);
		
		qMExtendAttribute.getExportCsvBtn().setSelection(funcModel.isExportCsv());
		qMExtendAttribute.getExportXslBtn().setSelection(funcModel.isExportXls());
		qMExtendAttribute.getPaginationBtn().setSelection(funcModel.isPagination());
		qMExtendAttribute.getSortListBtn().setSelection(funcModel.isSortAble());
	}
	public void registryModifier() {
		funcNameText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
//		funcSubPkgText.addModifyListener(new ModifyListener() {
//			public void modifyText(ModifyEvent e) {
//				if (!funcSubPkgText.getText().equals(funcModel.getFuncSubPkg())){
//					modelEditor.setModified(true);
//					modelEditor.fireDirty();				
//				}
//			}
//		});
		detailRadioButtonY.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		detailRadioButtonN.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		listSqlText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		qMExtendAttribute.getPaginationBtn().addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		qMExtendAttribute.getSortListBtn().addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		qMExtendAttribute.getExportCsvBtn().addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		qMExtendAttribute.getExportXslBtn().addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		qMExtendAttribute.getTemplateCombo().addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		qMExtendAttribute.getDetailJspNameText().addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		qMExtendAttribute.getListJspNameText().addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		qMExtendAttribute.getInterfaceNameText().addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		qMExtendAttribute.getImplClassNameText().addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		qMExtendAttribute.getServiceIdText().addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		sqlMapFileText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
	}
	public QMExtendAttribute getQMExtendAttribute() {
		return qMExtendAttribute;
	}
	public Text getListSqlText() {
		return listSqlText;
	}
	public Text getSqlMapFileText() {
		return sqlMapFileText;
	}
	public Text getFuncSubPkgText() {
		return funcSubPkgText;
	}
}
