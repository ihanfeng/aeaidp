package com.agileai.miscdp.hotweb.ui.editors.treemanage;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.FormToolkit;

import com.agileai.miscdp.DeveloperConst;
import com.agileai.miscdp.util.MiscdpUtil;
import com.agileai.miscdp.util.ResourceUtil;
import com.agileai.common.IniReader;
import com.agileai.domain.KeyNamePair;
import com.agileai.util.StringUtil;
/**
 * 扩展属性
 */
public class TMMExtendAttribute extends Composite {
	private final FormToolkit toolkit = new FormToolkit(Display.getCurrent());
	
	private Combo templateCombo;
	private Text jspNameText;
	
	private Text interfaceNameText;
	private Text serviceIdText;
	private Text implClassNameText;
	
	@SuppressWarnings("rawtypes")
	public TMMExtendAttribute(Composite parent, int style) {
		super(parent, style);
		final GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 2;
		setLayout(gridLayout);
		toolkit.adapt(this);
		toolkit.paintBordersFor(this);

		toolkit.createLabel(this, "业务标识", SWT.NONE);

		serviceIdText = toolkit.createText(this, null, SWT.NONE);
		final GridData gd_serviceIdText = new GridData(SWT.FILL, SWT.CENTER, true, false);
		serviceIdText.setLayoutData(gd_serviceIdText);
		serviceIdText.setEditable(false);
		
		final Label serviceimplLabel = toolkit.createLabel(this, "业务实现类", SWT.NONE);
		final GridData gd_serviceimplLabel = new GridData();
		serviceimplLabel.setLayoutData(gd_serviceimplLabel);

		implClassNameText = toolkit.createText(this, null, SWT.NONE);
		final GridData gd_implClassNameText = new GridData(SWT.FILL, SWT.CENTER, true, false);
		implClassNameText.setLayoutData(gd_implClassNameText);

		final Label serviceInterfaceLabel = toolkit.createLabel(this,"业务接口类" , SWT.NONE);
		final GridData gd_serviceInterfaceLabel = new GridData();
		serviceInterfaceLabel.setLayoutData(gd_serviceInterfaceLabel);

		interfaceNameText = toolkit.createText(this, null, SWT.NONE);
		final GridData gd_interfaceNameText = new GridData(SWT.FILL, SWT.CENTER, true, false);
		interfaceNameText.setLayoutData(gd_interfaceNameText);
		interfaceNameText.setForeground(ResourceUtil.getColor(255, 255, 255));
		interfaceNameText.setBackground(ResourceUtil.getColor(128, 0, 255));
		interfaceNameText.addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent e) {
				String interfaceNameTemp = interfaceNameText.getText();
				int lastPoint = interfaceNameTemp.lastIndexOf(".");
				String interfaceShortNameTemp = interfaceNameTemp.substring(lastPoint+1,interfaceNameTemp.length());
				String implClassNameTemp = interfaceNameTemp.substring(0,lastPoint)
					+"."+interfaceShortNameTemp
					+"Impl";
				String serviceIdTemp = interfaceNameTemp.substring(lastPoint+1,interfaceNameTemp.length())
					+ "Service";
				
				String[] names = interfaceNameTemp.split("\\.");
				if (names.length >= DeveloperConst.PACKAGE_LENGTH){
					String pathTemp = "";
					for (int i= DeveloperConst.PACKAGE_STARTER;i < names.length-1;i++){
						pathTemp = pathTemp + names[i] + "/";
					}
					String listFileName = jspNameText.getText();
					lastPoint = listFileName.lastIndexOf("/")+1;
					jspNameText.setText(pathTemp + interfaceShortNameTemp+".jsp");
				}
				serviceIdText.setText(StringUtil.lowerFirst(serviceIdTemp));
				implClassNameText.setText(implClassNameTemp);
			}
		});
		
		

		final Label filteableLabel_1_1 = new Label(this, SWT.NONE);
		filteableLabel_1_1.setLayoutData(new GridData());
		toolkit.adapt(filteableLabel_1_1, true, true);
		filteableLabel_1_1.setText("JSP名称");

		jspNameText = toolkit.createText(this, null, SWT.NONE);
		final GridData gd_listJspNameText = new GridData(SWT.FILL, SWT.CENTER, true, false);
		jspNameText.setLayoutData(gd_listJspNameText);


		final Label filteableLabel_1_1_1_1 = new Label(this, SWT.NONE);
		filteableLabel_1_1_1_1.setLayoutData(new GridData());
		toolkit.adapt(filteableLabel_1_1_1_1, true, true);
		filteableLabel_1_1_1_1.setText("功能模板");

		templateCombo = new Combo(this, SWT.NONE);

		IniReader reader = MiscdpUtil.getIniReader();
		List defValueList = reader.getList("TemplateDefine");
		for (int i=0;i < defValueList.size();i++){
			KeyNamePair keyNamePair = (KeyNamePair)defValueList.get(i);
			templateCombo.add(keyNamePair.getKey());
		}
		toolkit.adapt(templateCombo, true, true);
		final GridData gd_templateCombo = new GridData(SWT.FILL, SWT.CENTER, true, false);
		templateCombo.setLayoutData(gd_templateCombo);
	}

	public Combo getTemplateCombo() {
		return templateCombo;
	}
	public Text getJspNameText() {
		return jspNameText;
	}
	public Text getInterfaceNameText() {
		return interfaceNameText;
	}
	public Text getServiceIdText() {
		return serviceIdText;
	}
	public Text getImplClassNameText() {
		return implClassNameText;
	}
}
