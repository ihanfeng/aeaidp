﻿package com.agileai.miscdp.hotweb;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectNature;
import org.eclipse.core.runtime.CoreException;
/**
 * HotwebNature
 */
public class HotwebProjectNature implements IProjectNature{
	private IProject project = null;
	public void configure() throws CoreException {
	}
	public void deconfigure() throws CoreException {
	}
	public IProject getProject() {
		return this.project;
	}
	public void setProject(IProject project) {
		this.project = project;
	}
}
