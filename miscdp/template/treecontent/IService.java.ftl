<#ftl ns_prefixes={"D":"http://www.hotweb.agileai.com/model/treecontent",
"fa":"http://www.hotweb.agileai.com/model"}>
<#import "/common/Util.ftl" as Util>
<#import "/common/PageForm.ftl" as Form>
<#visit doc>
<#macro TreeContentFuncModel>
<#local baseInfo = .node.BaseInfo>
<#local service = baseInfo.Service>
package ${Util.parsePkg(service.@InterfaceName)};

import com.agileai.hotweb.bizmoduler.core.TreeAndContentManage;

public interface ${Util.parseClass(service.@InterfaceName)} extends TreeAndContentManage{

}
</#macro>