<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE sqlMap      
    PUBLIC "-//ibatis.apache.org//DTD SQL Map 2.0//EN"      
    "http://ibatis.apache.org/dtd/sql-map-2.dtd">
<sqlMap namespace="${table.namespace}">  
  <select id="findMasterRecords" parameterClass="com.agileai.domain.DataParam" resultClass="com.agileai.domain.DataRow">
	${table.findMasterRecordsSql}
  </select>
  <select id="getMasterRecord" parameterClass="com.agileai.domain.DataParam" resultClass="com.agileai.domain.DataRow">
    ${table.getMasterRecordSql}
  </select>
  <insert id="insertMasterRecord" parameterClass="com.agileai.domain.DataParam">
    ${table.insertMasterRecordSql}
  </insert>
  <update id="updateMasterRecord" parameterClass="com.agileai.domain.DataParam">
    ${table.updateMasterRecordSql}
  </update>
  <delete id="deleteMasterRecord" parameterClass="com.agileai.domain.DataParam">
    ${table.deleteMasterRecordSql}
  </delete>
  <#list table.subTableInfoList as subTableInfo>
   <select id="find${subTableInfo.subTableId}Records" parameterClass="com.agileai.domain.DataParam" resultClass="com.agileai.domain.DataRow">
   ${subTableInfo.findSubRecordsSql}
   </select>
<#if (subTableInfo.entryEdit?string = "false")>
   <select id="get${subTableInfo.subTableId}Record" parameterClass="com.agileai.domain.DataParam" resultClass="com.agileai.domain.DataRow">
   ${subTableInfo.getSubRecordSql}
   </select>
</#if>   
   <delete id="delete${subTableInfo.subTableId}Records" parameterClass="com.agileai.domain.DataParam">
   ${subTableInfo.deleteSubRecordsSql}
   </delete>    
   <delete id="delete${subTableInfo.subTableId}Record" parameterClass="com.agileai.domain.DataParam">
   ${subTableInfo.deleteSubRecordSql}
   </delete>  
   <insert id="insert${subTableInfo.subTableId}Record" parameterClass="com.agileai.domain.DataParam">
   ${subTableInfo.insertSubRecordSql}
   </insert>
   <update id="update${subTableInfo.subTableId}Record" parameterClass="com.agileai.domain.DataParam">
   ${subTableInfo.updateSubRecordSql}
   </update>       
  </#list>
</sqlMap>