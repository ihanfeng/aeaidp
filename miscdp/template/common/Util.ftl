<#function isValid theNode>
	<#if (theNode?exists && theNode?has_content)>
		<#return true>
	<#else>
		<#return false>
	</#if>
</#function>

<#function isValidValue theNodeValue>
	<#if (theNodeValue?exists && theNodeValue?has_content && theNodeValue?string != "")>
		<#return true>
	<#else>
		<#return false>
	</#if>
</#function> 

<#function isTrue theNode>
	<#if (theNode?exists && theNode?has_content && theNode?string="true")>
		<#return true>
	<#else>
		<#return false>
	</#if>
</#function> 

<#function isFalse theNode>
	<#if (theNode?exists && theNode?has_content && theNode?string="false")>
		<#return true>
	<#else>
		<#return false>
	</#if>
</#function> 

<#function parsePkg className>
	<#local endIndex = className?last_index_of(".")-1>
	<#return (className?string[0..endIndex])>
</#function> 

<#function parseClass className>
	<#local beginIndex = className?last_index_of(".")+1>
	<#local endIndex = className?length>
	<#return (className?string[beginIndex..endIndex-1])>
</#function>

<#function getLowerValidName nameValue>
	<#assign result = "">
	<#list nameValue?split("_") as tempValue>
		<#assign result= result + tempValue?lower_case?cap_first>
	</#list>
	<#return result?uncap_first>
</#function>

<#function getUpperValidName nameValue>
	<#assign result = "">
	<#list nameValue?split("_") as tempValue>
		<#assign result= result + tempValue?lower_case?cap_first>
	</#list>
	<#return result?cap_first>
</#function>

<#function parseRsIdField rsIdColumn>
	<#assign result = "">
	<#list rsIdColumn?split(",") as rsId>
		<#assign result= result + rsId +":" + "'$"+"{" +"row." + rsId +"}'"+",">
	</#list>
	<#local endIndex = result?length>
	<#return "{"+result?string[0..endIndex-2]+"}">
</#function>

<#function parseTreeContentRsIdField rsIdColumn curColumnId>
	<#assign result = "">
	<#list rsIdColumn?split(",") as rsId>
		<#assign result= result + rsId +":" + "'$"+"{" +"row." + rsId +"}'"+",">
	</#list>
	<#assign result= result + "curColumnId:" + "'$"+"{" +"row." + curColumnId +"}'"+",">
	<#local endIndex = result?length>
	<#return "{"+result?string[0..endIndex-2]+"}">
</#function>

<#function parseRsIdTag rsIdColumn>
	<#assign result = "">
	<#list rsIdColumn?split(",") as rsId>
		<#assign result= result + rsId +",">
	</#list>
	<#local endIndex = result?length>
	<#return "'"+result?string[0..endIndex-2]+"'">
</#function>

<#function parseRsIdName rsIdColumn>
	<#assign result = "">
	<#list rsIdColumn?split(",") as rsId>
		<#assign result= result + rsId +",">
	</#list>
	<#local endIndex = result?length>
	<#return result?string[0..endIndex-2]>
</#function>