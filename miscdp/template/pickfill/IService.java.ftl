<#ftl ns_prefixes={"D":"http://www.hotweb.agileai.com/model/pickfill",
"fa":"http://www.hotweb.agileai.com/model"}>
<#import "/common/Util.ftl" as Util>
<#import "/common/PageForm.ftl" as Form>
<#visit doc>
<#macro PickFillFuncModel>
<#local baseInfo = .node.BaseInfo>
<#local service = baseInfo.Service>
package ${Util.parsePkg(service.@InterfaceName)};

import com.agileai.hotweb.bizmoduler.core.PickFillModelService;

public interface  ${Util.parseClass(service.@InterfaceName)}  extends PickFillModelService{

}
</#macro>