<#ftl ns_prefixes={"D":"http://www.hotweb.agileai.com/model/treemanage",
"fa":"http://www.hotweb.agileai.com/model"}>
<#import "/common/Util.ftl" as Util>
<#import "/common/PageForm.ftl" as Form>
<#visit doc>
<#macro TreeManageFuncModel>
<#local baseInfo = .node.BaseInfo>
<#local handler = baseInfo.Handler>
<#local service = baseInfo.Service>
package ${Util.parsePkg(handler.@handlerClass)};

import java.util.*;

import ${service.@InterfaceName};
import com.agileai.hotweb.controller.core.TreeSelectHandler;
import com.agileai.domain.*;
import com.agileai.util.*;
import com.agileai.hotweb.domain.*;

<#assign tempHandlerId=baseInfo.Handler.@handlerId?string>
public class ${tempHandlerId[0..tempHandlerId?length-11]}ParentSelectHandler extends TreeSelectHandler{
	public ${tempHandlerId[0..tempHandlerId?length-11]}ParentSelectHandler(){
		super();
		this.serviceId = buildServiceId(${Util.parseClass(service.@InterfaceName)}.class);
		this.isMuilSelect = false;
	}
	protected TreeBuilder provideTreeBuilder(DataParam param){
		List<DataRow> records = getService().queryPickTreeRecords(param);
		TreeBuilder treeBuilder = new TreeBuilder(records,"${baseInfo.@idField}","${baseInfo.@nameField}","${baseInfo.@parentIdField}");

		String excludeId = param.get("${baseInfo.@idField}");
		treeBuilder.getExcludeIds().add(excludeId);
		
		return treeBuilder;
	}

	protected ${Util.parseClass(service.@InterfaceName)} getService() {
		return (${Util.parseClass(service.@InterfaceName)})this.lookupService(this.getServiceId());
	}
}	
</#macro>	
