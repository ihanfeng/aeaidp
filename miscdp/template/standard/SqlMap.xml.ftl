<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE sqlMap      
    PUBLIC "-//ibatis.apache.org//DTD SQL Map 2.0//EN"      
    "http://ibatis.apache.org/dtd/sql-map-2.dtd">
<sqlMap namespace="${table.namespace}">  
  <select id="findRecords" parameterClass="com.agileai.domain.DataParam" resultClass="com.agileai.domain.DataRow">
	${table.findRecordsSql}
  </select>
  <select id="getRecord" parameterClass="com.agileai.domain.DataParam" resultClass="com.agileai.domain.DataRow">
    ${table.getRecordSql}
  </select>
  <insert id="insertRecord" parameterClass="com.agileai.domain.DataParam">
    ${table.insertRecordSql}
  </insert>
  <update id="updateRecord" parameterClass="com.agileai.domain.DataParam">
    ${table.updateRecordSql}
  </update>
  <delete id="deleteRecord" parameterClass="com.agileai.domain.DataParam">
    ${table.deleteRecordSql}
  </delete>
</sqlMap>