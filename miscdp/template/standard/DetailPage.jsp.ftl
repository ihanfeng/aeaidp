<#ftl ns_prefixes={"D":"http://www.hotweb.agileai.com/model/standard",
"fa":"http://www.hotweb.agileai.com/model"}>
<#import "/common/Util.ftl" as Util>
<#import "/common/PageForm.ftl" as Form>
<#visit doc>
<#macro StandardFuncModel>
<#local baseInfo = .node.BaseInfo>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>${.node.BaseInfo.@detailTitle}</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
<@Form.PoupBoxScript paramArea=.node.DetailView.DetailEditArea></@Form.PoupBoxScript>
<#if Util.isValid(resFileModel)>
var addResourceRequestBox;
function openAddResourceRequestBox(){
	var title = "上载资源";
	if (!addResourceRequestBox){
		addResourceRequestBox = new PopupBox('addResourceRequestBox',title,{size:'big',width:'260px',height:'400px',top:'3px'});
	}
	var url = 'index?${resFileModel.handlerId}&${resFileModel.bizIdField}='+$('#${resFileModel.bizPrimaryKeyField}').val();
	addResourceRequestBox.sendRequest(url);	
}
function delResourceRequest(){
	if (confirm('是否确认要删除附件？')){
		var url = 'index?${resFileModel.handlerId}&actionType=delResource&${resFileModel.primaryKeyField}='+$('#__ResouceList').val();
		sendRequest(url,{onComplete:function(responseText){
			if ("success"==responseText){
				loadResourceList();
			}
		}});		
	}
}
function loadResourceList(){
	var formSelect = new FormSelect("__ResouceList");
	formSelect.removeAll();
	var url = 'index?${resFileModel.handlerId}&actionType=loadResourceList&${resFileModel.bizIdField}='+$('#${resFileModel.bizPrimaryKeyField}').val();
	sendRequest(url,{onComplete:function(responseText){
		if ("fail" != responseText){
			var datas = $.parseJSON(responseText);
			if (datas){
				for (var i=0;i < datas.length;i++){
					var data = datas[i];
					var dataText = data.text;
					var dataValue = data.value;
					formSelect.addItem(dataText,dataValue);	
				}				
			}
		}
	}});
}
function saveRecord(){
	postRequest('form1',{actionType:'save',onComplete:function(responseText){
		if ("fail" != responseText){
			alert("保存信息成功！");
			$("#${resFileModel.bizPrimaryKeyField}").val(responseText);
			$("#operaType").val("update");
			doSubmit({actionType:'prepareDisplay'});	
		}else{
			writeErrorMsg("保存信息失败！");
		}
	}});
}
</#if>
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="enableSave()" ><input value="&nbsp;" type="button" class="editImgBtn" id="modifyImgBtn" title="编辑" />编辑</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="<#if Util.isValid(resFileModel)>saveRecord()<#else>doSubmit({actionType:'save'<#if (checkUnique)>,checkUnique:'true'</#if>})</#if>"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="goToBack();"><input value="&nbsp;" type="button" class="backImgBtn" title="返回" />返回</td>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<@Form.DetailEditArea paramArea=.node.DetailView.DetailEditArea></@Form.DetailEditArea>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<@Form.DetailHiddenArea hiddenArea=.node.DetailView.DetailEditArea></@Form.DetailHiddenArea>
</form>
<script language="javascript">
<@Form.Validation paramArea=.node.DetailView.DetailEditArea></@Form.Validation>
initDetailOpertionImage();
<#if Util.isValid(resFileModel)>
if (isValid($("#${resFileModel.bizPrimaryKeyField}").val())){
	loadResourceList();
}
</#if>
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
</#macro>