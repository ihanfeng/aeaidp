package com.agileai.hotweb.module.system.service;

import com.agileai.hotweb.bizmoduler.core.TreeSelectServiceImpl;
import com.agileai.hotweb.cxmodule.SecurityGroupTreeSelect;

public class SecurityGroupTreeSelectImpl
        extends TreeSelectServiceImpl
        implements SecurityGroupTreeSelect {
    public SecurityGroupTreeSelectImpl() {
        super();
    }
}
