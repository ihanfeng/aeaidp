package com.agileai.miscdp.classpath;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jdt.core.ClasspathContainerInitializer;
import org.eclipse.jdt.core.IClasspathContainer;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;

import com.agileai.miscdp.MiscdpPlugin;

public class HotServerContainerInitializer extends
        ClasspathContainerInitializer {
 
    @Override
    public void initialize(IPath containerPath, IJavaProject project)
            throws CoreException {
        HotServerContainer container = new HotServerContainer(containerPath, project );
        if(container.isValid()) {
            JavaCore.setClasspathContainer(containerPath, new IJavaProject[] {project}, new IClasspathContainer[] {container}, null);             
        } else {
        	MiscdpPlugin.getDefault().logError("HotServerContainerInitializer initialize error !");
        }
    }
    
    @Override
    public boolean canUpdateClasspathContainer(IPath containerPath, 
            IJavaProject project) {
        return true;
    }

    @Override
    public void requestClasspathContainerUpdate(IPath containerPath, IJavaProject project, IClasspathContainer containerSuggestion) throws CoreException {
        JavaCore.setClasspathContainer(containerPath, new IJavaProject[] {project}, new IClasspathContainer[] { containerSuggestion }, null);
    }
}
