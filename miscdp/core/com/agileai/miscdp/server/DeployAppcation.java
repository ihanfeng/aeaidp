
package com.agileai.miscdp.server;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for deployAppcation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="deployAppcation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="appName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="appZip" type="{http://service.hotserver.agileai.com/}deployableZip" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "deployAppcation", propOrder = {
    "appName",
    "appZip"
})
public class DeployAppcation {

    protected String appName;
    protected DeployableZip appZip;

    /**
     * Gets the value of the appName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAppName() {
        return appName;
    }

    /**
     * Sets the value of the appName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAppName(String value) {
        this.appName = value;
    }

    /**
     * Gets the value of the appZip property.
     * 
     * @return
     *     possible object is
     *     {@link DeployableZip }
     *     
     */
    public DeployableZip getAppZip() {
        return appZip;
    }

    /**
     * Sets the value of the appZip property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeployableZip }
     *     
     */
    public void setAppZip(DeployableZip value) {
        this.appZip = value;
    }

}
