
package com.agileai.miscdp.server;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for deployModules complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="deployModules">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="appName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="modulesName" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="modulesZip" type="{http://service.hotserver.agileai.com/}deployableZip" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "deployModules", propOrder = {
    "appName",
    "modulesName",
    "modulesZip"
})
public class DeployModules {

    protected String appName;
    protected List<String> modulesName;
    protected DeployableZip modulesZip;

    /**
     * Gets the value of the appName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAppName() {
        return appName;
    }

    /**
     * Sets the value of the appName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAppName(String value) {
        this.appName = value;
    }

    /**
     * Gets the value of the modulesName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the modulesName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getModulesName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getModulesName() {
        if (modulesName == null) {
            modulesName = new ArrayList<String>();
        }
        return this.modulesName;
    }

    /**
     * Gets the value of the modulesZip property.
     * 
     * @return
     *     possible object is
     *     {@link DeployableZip }
     *     
     */
    public DeployableZip getModulesZip() {
        return modulesZip;
    }

    /**
     * Sets the value of the modulesZip property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeployableZip }
     *     
     */
    public void setModulesZip(DeployableZip value) {
        this.modulesZip = value;
    }

}
