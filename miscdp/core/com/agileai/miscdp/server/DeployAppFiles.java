
package com.agileai.miscdp.server;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for deployAppFiles complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="deployAppFiles">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="appName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="deleteDirs" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="appFilesZip" type="{http://service.hotserver.agileai.com/}deployableZip" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "deployAppFiles", propOrder = {
    "appName",
    "deleteDirs",
    "appFilesZip"
})
public class DeployAppFiles {

    protected String appName;
    protected List<String> deleteDirs;
    protected DeployableZip appFilesZip;

    /**
     * Gets the value of the appName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAppName() {
        return appName;
    }

    /**
     * Sets the value of the appName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAppName(String value) {
        this.appName = value;
    }

    /**
     * Gets the value of the deleteDirs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the deleteDirs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDeleteDirs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getDeleteDirs() {
        if (deleteDirs == null) {
            deleteDirs = new ArrayList<String>();
        }
        return this.deleteDirs;
    }

    /**
     * Gets the value of the appFilesZip property.
     * 
     * @return
     *     possible object is
     *     {@link DeployableZip }
     *     
     */
    public DeployableZip getAppFilesZip() {
        return appFilesZip;
    }

    /**
     * Sets the value of the appFilesZip property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeployableZip }
     *     
     */
    public void setAppFilesZip(DeployableZip value) {
        this.appFilesZip = value;
    }

}
