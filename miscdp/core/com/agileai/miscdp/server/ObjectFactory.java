
package com.agileai.miscdp.server;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.agileai.miscdp.server package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _StopApplicationResponse_QNAME = new QName("http://service.hotserver.agileai.com/", "stopApplicationResponse");
    private final static QName _IsExistContext_QNAME = new QName("http://service.hotserver.agileai.com/", "isExistContext");
    private final static QName _DeployServices_QNAME = new QName("http://service.hotserver.agileai.com/", "deployServices");
    private final static QName _CreateContextResponse_QNAME = new QName("http://service.hotserver.agileai.com/", "createContextResponse");
    private final static QName _DeleteContext_QNAME = new QName("http://service.hotserver.agileai.com/", "deleteContext");
    private final static QName _StartApplicationResponse_QNAME = new QName("http://service.hotserver.agileai.com/", "startApplicationResponse");
    private final static QName _DeleteContextResponse_QNAME = new QName("http://service.hotserver.agileai.com/", "deleteContextResponse");
    private final static QName _DeployAppFilesResponse_QNAME = new QName("http://service.hotserver.agileai.com/", "deployAppFilesResponse");
    private final static QName _StopApplication_QNAME = new QName("http://service.hotserver.agileai.com/", "stopApplication");
    private final static QName _RestartApplicationResponse_QNAME = new QName("http://service.hotserver.agileai.com/", "restartApplicationResponse");
    private final static QName _StartApplication_QNAME = new QName("http://service.hotserver.agileai.com/", "startApplication");
    private final static QName _DeployServicesResponse_QNAME = new QName("http://service.hotserver.agileai.com/", "deployServicesResponse");
    private final static QName _DeployModulesResponse_QNAME = new QName("http://service.hotserver.agileai.com/", "deployModulesResponse");
    private final static QName _RestartApplication_QNAME = new QName("http://service.hotserver.agileai.com/", "restartApplication");
    private final static QName _DeployModules_QNAME = new QName("http://service.hotserver.agileai.com/", "deployModules");
    private final static QName _DeployApplication_QNAME = new QName("http://service.hotserver.agileai.com/", "deployApplication");
    private final static QName _CreateContext_QNAME = new QName("http://service.hotserver.agileai.com/", "createContext");
    private final static QName _DeployApplicationResponse_QNAME = new QName("http://service.hotserver.agileai.com/", "deployApplicationResponse");
    private final static QName _DeployAppFiles_QNAME = new QName("http://service.hotserver.agileai.com/", "deployAppFiles");
    private final static QName _IsExistContextResponse_QNAME = new QName("http://service.hotserver.agileai.com/", "isExistContextResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.agileai.miscdp.server
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DeleteContextResponse }
     * 
     */
    public DeleteContextResponse createDeleteContextResponse() {
        return new DeleteContextResponse();
    }

    /**
     * Create an instance of {@link DeployApplication }
     * 
     */
    public DeployApplication createDeployApplication() {
        return new DeployApplication();
    }

    /**
     * Create an instance of {@link IsExistContext }
     * 
     */
    public IsExistContext createIsExistContext() {
        return new IsExistContext();
    }

    /**
     * Create an instance of {@link DeployServicesResponse }
     * 
     */
    public DeployServicesResponse createDeployServicesResponse() {
        return new DeployServicesResponse();
    }

    /**
     * Create an instance of {@link StartApplication }
     * 
     */
    public StartApplication createStartApplication() {
        return new StartApplication();
    }

    /**
     * Create an instance of {@link StopApplicationResponse }
     * 
     */
    public StopApplicationResponse createStopApplicationResponse() {
        return new StopApplicationResponse();
    }

    /**
     * Create an instance of {@link DeployAppFiles }
     * 
     */
    public DeployAppFiles createDeployAppFiles() {
        return new DeployAppFiles();
    }

    /**
     * Create an instance of {@link DeployServices }
     * 
     */
    public DeployServices createDeployServices() {
        return new DeployServices();
    }

    /**
     * Create an instance of {@link RestartApplication }
     * 
     */
    public RestartApplication createRestartApplication() {
        return new RestartApplication();
    }

    /**
     * Create an instance of {@link IsExistContextResponse }
     * 
     */
    public IsExistContextResponse createIsExistContextResponse() {
        return new IsExistContextResponse();
    }

    /**
     * Create an instance of {@link DeployModulesResponse }
     * 
     */
    public DeployModulesResponse createDeployModulesResponse() {
        return new DeployModulesResponse();
    }

    /**
     * Create an instance of {@link DeployAppFilesResponse }
     * 
     */
    public DeployAppFilesResponse createDeployAppFilesResponse() {
        return new DeployAppFilesResponse();
    }

    /**
     * Create an instance of {@link StopApplication }
     * 
     */
    public StopApplication createStopApplication() {
        return new StopApplication();
    }

    /**
     * Create an instance of {@link DeployApplicationResponse }
     * 
     */
    public DeployApplicationResponse createDeployApplicationResponse() {
        return new DeployApplicationResponse();
    }

    /**
     * Create an instance of {@link CreateContextResponse }
     * 
     */
    public CreateContextResponse createCreateContextResponse() {
        return new CreateContextResponse();
    }

    /**
     * Create an instance of {@link CreateContext }
     * 
     */
    public CreateContext createCreateContext() {
        return new CreateContext();
    }

    /**
     * Create an instance of {@link DeployableZip }
     * 
     */
    public DeployableZip createDeployableZip() {
        return new DeployableZip();
    }

    /**
     * Create an instance of {@link DeleteContext }
     * 
     */
    public DeleteContext createDeleteContext() {
        return new DeleteContext();
    }

    /**
     * Create an instance of {@link DeployModules }
     * 
     */
    public DeployModules createDeployModules() {
        return new DeployModules();
    }

    /**
     * Create an instance of {@link RestartApplicationResponse }
     * 
     */
    public RestartApplicationResponse createRestartApplicationResponse() {
        return new RestartApplicationResponse();
    }

    /**
     * Create an instance of {@link StartApplicationResponse }
     * 
     */
    public StartApplicationResponse createStartApplicationResponse() {
        return new StartApplicationResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StopApplicationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.hotserver.agileai.com/", name = "stopApplicationResponse")
    public JAXBElement<StopApplicationResponse> createStopApplicationResponse(StopApplicationResponse value) {
        return new JAXBElement<StopApplicationResponse>(_StopApplicationResponse_QNAME, StopApplicationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsExistContext }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.hotserver.agileai.com/", name = "isExistContext")
    public JAXBElement<IsExistContext> createIsExistContext(IsExistContext value) {
        return new JAXBElement<IsExistContext>(_IsExistContext_QNAME, IsExistContext.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeployServices }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.hotserver.agileai.com/", name = "deployServices")
    public JAXBElement<DeployServices> createDeployServices(DeployServices value) {
        return new JAXBElement<DeployServices>(_DeployServices_QNAME, DeployServices.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateContextResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.hotserver.agileai.com/", name = "createContextResponse")
    public JAXBElement<CreateContextResponse> createCreateContextResponse(CreateContextResponse value) {
        return new JAXBElement<CreateContextResponse>(_CreateContextResponse_QNAME, CreateContextResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteContext }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.hotserver.agileai.com/", name = "deleteContext")
    public JAXBElement<DeleteContext> createDeleteContext(DeleteContext value) {
        return new JAXBElement<DeleteContext>(_DeleteContext_QNAME, DeleteContext.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StartApplicationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.hotserver.agileai.com/", name = "startApplicationResponse")
    public JAXBElement<StartApplicationResponse> createStartApplicationResponse(StartApplicationResponse value) {
        return new JAXBElement<StartApplicationResponse>(_StartApplicationResponse_QNAME, StartApplicationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteContextResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.hotserver.agileai.com/", name = "deleteContextResponse")
    public JAXBElement<DeleteContextResponse> createDeleteContextResponse(DeleteContextResponse value) {
        return new JAXBElement<DeleteContextResponse>(_DeleteContextResponse_QNAME, DeleteContextResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeployAppFilesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.hotserver.agileai.com/", name = "deployAppFilesResponse")
    public JAXBElement<DeployAppFilesResponse> createDeployAppFilesResponse(DeployAppFilesResponse value) {
        return new JAXBElement<DeployAppFilesResponse>(_DeployAppFilesResponse_QNAME, DeployAppFilesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StopApplication }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.hotserver.agileai.com/", name = "stopApplication")
    public JAXBElement<StopApplication> createStopApplication(StopApplication value) {
        return new JAXBElement<StopApplication>(_StopApplication_QNAME, StopApplication.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RestartApplicationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.hotserver.agileai.com/", name = "restartApplicationResponse")
    public JAXBElement<RestartApplicationResponse> createRestartApplicationResponse(RestartApplicationResponse value) {
        return new JAXBElement<RestartApplicationResponse>(_RestartApplicationResponse_QNAME, RestartApplicationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StartApplication }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.hotserver.agileai.com/", name = "startApplication")
    public JAXBElement<StartApplication> createStartApplication(StartApplication value) {
        return new JAXBElement<StartApplication>(_StartApplication_QNAME, StartApplication.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeployServicesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.hotserver.agileai.com/", name = "deployServicesResponse")
    public JAXBElement<DeployServicesResponse> createDeployServicesResponse(DeployServicesResponse value) {
        return new JAXBElement<DeployServicesResponse>(_DeployServicesResponse_QNAME, DeployServicesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeployModulesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.hotserver.agileai.com/", name = "deployModulesResponse")
    public JAXBElement<DeployModulesResponse> createDeployModulesResponse(DeployModulesResponse value) {
        return new JAXBElement<DeployModulesResponse>(_DeployModulesResponse_QNAME, DeployModulesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RestartApplication }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.hotserver.agileai.com/", name = "restartApplication")
    public JAXBElement<RestartApplication> createRestartApplication(RestartApplication value) {
        return new JAXBElement<RestartApplication>(_RestartApplication_QNAME, RestartApplication.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeployModules }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.hotserver.agileai.com/", name = "deployModules")
    public JAXBElement<DeployModules> createDeployModules(DeployModules value) {
        return new JAXBElement<DeployModules>(_DeployModules_QNAME, DeployModules.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeployApplication }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.hotserver.agileai.com/", name = "deployApplication")
    public JAXBElement<DeployApplication> createDeployApplication(DeployApplication value) {
        return new JAXBElement<DeployApplication>(_DeployApplication_QNAME, DeployApplication.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateContext }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.hotserver.agileai.com/", name = "createContext")
    public JAXBElement<CreateContext> createCreateContext(CreateContext value) {
        return new JAXBElement<CreateContext>(_CreateContext_QNAME, CreateContext.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeployApplicationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.hotserver.agileai.com/", name = "deployApplicationResponse")
    public JAXBElement<DeployApplicationResponse> createDeployApplicationResponse(DeployApplicationResponse value) {
        return new JAXBElement<DeployApplicationResponse>(_DeployApplicationResponse_QNAME, DeployApplicationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeployAppFiles }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.hotserver.agileai.com/", name = "deployAppFiles")
    public JAXBElement<DeployAppFiles> createDeployAppFiles(DeployAppFiles value) {
        return new JAXBElement<DeployAppFiles>(_DeployAppFiles_QNAME, DeployAppFiles.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsExistContextResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.hotserver.agileai.com/", name = "isExistContextResponse")
    public JAXBElement<IsExistContextResponse> createIsExistContextResponse(IsExistContextResponse value) {
        return new JAXBElement<IsExistContextResponse>(_IsExistContextResponse_QNAME, IsExistContextResponse.class, null, value);
    }

}
