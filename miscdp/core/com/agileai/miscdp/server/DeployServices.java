
package com.agileai.miscdp.server;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for deployServices complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="deployServices">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="appName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="serivcesZip" type="{http://service.hotserver.agileai.com/}deployableZip" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "deployServices", propOrder = {
    "appName",
    "serivcesZip"
})
public class DeployServices {

    protected String appName;
    protected DeployableZip serivcesZip;

    /**
     * Gets the value of the appName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAppName() {
        return appName;
    }

    /**
     * Sets the value of the appName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAppName(String value) {
        this.appName = value;
    }

    /**
     * Gets the value of the serivcesZip property.
     * 
     * @return
     *     possible object is
     *     {@link DeployableZip }
     *     
     */
    public DeployableZip getSerivcesZip() {
        return serivcesZip;
    }

    /**
     * Sets the value of the serivcesZip property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeployableZip }
     *     
     */
    public void setSerivcesZip(DeployableZip value) {
        this.serivcesZip = value;
    }

}
