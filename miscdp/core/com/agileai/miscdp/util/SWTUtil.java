package com.agileai.miscdp.util;

import java.awt.Toolkit;

import org.apache.commons.lang.StringUtils;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DragSource;
import org.eclipse.swt.dnd.DropTarget;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Caret;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.ScrollBar;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Widget;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

public class SWTUtil {
	private static String fileDlgLatestPath;
	private static String dirDlgLatestPath;

	public static Display getStandardDisplay() {
		Display display = Display.getCurrent();
		if (display == null)
			display = Display.getDefault();
		return display;
	}

	public static Shell getShell(Widget widget) {
		if ((widget instanceof Control))
			return ((Control) widget).getShell();
		if ((widget instanceof Caret))
			return ((Caret) widget).getParent().getShell();
		if ((widget instanceof DragSource))
			return ((DragSource) widget).getControl().getShell();
		if ((widget instanceof DropTarget))
			return ((DropTarget) widget).getControl().getShell();
		if ((widget instanceof Menu))
			return ((Menu) widget).getParent().getShell();
		if ((widget instanceof ScrollBar)) {
			return ((ScrollBar) widget).getParent().getShell();
		}
		return null;
	}

	public static void invokeOnDisplayThread(Runnable runnable) {
		IWorkbench workbench = PlatformUI.getWorkbench();
		IWorkbenchWindow[] windows = workbench.getWorkbenchWindows();
		if ((windows != null) && (windows.length > 0)) {
			Display display = windows[0].getShell().getDisplay();
			display.syncExec(runnable);
		} else {
			runnable.run();
		}
	}

	public static void invokeOnDisplayAsynThread(Runnable runnable) {
		IWorkbench workbench = PlatformUI.getWorkbench();
		IWorkbenchWindow[] windows = workbench.getWorkbenchWindows();
		if ((windows != null) && (windows.length > 0)) {
			Display display = windows[0].getShell().getDisplay();
			display.asyncExec(runnable);
		} else {
			runnable.run();
		}
	}


	public static int getTableHeightHint(Table table, int rows) {
		if (table.getFont().equals(JFaceResources.getDefaultFont()))
			table.setFont(JFaceResources.getDialogFont());
		int result = table.getItemHeight() * rows + table.getHeaderHeight();
		if (table.getLinesVisible())
			result += table.getGridLineWidth() * (rows - 1);
		return result;
	}

	public static boolean isWindows() {
		return SWT.getPlatform().toLowerCase().startsWith("win");
	}

	public static void centerWindow(Shell shell) {
		int w = Toolkit.getDefaultToolkit().getScreenSize().width;
		int h = Toolkit.getDefaultToolkit().getScreenSize().height;
		Rectangle shellRect = shell.getBounds();
		int x = (w - shellRect.width) / 2;
		int y = (h - shellRect.height) / 2;
		shell.setLocation(x, y);
	}

	public static FileDialog openFileDialog(Shell shell, int style,
			String[] extensions) {
		FileDialog dlg = new FileDialog(shell, style);
		dlg.setFilterExtensions(extensions);
		if (!StringUtils.isEmpty(fileDlgLatestPath)) {
			dlg.setFilterPath(fileDlgLatestPath);
		}
		dlg.open();
		fileDlgLatestPath = dlg.getFilterPath();
		return dlg;
	}

	public static DirectoryDialog openDirDialog(Shell shell, int style) {
		DirectoryDialog dlg = new DirectoryDialog(shell, style);
		if (!StringUtils.isEmpty(dirDlgLatestPath)) {
			dlg.setFilterPath(dirDlgLatestPath);
		}
		dlg.open();
		dirDlgLatestPath = dlg.getFilterPath();
		return dlg;
	}

	public static void showDialog(final Dialog dialog) {
		if (dialog == null) {
			return;
		}
		Runnable runnable = new Runnable() {
			public void run() {
				dialog.open();
			}
		};
		Display.getDefault().syncExec(runnable);
	}

	public static void setDialogSize(Dialog dialog, int width, int height) {
		Shell shell = dialog.getShell();
		Point computedSize = shell.computeSize(-1, -1);
		width = Math.max(computedSize.x, width);
		height = Math.max(computedSize.y, height);
		dialog.getShell().setSize(width, height);
	}
}