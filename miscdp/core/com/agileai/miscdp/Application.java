package com.agileai.miscdp;

import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;

public class Application implements IApplication {
	public Object start(IApplicationContext context) throws Exception {
		System.setProperty("file.encoding", "UTF-8");
		Display display = PlatformUI.createDisplay();
		try {
			int returnCode = PlatformUI.createAndRunWorkbench(display,new ApplicationWorkbenchAdvisor());
			if (returnCode == 1) {
				return IApplication.EXIT_RESTART;
			}
			Integer localInteger = IApplication.EXIT_OK;
			return localInteger;
		} finally {
			display.dispose();
		}
	}
	public void stop() {
	}
}